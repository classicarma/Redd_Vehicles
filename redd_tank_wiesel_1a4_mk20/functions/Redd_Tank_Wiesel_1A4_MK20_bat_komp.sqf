

	//Function for Eden attributes to set battalion and company number

	if (!isServer) exitWith {};

	params["_veh"];
	
	_w = _veh getVariable ["Redd_Tank_Wiesel_1A4_MK_Waffengattung",""];
	_b = _veh getVariable ["Redd_Tank_Wiesel_1A4_MK_Bataillon",""];
	_k = _veh getVariable ["Redd_Tank_Wiesel_1A4_MK_Kompanie",""];

	//sets unit
	switch toLower (_w) do
	{
		

		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [8, "\Redd_Vehicles_Main\data\Jg.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [8, "\Redd_Vehicles_Main\data\GebJg.paa"];
		
		};

		case ("3"):
		{
		
			_veh setObjectTextureGlobal [8, "\Redd_Vehicles_Main\data\falli.paa"];
		
		};
		
	};

	//sets battalion
	switch toLower (_b) do 
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_1_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_91_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_291_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_292_ca.paa"];
		
		};
		
		case ("5"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_413_ca.paa"];
		
		};

		case ("6"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_231_ca.paa"];
		
		};

		case ("7"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_232_ca.paa"];
		
		};

		case ("8"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_233_ca.paa"];
		
		};

		case ("9"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_26_ca.paa"];
		
		};

		case ("10"):
		{
		
			_veh setObjectTextureGlobal [9, "\Redd_Vehicles_Main\data\Redd_Bataillon_31_ca.paa"];
		
		};

	};

	//sets company
	switch toLower (_k) do 
	{
	
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [10, "\Redd_Vehicles_Main\data\Redd_Kompanie_5_ca.paa"];
				
		};

		case ("2"):
		{
		
			_veh setObjectTextureGlobal [10, "\Redd_Vehicles_Main\data\Redd_Kompanie_7_ca.paa"];
				
		};
		
	};