

	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;
			class CommanderOptics;
			
		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/
		class Redd_Tank_Wiesel_1A4_MK20_base: Tank_F
		{
			
			#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
			displayname = "$STR_Wiesel_1A4_MK20";
			picture="\A3\armor_f_beta\APC_Tracked_01\Data\UI\APC_Tracked_01_base_ca.paa";
			icon="\A3\armor_f_beta\APC_Tracked_01\Data\UI\map_APC_Tracked_01_CA.paa";
			side = 1;
			crew = "B_crew_F";
			author = "ReddNTank";
			model = "\Redd_Tank_Wiesel_1A4_MK20\Redd_Tank_Wiesel_1A4_MK20";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Waffentraeger";
			smokeLauncherGrenadeCount = 0;
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
			driverInAction="Redd_Tank_Wiesel_1A2_TOW_Driver";
			armor = 150;
			armorStructural = 6;
			cost = 1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_3_bound";
			dustFrontRightPos = "wheel_2_3_bound";
			dustBackLeftPos = "wheel_1_5_bound";
			dustBackRightPos = "wheel_2_5_bound";
			viewDriverInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1100; //Pilot
			enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos gunner";
			forceHideDriver = 0;
			driverWeaponsInfoType="Redd_RCS_Driver";
			headGforceLeaningFactor[]={0.001,0,0.0075};

			threat[]={0.9,0.6,0.6};
			audible = 4;
			camouflage = 5;	

			slingLoadCargoMemoryPoints[] = {"SlingLoadCargo1","SlingLoadCargo2","SlingLoadCargo3","SlingLoadCargo4"};
			
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "gunner"};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "gunner"};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "gunner"};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 0;
			//Ende ACRE2

			hiddenSelections[] = 
			{

				"wanne",//0
				"turm_ext",//1
				"plate_1", //2
				"plate_2", //3
				"plate_3", //4
				"plate_4", //5
				"plate_5", //6
				"plate_6", //7

				"TakZeichen", //8
				"Bataillon", //9
				"Kompanie" //10
				
			};
			
			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"Redd_Tank_Wiesel_1A4_MK20\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne.rvmat",
					"Redd_Tank_Wiesel_1A4_MK20\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne_damage.rvmat",
					"Redd_Tank_Wiesel_1A4_MK20\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne_destruct.rvmat",
					
					"Redd_Tank_Wiesel_1A4_MK20\mats\Redd_Tank_Wiesel_1A4_MK20_Turm_ext.rvmat",
					"Redd_Tank_Wiesel_1A4_MK20\mats\Redd_Tank_Wiesel_1A4_MK20_Turm_ext_damage.rvmat",
					"Redd_Tank_Wiesel_1A4_MK20\mats\Redd_Tank_Wiesel_1A4_MK20_Turm_ext_destruct.rvmat"
					
				};
				
        	};
			
			class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};

			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust";
					direction = "exhaust_dir";
					effect = "ExhaustsEffect";
					
				};
				
			};

			class Reflectors 
			{

				class Left 
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
                    position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
                    size = 1;
                    innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
                    intensity = 1;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };

				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

			};

			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull 
				{	
				
					armor=2;
					material=-1;
					name="hull";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.15;
					armorComponent="";

				};
				
				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="DamageVisual";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.02;
					armorComponent="";

				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="engine";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.15;
					armorComponent="";

				};
				
				class HitLTrack: HitLTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.1;
					armorComponent="";

				};
				
				class HitRTrack: HitRTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.1;
					armorComponent="";

				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks {

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};

			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets {};

					//Mainturret [0]
					weapons[] =
					{
						
						"Redd_Gesichert",
						"Redd_MK20FL"
						
					};
					
					magazines[] = 
					{	
					
						"Redd_MK20_HE_Mag200",
						"Redd_MK20_AP_Mag120"
				
					};

					startEngine = 0;
					stabilizedInAxes = 0;
					forceHideGunner = 0;
					initElev = 0;
					minElev = -8;
					maxElev = 45;
					initTurn = 0;
					minTurn = -110;
					maxTurn = 110;
					maxHorizontalRotSpeed = 1;// 1 = 45°/sec
					maxVerticalRotSpeed = 1;
					gunnerForceOptics=0;
					gunnerInAction="Redd_Tank_Wiesel_1A4_MK_Commander";
					gunnerAction="Redd_Tank_Wiesel_1A4_MK_Commander_Out";
					proxyindex = 1;
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetInAction="GetInMedium";
					gunnerGetOutAction="GetOutMedium";
					turretInfoType = "Redd_RCS_Turret";
					discreteDistance[] = {200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000};
					discreteDistanceInitIndex = 3;
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					soundAttenuationTurret = "TankAttenuation";
					gunnerCompartments= "Compartment2";
					memoryPointsGetInGunner= "pos_commander";
					memoryPointsGetInGunnerDir= "pos_commander_dir";
					animationSourceHatch = "hatchCommander_Source";
					lockWhenDriverOut = 1;
					gunnerRightHandAnimName = "handle_r";
					gunnerLeftHandAnimName = "handle_l";
					gunnerName = "$STR_Kommandant";	
					soundServo[]={};
					soundServoVertical[]={};
					gunnerOutOpticsModel="";
					outGunnerMayFire=0;

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {2,3};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
					
					class OpticsIn
					{
						
						class Day1
						{

							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.23;
							maxFov = 0.23;
							minFov = 0.23;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.06;
							maxFov = 0.06;
							minFov = 0.06;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T2";
							gunnerOpticsEffect[] = {};
							
						};

						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.23;
							maxFov = 0.23;
							minFov = 0.23;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W1";
							gunnerOpticsEffect[] = {};
							
						};

						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.023;
							maxFov = 0.023;
							minFov = 0.023;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W2";
							gunnerOpticsEffect[] = {};
							
						};
						
					};

					class HitPoints 
					{
						
						class HitTurret	
						{
							
							armor = 1;
							material = -1;
							name = "vez";
							visual="DamageVisual";
							passThrough = 0;
							minimalHit = 0.02;
							explosionShielding = 0.3;
							radius = 0.095;
							armorComponent="";
							isTurret=1;

						};
						
						class HitGun	
						{
							
							armor = 1;
							material = -1;
							name = "zbran";
							visual="DamageVisual";
							passThrough = 0;
							minimalHit = 0;
							explosionShielding = 1;
							radius = 0.09;
							armorComponent="";
							isGun=1;
							
						};

					};

				};

				class wieselMk20_Bino_Turret_Com: NewTurret //[1]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 1;
					proxyType= "CPGunner";
					gunnerName="Bino_Com";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutMedium";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment3";
					LODTurnedIn = 1000; //Gunner
					LODTurnedOut= 1000; //Gunner
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="rnt_wieselMK20_com_out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="rnt_wieselMK20_com_out_ffv";

					memoryPointsGetInGunner= "pos_commander";
					memoryPointsGetInGunnerDir= "pos_commander_dir";

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};

			};

			class AnimationSources
			{
				
				class HatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class recoil_source
				{
					
					source = "reload"; 
					weapon = "Redd_MK20FL";
					
				};

				class flash_mk_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MK20FL";
					initPhase = 0;
					
				};

				//Netze
				class mk20_netz_boden_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_boden_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_boden_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class mk20_netz_wanne_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_wanne_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_wanne_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class mk20_netz_turm_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_turm_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_turm_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class mk20_netz_waffe_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_waffe_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				class mk20_netz_waffe_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

			};

			class UserActions
			{
				
                class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this];";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};
				
				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0] == player) and (this animationSourcePhase 'hatchCommander_Source' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_wieselMk20_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0]];[this,[[0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_wieselMk20_Bino_In', false, true];"; 
				
				};

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and (this getVariable 'has_flag')) or ((player in [gunner this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and (this getVariable 'has_flag'))";
					statement = "[this,0] call Redd_fnc_wiesel_mk_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and !(this getVariable 'has_flag')) or ((player in [gunner this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and !(this getVariable 'has_flag'))";
					statement = "[this,1] call Redd_fnc_wiesel_mk_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and !(this getVariable 'has_flag')) or ((player in [gunner this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and !(this getVariable 'has_flag'))";
					statement = "[this,2] call Redd_fnc_wiesel_mk_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and !(this getVariable 'has_flag')) or ((player in [gunner this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and !(this getVariable 'has_flag'))";
					statement = "[this,3] call Redd_fnc_wiesel_mk_flags";

				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_mk20_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_mk20_camonet";

				};

				class Tarnnetz_Boden_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_mk20_camonet";

				};

				class Tarnnetz_Boden_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and (this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_mk20_camonet";

				};
				
			};

			class EventHandlers: EventHandlers 
			{

				init = "_this call redd_fnc_Wiesel_MK20_Init";
				turnIn = "_this call redd_fnc_Wiesel_MK20_TurnIn";
				turnOut = "_this call redd_fnc_Wiesel_MK20_TurnOut";
				getOut = "_this call redd_fnc_Wiesel_MK20_GetOut";
				getIn = "_this call redd_fnc_Wiesel_MK20_GetIn";

			};

			class Attributes 
			{
				
				class Redd_Tank_Wiesel_1A4_MK20_Waffengattung_Attribute 
				{

					displayName = "$STR_Waffengattung";
					tooltip = "$STR_Waffengattung";
					property = "Redd_Tank_Wiesel_1A4_MK_Waffengattung_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A4_MK_Waffengattung', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Jg
						{
							
							name = "Jaeger";
							value = "1";
							
						};

                        class GebJg 
						{
							
							name = "Gebirgsjaeger";
							value = "2";
							
						};

                        class Fallis 
						{
							
							name = "Fallschirmjaeger";
							value = "3";
							
						};

                    };

				};

				class Redd_Tank_Wiesel_1A4_MK20_Bataillon_Attribute 
				{

					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Tank_Wiesel_1A4_MK_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A4_MK_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_1 
						{
							
							name = "JgBtl 1";
							value = "1";
							
						};

						class Bataillon_91
						{
							
							name = "JgBtl 91";
							value = "2";
							
						};

						class Bataillon_291 
						{
							
							name = "JgBtl 291";
							value = "3";
							
						};

						class Bataillon_292 
						{
							
							name = "JgBtl 292";
							value = "4";
							
						};
						
						class Bataillon_413 
						{
							
							name = "JgBtl 413";
							value = "5";
							
						};

                        class Bataillon_231
						{
							
							name = "GebJgBtl 231";
							value = "6";
							
						};

                        class Bataillon_232 
						{
							
							name = "GebJgBtl 232";
							value = "7";
							
						};

                        class Bataillon_233
						{
							
							name = "GebJgBtl 233";
							value = "8";
							
						};
						
						class Bataillon_26
						{
							
							name = "FJgReg 26";
							value = "9";
							
						};

						class Bataillon_31
						{
							
							name = "FJgReg 31";
							value = "10";
							
						};

					};

				};

				class Redd_Tank_Wiesel_1A4_MK20_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Wiesel_1A4_MK_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A4_MK_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
				
						class Kompanie_5
						{
							
							name = "5";
							value = "1";
							
						};

						class Kompanie_7
						{
							
							name = "7";
							value = "2";
							
						};
						
					};
					
				};

			};

		};
		
		/*	Public class Flecktarn	*/
		class Redd_Tank_Wiesel_1A4_MK20_Flecktarn: Redd_Tank_Wiesel_1A4_MK20_base
		{
			
			editorPreview="\Redd_Tank_Wiesel_1A4_MK20\pictures\Wiesel_MK20_F_Pre_Picture.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Wiesel_1A4_MK20_Flecktarn";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_blend_co.paa",
				"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A4_MK20_Turm_ext_blend_co.paa"

			};

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Wiesel_1A4_MK20_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_blend_co.paa",
						"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A4_MK20_Turm_ext_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Wiesel_1A4_MK20_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_D_blend_co.paa",
						"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A4_MK20_Turm_D_ext_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Wiesel_1A4_MK20_Winter";
					author = "ReddNTank";
					
					textures[]=
					{

						"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_W_blend_co.paa",
						"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A4_MK20_Turm_W_ext_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};
			
		};
		
		/*	Public class Tropentarn	*/
		class Redd_Tank_Wiesel_1A4_MK20_Tropentarn: Redd_Tank_Wiesel_1A4_MK20_Flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Wiesel_1A4_MK20\pictures\Wiesel_MK20_D_Pre_Picture.paa";
			displayName="$STR_Wiesel_1A4_MK20_Tropentarn";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_D_blend_co.paa",
				"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A4_MK20_Turm_D_ext_blend_co.paa"

			};

		};
		
		/*	Public class Wintertarn	*/
		class Redd_Tank_Wiesel_1A4_MK20_Wintertarn: Redd_Tank_Wiesel_1A4_MK20_Flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Wiesel_1A4_MK20\pictures\Wiesel_MK20_W_Pre_Picture.paa";
			displayName="$STR_Wiesel_1A4_MK20_Winter";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_W_blend_co.paa",
				"Redd_Tank_Wiesel_1A4_MK20\data\Redd_Tank_Wiesel_1A4_MK20_Turm_W_ext_blend_co.paa"

			};

		};
		
	};