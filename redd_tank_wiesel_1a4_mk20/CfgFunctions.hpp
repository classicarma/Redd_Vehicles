
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class Wiesel_MK20_Init
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_Init.sqf";

                };

                class Wiesel_MK20_Bat_Komp
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_bat_komp.sqf";

                };

                class Wiesel_MK20_Plate
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_plate.sqf";

                };

                class Wiesel_MK20_TurnIn
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_TurnIn.sqf";

                };

                class Wiesel_MK20_TurnOut
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_TurnOut.sqf";

                };

                class wiesel_mk_flags
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_flags.sqf";

                };

                class Wiesel_MK20_GetOut
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_GetOut.sqf";

                };

                class Wiesel_MK20_GetIn
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_GetIn.sqf";

                };

                class mk20_camonet
                {

                    file = "\Redd_Tank_Wiesel_1A4_MK20\functions\Redd_Tank_Wiesel_1A4_MK20_camonet.sqf";

                };
                
            };

        };

    };