

    class CfgPatches
	{
		
		class Redd_Tank_Fuchs_1A4
		{
			
			units[] = 
			{
				
				"Redd_Tank_Fuchs_1A4_Jg_Flecktarn",
				"Redd_Tank_Fuchs_1A4_Jg_Tropentarn",
				"Redd_Tank_Fuchs_1A4_Jg_Wintertarn",

				"Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn",
				"Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn",
				"Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn",
				
				"Redd_Tank_Fuchs_1A4_Pi_Flecktarn",
				"Redd_Tank_Fuchs_1A4_Pi_Tropentarn",
				"Redd_Tank_Fuchs_1A4_Pi_Wintertarn",

				"Redd_Tank_Fuchs_1A4_San_Flecktarn",
				"Redd_Tank_Fuchs_1A4_San_Tropentarn",
				"Redd_Tank_Fuchs_1A4_San_Wintertarn"

			};
			weapons[] = {};
			requiredVersion = 0.1;
			//requiredAddons[] = {"A3_Soft_F","Redd_Vehicles_Main","Redd_Milan_Static"};
			requiredAddons[] = {"A3_Soft_F","Redd_Vehicles_Main"};

		};
		
	};