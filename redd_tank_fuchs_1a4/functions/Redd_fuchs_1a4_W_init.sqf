

	params ["_veh"];

	[_veh] spawn redd_fnc_fuchs_plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_fuchs_bat_Komp;//spawns function to set battalion and company numbers

	_veh setVariable ['Redd_Fuchs_MG3_In', false, true];
	_veh setVariable ['Redd_Fuchs_Milan_In', false, true];
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags

	[_veh,[[0],true]] remoteExecCall ['lockTurret']; //Locks MG3 turret
	[_veh,[[1],true]] remoteExecCall ['lockTurret']; //Locks Milan

	_veh setObjectMaterialGlobal [2, "Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_W_Reifen.rvmat"];
	
	true