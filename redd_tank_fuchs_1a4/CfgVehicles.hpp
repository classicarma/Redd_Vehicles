    

    class CfgVehicles 
    {

        class Car;

        class Car_F: Car
	    {

            class NewTurret;
            class Sounds;

            class HitPoints
            {

                class HitBody;
                class HitEngine;
                class HitFuel;
                class HitHull;
                class HitLFWheel;
                class HitLBWheel;
                class HitLMWheel;
                class HitLF2Wheel;
                class HitRFWheel;
                class HitRBWheel;
                class HitRMWheel;
                class HitRF2Wheel;
                class HitGlass1;
                class HitGlass2;
                class HitGlass3;
                class HitGlass4;
                class HitGlass5;
                class HitGlass6;

            };

	    };

        class Wheeled_APC_F: Car_F
	    {
            class ViewOptics;
            class ViewCargo;

            class Sounds: Sounds
            {

                class Engine;
                class Movement;

            };

            class NewTurret;

            class Turrets
            {

                class MainTurret: NewTurret
                {

                    class ViewOptics;
                    class Turrets
                    {

                        class CommanderOptics;

                    };

                };

            };

            class AnimationSources;
            class EventHandlers;

        };

        class Redd_Tank_Fuchs_1A4_Base: Wheeled_APC_F
	    {
            
            #include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
            displayName = "$STR_Fuchs_1A4";
			icon = "\A3\Armor_F_Beta\APC_Wheeled_02\Data\UI\map_APC_Wheeled_02_rcws_ca.paa";
			picture = "\A3\Armor_F_Beta\APC_Wheeled_02\Data\UI\APC_Wheeled_02_RCWS_CA.paa";
			side = 1;
			crew = "B_crew_F";
            author = "Tank, Pazuzu, Redd";
            model = "\Redd_Tank_Fuchs_1A4\Redd_Tank_Fuchs_1A4";
            editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Tpz";
            smokeLauncherGrenadeCount = 6;
			smokeLauncherAngle = 142.5;
            getInAction = "GetInMedium";
		    getOutAction = "GetOutMedium"; 
            driverAction = "Redd_Tank_Fuchs_Driver";
            driverInAction="Redd_Tank_Fuchs_Driver";
            armor = 200;
            armorStructural = 5;
           	cost = 1000000;
			dustFrontLeftPos = "TrackFLL";
			dustFrontRightPos = "TrackFRR";
			dustBackLeftPos = "TrackBLL";
			dustBackRightPos = "TrackBRR";
            viewDriverInExternal = 1;
            lodTurnedIn = 1100; //Pilot 1100
			lodTurnedOut = 1100; //Pilot 1100
            enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos cargo";
			driverWeaponsInfoType="Redd_RCS_Driver_APC";
			headGforceLeaningFactor[]={0.00075,0,0.0075};
			
            attenuationEffectType = "TankAttenuation";
		    crewCrashProtection = 0.25;
            weapons[] = {"TruckHorn","Redd_SmokeLauncher"};
		    magazines[] = {"Redd_SmokeLauncherMag"};
            hideWeaponsCargo = 0;
			destrType = "DestructWreck";
			wheelDamageThreshold = 1;
        	wheelDestroyThreshold = 0.99;
        	wheelDamageRadiusCoef = 1;
        	wheelDestroyRadiusCoef = 0.55;
			unloadInCombat = 0;
			typicalCargo[] = {"B_crew_F"};
			transportSoldier = 5;
			cargoAction[] = {"Redd_Tank_Fuchs_Passenger_2","Redd_Tank_Fuchs_Passenger_3","Redd_Tank_Fuchs_Passenger_4"};
            cargoProxyIndexes[] = {2,3,4,5,6};
			viewCargoInExternal = 1;
			cargoCompartments[] = {"Compartment2"};
			memoryPointTrackFLL = "TrackFLL";
		    memoryPointTrackFLR = "TrackFLR";
		    memoryPointTrackBLL = "TrackBLL";
		    memoryPointTrackBLR = "TrackBLR";
		    memoryPointTrackFRL = "TrackFRL";
		    memoryPointTrackFRR = "TrackFRR";
		    memoryPointTrackBRL = "TrackBRL";
		    memoryPointTrackBRR = "TrackBRR";

			TFAR_AdditionalLR_Turret[] = {{0,3},{1},{2}};

			threat[]={0.9,0.6,0.6};
			audible = 3;
			camouflage = 7;	
			
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1},{"Turret",{2}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1},{"Turret",{2}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1},{"Turret",{2}}};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 0;
			//Ende ACRE2

            hiddenSelections[] = 
            {
                
                "Wanne",
                "Anbauteile",
				"Reifen",
				"Anbauteile_Pi",

                "plate_vorne_1",
				"plate_vorne_2",
				"plate_vorne_3",
				"plate_vorne_4",
				"plate_vorne_5",
				"plate_vorne_6",
                "plate_hinten_1",
				"plate_hinten_2",
				"plate_hinten_3",
				"plate_hinten_4",
				"plate_hinten_5",
				"plate_hinten_6",

                "TakZeichen",
                "Bataillon",
				"Kompanie"
				
            };
			
            class PlayerSteeringCoefficients
		    {

			    turnIncreaseConst = 0.1;
			    turnIncreaseLinear = 1;
			    turnIncreaseTime = 1;

			    turnDecreaseConst = 1;
			    turnDecreaseLinear = 1; 
			    turnDecreaseTime = 0;

			    maxTurnHundred = 0.7;
				
            };

			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{
					
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Wanne.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Wanne_damage.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Wanne_destruct.rvmat",

					"Redd_Tank_Fuchs_1A4\mats\reflective_glass.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\reflective_glass_damage.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\reflective_glass_destruct.rvmat",

					"Redd_Tank_Fuchs_1A4\mats\glass.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\glass_damage.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\glass_destruct.rvmat",

					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Anbauteile.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Anbauteile_damage.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Anbauteile_destruct.rvmat",

					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Anbauteile_Pi.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Anbauteile_Pi_damage.rvmat",
					"Redd_Tank_Fuchs_1A4\mats\Redd_Tank_Fuchs_1A4_Anbauteile_Pi_destruct.rvmat"
					
				};
				
        	};

            class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};

            class Exhausts
		    {

			    class Exhaust1
			    {

				    position = "exhaust1_pos"; 
				    direction = "exhaust1_dir";
				    effect = "ExhaustsEffect";

			    };

		    };

            class Reflectors
		    {

                class Left
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
                    position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
                    size = 1;
                    innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
                    intensity = 1;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 1;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 30;
                        hardLimitEnd = 60;
                   
                    };

                };

                class Right: Left
			    {

                    position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";

			    };

				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };

				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};
				
				class Bluelight
				{

					color[]={100, 100, 1000};
					ambient[]={10,10,10};
					position="pos_blaulicht";
					direction="dir_blaulicht";
					hitpoint="";
					selection="pos_blaulicht";
					size=2;
					innerAngle=0;
					outerAngle=100;
					coneFadeCoef=2;
					intensity=1000;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Bluelight_Flare
				{

					color[]={100, 100, 1000};
					ambient[]={0,0,0};
					position="pos_blaulicht_flare";
					direction="dir_blaulicht_flare";
					hitpoint="";
					selection="pos_blaulicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=1000;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};
				
				class Orangelight
				{

					color[]={236, 99, 17};
					ambient[]={10,10,10};
					position="pos_orangelicht";
					direction="dir_orangelicht";
					hitpoint="";
					selection="pos_orangelicht";
					size=2;
					innerAngle=0;
					outerAngle=100;
					coneFadeCoef=2;
					intensity=250;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Orangelight_Flare
				{

					color[]={236, 99, 17};
					ambient[]={0,0,0};
					position="pos_orangelicht_flare";
					direction="dir_orangelicht_flare";
					hitpoint="";
					selection="pos_orangelicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=250;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};
				
			};

            class HitPoints: HitPoints
            {
				
                class HitEngine: HitEngine
                {

                    armor = 0.5;
                    material = -1;
                    name = "engine";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 0.2;
                    radius = 0.3;
					armorComponent="";

                };

                class HitFuel: HitFuel
                {

                    armor = 0.5;
                    material = -1;
                    name = "fuel";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 0.5;
                    radius = 0.3;
					armorComponent="";

                };

                class HitHull: HitHull
                {

                    armor = 2.0;
                    material = -1;
                    name = "karoserie";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 1.5;
                    radius = 0.3;
					armorComponent="";

                };

                class HitLFWheel: HitLFWheel
                {

                    armor = 1;
                    name = "wheel_1_1_steering";
					visual = "damage_LF_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

				class HitLF2Wheel: HitLF2Wheel
                {

                    armor = 1;
                    name = "wheel_1_2_steering";
					visual = "damage_LF2_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

                class HitLMWheel: HitLMWheel
                {

                    armor = 1;
                    name = "wheel_1_3_steering";
					visual = "damage_LM_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;
                };

				class HitRFWheel: HitRFWheel
                {

                    armor = 1;
                    name = "wheel_2_1_steering";
					visual = "damage_RF_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

				class HitRF2Wheel: HitRF2Wheel
                {

                    armor = 1;
                    name = "wheel_2_2_steering";
					visual = "damage_RF2_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };
                
                class HitRMWheel: HitRMWheel
                {

                    armor = 1;
                    name = "wheel_2_3_steering";
					visual = "damage_RM_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

				class HitGlass1: HitGlass1 
				{
					name = "glass1";
					visual = "glassF";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;

				};
                class HitGlass2: HitGlass2
				{
					name = "glass2";
					visual = "glassFS";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;
					
				};
                class HitGlass3: HitGlass3 
				{

					name = "glass3";
					visual = "glassPFS";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;

				};
                class HitGlass4: HitGlass4 
				{
					name = "glass4";
					visual = "glassB";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;

				};
                class HitGlass5: HitGlass5 
				{

					name = "glass5";
					visual = "glassPBS";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;

				};
				class HitGlass6: HitGlass6 
				{
					
					name = "glass6";
					visual = "glassBS";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;
					
				};

            };

			class TransportMagazines {};

			class TransportWeapons {};

            class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

			};

            class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};

            class MFD 
		    {

			    class ClockHUD
			    {

			        #include "cfgHUD.hpp"

			    };
		    };
            
            class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret 
				{

                    class Turrets: Turrets 
					{

                        class commander_hatch: NewTurret //Luke_Kommandant [0,0]
                        {
                            
                            body = "";
                            gun = "";
                            animationSourceBody = "";
                            animationSourceGun = "";
                            animationSourceHatch = "commander_hatch_source";
                            enabledByAnimationSource = "commander_hatch_1";
                            proxyType= "CPGunner";
                            proxyIndex=1;
                            gunnerName="$STR_Kommandantenplatz";
                            primaryGunner = 0;
                            primaryObserver = 1;
                            gunnerGetInAction="GetInMedium";
                            gunnerGetOutAction="GetOutMedium";
                            gunnerForceOptics = 0;
							inGunnerMayFire=0;
                            viewGunnerInExternal = 1;
                            gunnerCompartments= "Compartment2";
                            LODTurnedIn = 1200; //Cargo 1200
                            LODTurnedOut= 1000; //Gunner 1000
                            startEngine = 0;
                            dontCreateAi = 1;
                            gunnerInAction = "Redd_Tank_Fuchs_Passenger_1";
							gunnerAction  = "Redd_Tank_Fuchs_Milan_2";
                            memoryPointGunnerOptics = "";
                            memoryPointsGetInGunner = "pos cargo";
                            memoryPointsGetInGunnerDir = "pos cargo dir";
                            soundAttenuationTurret = "TankAttenuation";
                            disableSoundAttenuation = 0;
							hideWeaponsGunner = 1;

                            class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};
							
                        };
                        
                        class ffv_hatch_R: commander_hatch //right [0,1]
                        {
                            
							gunnerInAction = "Redd_Tank_Fuchs_Passenger_1_2";
                           	gunnerName="$STR_Rechte_Luke";
                            animationSourceHatch = "ffv_hatch_R_source";
                            enabledByAnimationSource = "ffv_hatch_R";
                            proxyIndex=3;
                            primaryObserver = 0;
                            commanding = 0;
							isPersonTurret = 1;
							gunnerAction  = "vehicle_turnout_2";
							hideWeaponsGunner = 0;

                           	class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};

                            class TurnOut: TurnIn
                            {

                                limitsArrayTop[]=
                                {

									{30, -90},
									{30, 90}
                                  
                                };

                                limitsArrayBottom[]=
                                {

                                   	{-16, -90},
									{-15, -60},
									{-25, 0},
									{-7, 85},
									{-7, 86},
									{-7, 90}

                                };
                            
                            };
                        
                        };

                        class ffv_hatch_L: commander_hatch //left [0,2]
                        {
                            
                            gunnerInAction = "Redd_Tank_Fuchs_Passenger_1_1";
                            gunnerName="$STR_Linke_Luke";
                            animationSourceHatch = "ffv_hatch_L_source";
                            enabledByAnimationSource = "ffv_hatch_L";
                            proxyIndex=4;
                            primaryObserver = 0;
                            commanding = 0;
							isPersonTurret = 1;
							gunnerAction  = "vehicle_turnout_2";
							hideWeaponsGunner = 0;

                            class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};
                            
                            class TurnOut: TurnIn
                            {

                                limitsArrayTop[]=
                                {

									{30, -90},
									{30, 90}
                                  
                                };

                                limitsArrayBottom[]=
                                {

                                   	{-5.5, -90},
									{-5.5, -80},
									{-25, 90}

                                };
                            
                            };
                        
                        };

                        class fake_gunner_turret: commander_hatch // [0,3]
                        {
							
							gunnerInAction="Redd_Tank_Fuchs_Co_Driver_TurnedIn";
                            gunnerAction="Redd_Tank_Fuchs_Co_Driver_TurnedOut_2";
							animationSourceHatch = "maingunner_hatch_source";
                            enabledByAnimationSource = "maingunner_hatch";
                            gunnerName="$STR_Beifahrer";
                            memoryPointsGetInGunner = "pos gunner";
                            memoryPointsGetInGunnerDir = "pos gunner dir";
                            proxyIndex=5;
                            primaryObserver = 0;
                            commanding = 0;
                            gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
                            LODTurnedIn = 1100; //Pilot 1100
                            LODTurnedOut= 1000; //Gunner 1000
                            gunnerCompartments= "Compartment1";
							memoryPointGunnerOptics= "gunnerview";
							hideWeaponsGunner = 1;
							turretInfoType = "Redd_RCS_Driver_APC";

							class ViewOptics: ViewOptics 
							{
				
								initFov = 0.75;
								maxFov = 0.75;
								minFov = 0.75;
								visionMode[] = {"Normal","NVG"};
				
							};

							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};
                            
                            class TurnOut: TurnIn
                            {

                                limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};
                            
                            };

						};

                    };

                    //Mainturret MG3 [0]
                    weapons[] = {"Redd_MG3"};
                    magazines[] = 
                    {

                        "Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120"
                    
                    };

                    gunnerName="Fake_Beifahrer";
                    proxyIndex = 2;
					gunnerAction="Redd_Tank_Fuchs_Co_Driver_TurnedOut";
					stabilizedInAxes = 3;
                    viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetOutAction="GetOutMedium";
                    commanding = 0;
                    primaryGunner = 1;
                    primaryObserver = 0;
                    dontCreateAi = 1;
					gunnerOutOpticsModel = "\A3\weapons_f\reticle\optics_empty";
					memoryPointGunnerOutOptics= "gunnerview2";
					turretInfoType="Redd_RSC_MG3";
                    discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
					discreteDistanceInitIndex = 4;
                    gunnerRightHandAnimName = "RecoilHlaven";
				    gunnerLeftHandAnimName = "otocHlaven";
					memoryPointsGetInGunner = "pos gunner";
                    memoryPointsGetInGunnerDir = "pos gunner dir";
					canHideGunner = 0; //0 für GunnerOut 
					forceHideGunner = 1;
					gunnerCompartments= "Compartment3";
					LODTurnedOut = 1000; //Gunner 1000
					lodTurnedIn = 1000; //Gunner 1000
                    initElev = 0;
					minElev = -20;
					maxElev = 50;
					initTurn = 0;
					minTurn = -360;
					maxTurn = 360;
					disableSoundAttenuation = 1;
					outGunnerMayFire = 1;
					startEngine = 0;

					class TurnIn
					{

						limitsArrayTop[]=
						{

							{50, -180},
							{50, 180}
							
						};

						limitsArrayBottom[]=
						{

							{-5, -180},
							{-20, 0},
							{-5, 180}

						};
					
					};

                    class ViewOptics: ViewOptics 
			        {
				
				        initFov = 0.4;
			        	maxFov = 0.5;
				        minFov = 0.1;
				        visionMode[] = {"Normal","NVG"};
				
			        };

					class OpticsOut //So addaptieren für ändern für Gunner Out
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 1.25;
							minFov = 0.25;
							visionMode[] = {};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};

					};

					class OpticsIn
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.4;
							maxFov = 0.4;
							minFov = 0.4;
							visionMode[] = {"Normal", "NVG"};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Narrow
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.2;
							maxFov = 0.2;
							minFov = 0.2;
							visionMode[] = {"Normal", "NVG"};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};

						};
						
					};
					
                };
				
				class Fuchs_Bino_Turret_Com: NewTurret //[1]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 1;
					proxyType= "CPGunner";
					gunnerName="Bino_Com";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutMedium";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment4";
					LODTurnedIn = 1200; //Cargo
					LODTurnedOut= 1000; //Gunner
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="rnt_fuchs_com_out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="rnt_fuchs_com_out_ffv";
					memoryPointsGetInGunner = "pos cargo";
                    memoryPointsGetInGunnerDir = "pos cargo dir";

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};

				class Fuchs_Milan_Turret: NewTurret //[2]
				{

					body = "Milan_Rot_X";
					gun = "Milan_Rot_Y";
					animationSourceBody = "Milan_Rot_X_source";
					animationSourceGun = "Milan_Rot_Y_source";
					proxyindex = 6;
					gunnerName="Fake_Milan";
					primaryGunner = 0;
					primaryObserver = 1;
					weapons[] ={"Redd_Milan"};
					magazines[] = 
					{	
					
						"Redd_Milan_Mag",
						"Redd_Milan_Mag",
						"Redd_Milan_Mag",
						"Redd_Milan_Mag"
				
					};
					minElev = -15;
					maxElev = 10;
					initElev = 0;
					minTurn = -25;
					maxTurn = 35;
					initTurn = 0;
					stabilizedInAxes = 3;
					gunnerGetOutAction = "GetOutMedium";
					turretInfoType = "Redd_RSC_Milan";
					gunnerForceOptics=0;
					canHideGunner = 0; //0 für GunnerOut 
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment4";
					lodTurnedOut = 1000; //Gunner
					startEngine = 0;	
					gunnerRightHandAnimName = "Milan_Handle";
					gunnerLeftHandAnimName = "Milan_Gun";
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerAction="Redd_Tank_Fuchs_Milan";
					memoryPointGunnerOutOptics = "Milan_View";
					memoryPointsGetInGunner = "pos cargo";
                    memoryPointsGetInGunnerDir = "pos cargo dir";
					ace_fcs_Enabled = 0;

					outGunnerMayFire = 1; //Rein für Gunner Out
					gunnerOutOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1"; //Rein für Gunner Out

					class OpticsOut //So addaptieren für ändern für Gunner Out
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 1.25;
							minFov = 0.25;
							visionMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};

					};

					class OpticsIn
					{
						
						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.15;
							maxFov = 0.15;
							minFov = 0.15;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.037;
							maxFov = 0.037;
							minFov = 0.037;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.15;
							maxFov = 0.15;
							minFov = 0.15;
							visionMode[] = {"Ti"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_W1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.037;
							maxFov = 0.037;
							minFov = 0.037;
							visionMode[] = {"Ti"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_W1";
							gunnerOpticsEffect[] = {};
										
						};
						
					};
					
				};
				
            };

            class AnimationSources
		    {

                class commander_hatch_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 1;

                };

                class pio_luke_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 1;

                };

                class maingunner_hatch_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 1;
                
                };

                class ffv_hatch_L_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 1;
                
                };

                class ffv_hatch_R_source: ffv_hatch_L_source {};

                class door1_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;


                };

                class door2_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

                class door3_Source
                {

                   source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

                class Bullet_shield_Source
                {

                    source = user;
                    initPhase = 1;
                    animPeriod = 3;

                };

                class water_shield_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

                class ReloadAnim
                {

                    source="reload";
                    weapon = "Redd_MG3";

                };

			    class ReloadMagazine
                {

                    source="reloadmagazine";
                    weapon = "Redd_MG3";
                    
                };
                
                class hide_Jg_Luke_1 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_2 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_3
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class hide_Pi_Luke_1 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class hide_Pi_Luke_2 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class hide_pio_torten 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
                class hide_Int_Jg_Proxy 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class hide_Int_Pi_Proxy 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class hide_Int_San_Proxy 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Milan_Hide_Turret
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class Milan_Hide_Source 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Milan_Hide_Rohr 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
				
				class HitLFWheel
				{

					source = "Hit";
					hitpoint = "HitLFWheel";
					raw = 1;
				};
				class HitLF2Wheel: HitLFWheel{hitpoint = "HitLF2Wheel";};
				class HitLMWheel: HitLFWheel{hitpoint = "HitLMWheel";};
				class HitRFWheel: HitLFWheel{hitpoint = "HitRFWheel";};
				class HitRF2Wheel: HitLFWheel{hitpoint = "HitRF2Wheel";};
				class HitRMWheel: HitLFWheel{hitpoint = "HitRMWheel";};

				class HitGlass1
				{

					source = "Hit";
					hitpoint = "HitGlass1";

				};
				class HitGlass2: HitGlass1 {hitpoint = "HitGlass2";};
				class HitGlass3: HitGlass1 {hitpoint = "HitGlass3";};
				class HitGlass4: HitGlass1 {hitpoint = "HitGlass4";};
				class HitGlass5: HitGlass1 {hitpoint = "HitGlass5";};
				class HitGlass6: HitGlass1 {hitpoint = "HitGlass6";};
				
				//Rundumleuchte Hide
				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_fuss_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class Beacons
				{

					source = "user";
					animPeriod = 1;
					initPhase = 0;

				};
				class turn_left
            	{

					source = "user";
					animPeriod = 0.001;
					initPhase = 0;

           		};

            	class blaulicht_an: turn_left{};

				class orangelicht_an: turn_left{};

				class blaulicht_an_flare: turn_left{};

				class orangelicht_an_flare: turn_left{};

				//MG3 Hide
				class Hide_Fake_MG_Fuss
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_MG_Fuss
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_otocHlaven
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_RecoilHlaven
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_magazine
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_feedtray_cover
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_ammo_belt
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class San_hide
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class ReloadMagazine_2
                {

                    source="reloadmagazine";
                    weapon = "Redd_Milan";
                    
                };

				class flash_mg3_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MG3";
					initPhase = 0;
					
				};

				//Netze
				class fuchsBodenFleck_hide_source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

				class fuchsWanneFleck_hide_source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

				class fuchsBodenTrope_hide_source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

				class fuchsWanneTrope_hide_source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

				class fuchsBodenWinter_hide_source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

				class fuchsWanneWinter_hide_source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

		    };
			
            class UserActions
		    {

                class Driver_Shield_Close
                {

                    displayName = "$STR_Beschussschild_schliessen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "player == (driver this) && (alive this) && (this animationSourcePhase 'Bullet_shield_Source') < 0.5";
                    statement = "this animateSource ['Bullet_shield_Source',1]";
                    
                };

                class Driver_Shield_Open: Driver_Shield_Close
                {

                    displayName = "$STR_Beschussschild_oeffnen";
                    condition = "player == (driver this) && (alive this) && (this animationSourcePhase 'Bullet_shield_Source') > 0.5";
                    statement = "this animateSource ['Bullet_shield_Source',0]";

                };

                class Driver_Door_Open
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(surfaceIsWater position this) and (player in [driver this]) and (this animationSourcePhase 'door1_Source' == 0) and (alive this)";
					statement = "this animateSource ['door1_Source', 1];";

                };

                class Driver_Door_Close
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'door1_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['door1_Source', 0];";

                };

                class Driver_Door_Open_ext
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "open1_door_ext";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(surfaceIsWater position this) and !(player in this) and (this animationSourcePhase 'door1_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['door1_Source', 1];";

                };

                class Driver_Door_Close_ext
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "open1_door_ext";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door1_Source' > 0) and (alive this)";
					statement = "this animateSource ['door1_Source', 0];";

                };

                class Co_Driver_Door_Open
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(surfaceIsWater position this) and (this turretUnit [0,3] == player) and (this animationSourcePhase 'door2_Source' == 0) and (this animationSourcePhase 'maingunner_hatch_source' == 0) and (alive this)";
					statement = "this animateSource ['door2_Source', 1];";

                };

                class Co_Driver_Door_Close
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,3] == player) and (this animationSourcePhase 'door2_Source' > 0) and (this animationSourcePhase 'maingunner_hatch_source' == 0) and (alive this)"; 
					statement = "this animateSource ['door2_Source', 0];";

                };

                class Co_Driver_Door_Open_ext
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "open2_door_ext";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(surfaceIsWater position this) and !(player in this) and (this animationSourcePhase 'door2_Source' == 0) and (alive this)";
					statement = "this animateSource ['door2_Source', 1];";

                };

                class Co_Driver_Door_Close_ext
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "open2_door_ext";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door2_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['door2_Source', 0];";

                };

				//Cargo open
                class Rear_Door_Open_1 //Cargo
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(surfaceIsWater position this) and (player in this) and (this animationSourcePhase 'door3_Source' == 0) and !(player in [driver this]) and !(this turretUnit [0] == player) and !(this turretUnit [0,0] == player) and !(this turretUnit [0,1] == player) and !(this turretUnit [0,2] == player) and !(this turretUnit [0,3] == player) and !(this turretUnit [1] == player) and (alive this)";
					statement = "this animateSource ['door3_Source', 1];";

                };
				//Cargo close
                class Rear_Door_Close_1 //Cargo
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door3_Source' == 1) and !(player in [driver this]) and !(this turretUnit [0] == player) and !(this turretUnit [0,0] == player) and !(this turretUnit [0,1] == player) and !(this turretUnit [0,2] == player) and !(this turretUnit [0,3] == player) and !(this turretUnit [1] == player) and (alive this)";
					statement = "this animateSource ['door3_Source', 0];";

                };
				class Rear_Door_Open_2: Rear_Door_Open_1 //Commander
                {

					condition = "!(surfaceIsWater position this) and (this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' == 0) and (this animationSourcePhase 'door3_Source' == 0) and (alive this)";
					

				};
				class Rear_Door_Close_2: Rear_Door_Close_1 //Commander
                {

					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' == 0) and (this animationSourcePhase 'door3_Source' == 1) and (alive this)";
					
				};
				class Rear_Door_Open_3: Rear_Door_Open_1 //Hatch left
                {

					condition = "!(surfaceIsWater position this) and (this turretUnit [0,1] == player) and (this animationSourcePhase 'ffv_hatch_L_source' == 0) and (this animationSourcePhase 'door3_Source' == 0) and (alive this)";
					

				};
				class Rear_Door_Close_3: Rear_Door_Close_1 //Hatch left
                {

					condition = "(this turretUnit [0,1] == player) and (this animationSourcePhase 'ffv_hatch_L_source' == 0) and (this animationSourcePhase 'door3_Source' == 1) and (alive this)";
					
				};
				class Rear_Door_Open_4: Rear_Door_Open_1 //Hatch left
                {

					condition = "!(surfaceIsWater position this) and (this turretUnit [0,2] == player) and (this animationSourcePhase 'ffv_hatch_R_source' == 0) and (this animationSourcePhase 'door3_Source' == 0) and (alive this)";
					

				};
				class Rear_Door_Close_4: Rear_Door_Close_1 //Hatch left
                {

					condition = "(this turretUnit [0,2] == player) and (this animationSourcePhase 'ffv_hatch_R_source' == 0) and (this animationSourcePhase 'door3_Source' == 1) and (alive this)";
					
				};

                class Rear_Door_Open_ext
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "oper3_door_ext";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(surfaceIsWater position this) and !(player in this) and (this animationSourcePhase 'door3_Source' == 0) and (alive this)";
					statement = "this animateSource ['door3_Source', 1];";

                };

                class Rear_Door_Close_ext
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "oper3_door_ext";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door3_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['door3_Source', 0];";

                };

                class water_shield_Open
                {

                    displayName = "$STR_Schwallbrett_ausklappen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'water_shield_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['water_shield_Source', 1];";

                };

                class water_shield_Close
                {

                    displayName = "$STR_Schwallbrett_einklappen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'water_shield_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['water_shield_Source', 0];";

                };
               
                class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class MG3_in
				{
					
					displayName = "$STR_MG3_benutzen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'maingunner_hatch' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0]];[this,[[0,3],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_MG3_In', true, true];"; 
				
				};

				class MG3_out
				{
					
					displayName = "$STR_MG3_verlassen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,3]];[this,[[0,3],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_MG3_In', false, true];"; 
				
				};

				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_Bino_In', false, true];"; 
				
				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class Orangelicht_an_2
				{
					
					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_aufbauen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 1) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animate ['Hide_Rundumleuchte_fuss_2',0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abbauen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animate ['Hide_Rundumleuchte_fuss_2',1];";

				};

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_fuchs_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_fuchs_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_fuchs_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,3] call Redd_fnc_fuchs_flags";

				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_fuchs_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_fuchs_camonet";

				};

				class Tarnnetz_Boden_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_fuchs_camonet";

				};

				class Tarnnetz_Boden_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and (this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_fuchs_camonet";

				};
				
				class fixTurretBug
				{

					displayName = "Fix GetIn Bug"; 
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(alive this)";
					statement = "if (this turretUnit [0] isEqualTo objNull) then {this setVariable ['Redd_Fuchs_MG3_In', false, true];[this,[[0,3],false]] remoteExecCall ['lockTurret'];};if (this turretUnit [1] isEqualTo objNull) then {this setVariable ['Redd_Fuchs_Bino_In', false, true];[this,[[0,0],false]] remoteExecCall ['lockTurret'];};if (this turretUnit [2] isEqualTo objNull) then {this setVariable ['Redd_Fuchs_Milan_In', false, true];[this,[[0,0],false]] remoteExecCall ['lockTurret'];};";
				};
		    };
			
            class EventHandlers: EventHandlers 
            {

               	init = "_this call redd_fnc_Fuchs_init";
               	getOut = "_this call redd_fnc_Fuchs_GetOut";
				getIn = "_this call redd_fnc_Fuchs_GetIn";
				fired = "_this call redd_fnc_Fuchs_Fired";

            };

        };

        //Fuchs_1A4_JgGrp
        class Redd_Tank_Fuchs_1A4_Jg_Flecktarn: Redd_Tank_Fuchs_1A4_Base
		{
			
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_JG_W.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Fuchs_1A4_Jg_Flecktarn";

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Fuchs_1A4_Jg_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Fuchs_1A4_Jg_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Fuchs_1A4_Jg_Wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

            class AnimationSources: AnimationSources
		    {

                class hide_Jg_Luke_1 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_2 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_3
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Int_Jg_Proxy 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

            };

			class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret 
				{

                    class Turrets: Turrets 
					{

                        class commander_hatch: commander_hatch //Luke_Kommandant [0,0]
                        {};

                        class ffv_hatch_R: ffv_hatch_R //right [0,1]
						{};

                        class ffv_hatch_L: ffv_hatch_L //left [0,2]
                        {};

                        class fake_gunner_turret: fake_gunner_turret// [0,3]
                        {};

                    };

                };

				class Fuchs_Bino_Turret_Com: Fuchs_Bino_Turret_Com //[1]
				{};

            };

            class Attributes 
			{

                class Redd_Tank_Fuchs_1A4_Jg_Waffengattung_Attribute 
				{
					
					displayName = "$STR_Waffengattung";
					tooltip = "$STR_Waffengattung";
					property = "Redd_Tank_Fuchs_1A4_Jg_Waffengattung_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Waffengattung', _value];";
					defaultValue = "0";
					typeName = "STRING";

                    class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Jg
						{
							
							name = "Jaeger";
							value = "1";
							
						};

                        class GebJg 
						{
							
							name = "Gebirgsjaeger";
							value = "2";
							
						};

                        class PzAufkl 
						{
							
							name = "Panzeraufklaerer";
							value = "3";
							
						};

                    };

                };
				
				class Redd_Tank_Fuchs_1A4_Jg_Bataillon_Attribute 
				{
					
					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Tank_Fuchs_1A4_Jg_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_1 
						{
							
							name = "JgBtl 1";
							value = "1";
							
						};

						class Bataillon_91
						{
							
							name = "JgBtl 91";
							value = "2";
							
						};

						class Bataillon_291 
						{
							
							name = "JgBtl 291";
							value = "3";
							
						};

						class Bataillon_292 
						{
							
							name = "JgBtl 292";
							value = "4";
							
						};
						
						class Bataillon_413 
						{
							
							name = "JgBtl 413";
							value = "5";
							
						};

                        class Bataillon_231
						{
							
							name = "GebJgBtl 231";
							value = "6";
							
						};

                        class Bataillon_232 
						{
							
							name = "GebJgBtl 232";
							value = "7";
							
						};

                        class Bataillon_233
						{
							
							name = "GebJgBtl 233";
							value = "8";
							
						};

                        class Bataillon_5
						{
							
							name = "PzAufklBtl 5";
							value = "9";
							
						};

                        class Bataillon_6
						{
							
							name = "PzAufklBtl 6";
							value = "10";
							
						};

                        class Bataillon_7
						{
							
							name = "PzAufklBtl 7";
							value = "11";
							
						};

                        class Bataillon_8
						{
							
							name = "PzAufklBtl 8";
							value = "12";
							
						};

                        class Bataillon_12
						{
							
							name = "PzAufklBtl 12";
							value = "13";
							
						};

                        class Bataillon_13
						{
							
							name = "PzAufklBtl 13";
							value = "14";
							
						};
						
					};
					
				};
				
				class Redd_Tank_Fuchs_1A4_Jg_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Fuchs_1A4_Jg_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

                        class Kompanie_5
						{
							
							name = "5";
							value = "5";
							
						};

					};
					
				};

            };

		};

		class Redd_Tank_Fuchs_1A4_Jg_Tropentarn: Redd_Tank_Fuchs_1A4_Jg_Flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_JG_D.paa";
			displayName="$STR_Fuchs_1A4_Jg_Tropentarn";

			hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa"

            };

		};

		class Redd_Tank_Fuchs_1A4_Jg_Wintertarn: Redd_Tank_Fuchs_1A4_Jg_Flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_JG_Wi.paa";
			displayName="$STR_Fuchs_1A4_Jg_Wintertarn";

			hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa"

            };
			/*
			class EventHandlers: EventHandlers 
            {

                init = "_this call redd_fnc_Fuchs_W_init";

            };
			*/
		};
        
        //Fuchs_1A4_JgGrp_Milan
        class Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn: Redd_Tank_Fuchs_1A4_base
		{

            editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_Mi_W.paa";
            scope=2;
            scopeCurator = 2;
			forceInGarage = 1;
			displayName="$STR_Fuchs_1A4_Jg_Milan_Flecktarn";

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Fuchs_1A4_Jg_Milan_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Fuchs_1A4_Jg_Milan_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Fuchs_1A4_Jg_Milan_Wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};
			
            class AnimationSources: AnimationSources
            {

                class hide_Jg_Luke_1 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_2 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_3
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Int_Jg_Proxy 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class Milan_Hide_Turret 
                {
                    
                    source = "user";
                    initPhase = 0;
                    animPeriod = 0;
                    
                };
                
            };

            class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret 
				{

                    class Turrets: Turrets 
					{

                        class commander_hatch: commander_hatch //Luke_Kommandant [0,0]
                        {};

                        class ffv_hatch_R: ffv_hatch_R //right [0,1]
						{};

                        class ffv_hatch_L: ffv_hatch_L //left [0,2]
                        {};

                        class fake_gunner_turret: fake_gunner_turret// [0,3]
                        {};

                    };

                };

				class Fuchs_Bino_Turret_Com: Fuchs_Bino_Turret_Com //[1]
				{};

				class Fuchs_Milan_Turret: Fuchs_Milan_Turret //[2]
				{};


            };

            class Attributes 
			{

                class Redd_Tank_Fuchs_1A4_Jg_Waffengattung_Attribute 
				{
					
					displayName = "$STR_Waffengattung";
					tooltip = "$STR_Waffengattung";
					property = "Redd_Tank_Fuchs_1A4_Jg_Waffengattung_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Waffengattung', _value];";
					defaultValue = "0";
					typeName = "STRING";

                    class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Jg
						{
							
							name = "Jaeger";
							value = "1";
							
						};

                        class GebJg 
						{
							
							name = "Gebirgsjaeger";
							value = "2";
							
						};

                        class PzAufkl 
						{
							
							name = "Panzeraufklaerer";
							value = "3";
							
						};

                    };

                };
				
				class Redd_Tank_Fuchs_1A4_Jg_Bataillon_Attribute 
				{
					
					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Tank_Fuchs_1A4_Jg_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_1 
						{
							
							name = "JgBtl 1";
							value = "1";
							
						};

						class Bataillon_91
						{
							
							name = "JgBtl 91";
							value = "2";
							
						};

						class Bataillon_291 
						{
							
							name = "JgBtl 291";
							value = "3";
							
						};

						class Bataillon_292 
						{
							
							name = "JgBtl 292";
							value = "4";
							
						};
						
						class Bataillon_413 
						{
							
							name = "JgBtl 413";
							value = "5";
							
						};

                        class Bataillon_231
						{
							
							name = "GebJgBtl 231";
							value = "6";
							
						};

                        class Bataillon_232 
						{
							
							name = "GebJgBtl 232";
							value = "7";
							
						};

                        class Bataillon_233
						{
							
							name = "GebJgBtl 233";
							value = "8";
							
						};

                        class Bataillon_5
						{
							
							name = "PzAufklBtl 5";
							value = "9";
							
						};

                        class Bataillon_6
						{
							
							name = "PzAufklBtl 6";
							value = "10";
							
						};

                        class Bataillon_7
						{
							
							name = "PzAufklBtl 7";
							value = "11";
							
						};

                        class Bataillon_8
						{
							
							name = "PzAufklBtl 8";
							value = "12";
							
						};

                        class Bataillon_12
						{
							
							name = "PzAufklBtl 12";
							value = "13";
							
						};

                        class Bataillon_13
						{
							
							name = "PzAufklBtl 13";
							value = "14";
							
						};
						
					};
					
				};
				
				class Redd_Tank_Fuchs_1A4_Jg_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Fuchs_1A4_Jg_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

                        class Kompanie_5
						{
							
							name = "5";
							value = "5";
							
						};

					};
					
				};

            };

			class UserActions: UserActions
		    {

				class Milan_auf
				{
					
					displayName = "$STR_milan_auf";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' > 0) and (this animationSourcePhase 'Milan_Hide_Source' > 0) and (alive this)";
					statement = "this animateSource ['Milan_Hide_Source',0];this animate ['Milan_Hide_Rohr',0];";
					
				};

				class Milan_ab
				{
					
					displayName = "$STR_milan_ab";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' > 0) and (this animationSourcePhase 'Milan_Hide_Source' == 0) and (alive this)";
					statement = "this animateSource ['Milan_Hide_Source',1];this animate ['Milan_Hide_Rohr',1];";
					
				};

				class milan_in
				{
					
					displayName = "$STR_milan_in";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'commander_hatch_source' > 0) and (this animationSourcePhase 'Milan_Hide_Source' < 1) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [2]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_Milan_In', true, true];";
					
				};

				class milan_aus
				{
					
					displayName = "$STR_milan_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [2] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_Milan_In', false, true];";
					
				};

			};

			class TransportBackpacks: TransportBackpacks
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

				class _xx_milan_Bag
				{
					
					backpack = "Redd_Milan_Static_Barrel";
					count = 1;
					
				};

				class _xx_milan_tripod
				{
					
					backpack = "Redd_Milan_Static_Tripod";
					count = 1;
					
				};

			};

        };

        class Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn: Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn
		{
			
			scopeArsenal = 0;
			forceInGarage = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_Mi_D.paa";
			displayName="$STR_Fuchs_1A4_Jg_Milan_Tropentarn";

			hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa"

            };

		};

		class Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn: Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn 
		{
			
			scopeArsenal = 0;
			forceInGarage = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_Mi_Wi.paa";
			displayName="$STR_Fuchs_1A4_Jg_Milan_Wintertarn";

			hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa"

            };
			/*
			class EventHandlers: EventHandlers 
            {

               	init = "_this call redd_fnc_Fuchs_W_init";

            };
			*/
		};
        
        //Fuchs_1A4_PiGrp
        class Redd_Tank_Fuchs_1A4_Pi_Flecktarn: Redd_Tank_Fuchs_1A4_base
		{
			
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_Pi_W.paa";
			scope=2;
            scopeCurator = 2;
			forceInGarage = 1;
			displayName="$STR_Fuchs_1A4_Pi_Flecktarn";
			transportSoldier = 3;
			cargoProxyIndexes[] = {3,5,6};

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_Pi_blend_co.paa"
				
            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Fuchs_1A4_Pi_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Anbauteile_Pi_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Fuchs_1A4_Pi_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_Pi_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Fuchs_1A4_Pi_Wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_Pi_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

			class AnimationSources: AnimationSources
		    {

                class hide_Pi_Luke_1 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Pi_Luke_2 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_pio_torten 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class hide_Int_Pi_Proxy 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

            };

			class UserActions: UserActions
			{

				class Rear_Door_Open_2: Rear_Door_Open_2
                {

					condition = "false";

				};

				class Rear_Door_Close_2: Rear_Door_Close_2
                {

					condition = "false";

				};

				class Rear_Door_Open_5: Rear_Door_Open_1 //Pio
                {

					condition = "!(surfaceIsWater position this) and (this turretUnit [0,0] == player) and (this animationSourcePhase 'pio_luke_source' == 0) and (this animationSourcePhase 'door3_Source' == 0) and (alive this)";
					

				};
				class Rear_Door_Close_5: Rear_Door_Close_1 //Pio
                {

					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'pio_luke_source' == 0) and (this animationSourcePhase 'door3_Source' == 1) and (alive this)";
					
				};

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_aufbauen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'pio_luke_source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 1)and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animate ['Hide_Rundumleuchte_fuss_2',0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abbauen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'pio_luke_source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animate ['Hide_Rundumleuchte_fuss_2',1];";

				};

				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'pio_luke_source' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Fuchs_Bino_In', false, true];"; 
				
				};


			};

            class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret 
				{

                    class Turrets: Turrets 
					{

                        class commander_hatch: commander_hatch //Luke_Kommandant [0,0]
                        {

                            animationSourceHatch = "pio_luke_source";
                            enabledByAnimationSource = "pio_luke";
							gunnerAction="Redd_Tank_Fuchs_Pi";
							gunnerInAction = "Redd_Tank_Fuchs_Pi_In";
							proxyIndex=7;

                        };
						
                        class ffv_hatch_R: ffv_hatch_R //right [0,1]
                        {};

                        class ffv_hatch_L: ffv_hatch_L //left [0,2]
                        {};

                        class fake_gunner_turret: fake_gunner_turret// [0,3]
                        {};

                    };

                };

				class Fuchs_Bino_Turret_Com: Fuchs_Bino_Turret_Com //[1]
				{
					proxyIndex=7;
					personTurretAction="rnt_fuchs_pi_out_ffv";
				};

            };

			class Attributes 
			{

                class Redd_Tank_Fuchs_1A4_Pi_Waffengattung_Attribute 
				{
					
					displayName = "$STR_Waffengattung";
					tooltip = "$STR_Waffengattung";
					property = "Redd_Tank_Fuchs_1A4_Pi_Waffengattung_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Pi_1A4_Waffengattung', _value];";
					defaultValue = "0";
					typeName = "STRING";

                    class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Pi
						{
							
							name = "Pioniere";
							value = "1";
							
						};

                        class PzPi 
						{
							
							name = "Panzerpioniere";
							value = "2";
							
						};

						class GebPi 
						{
							
							name = "Gebirgspioniere";
							value = "3";
							
						};

                    };

                };
				
				class Redd_Tank_Fuchs_1A4_Pi_Bataillon_Attribute 
				{
					
					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Tank_Fuchs_1A4_Pi_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Pi_1A4_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_905
						{
							
							name = "PiBtl 905";
							value = "1";
							
						};

						class Bataillon_901
						{
							
							name = "PiBtl 901";
							value = "2";
							
						};

						class Bataillon_130
						{
							
							name = "PzPiBtl 130";
							value = "3";
							
						};

						class Bataillon_1 
						{
							
							name = "PzPiBtl 1";
							value = "4";
							
						};
						
						class Bataillon_803
						{
							
							name = "PzPiBtl 803";
							value = "5";
							
						};

                        class Bataillon_4
						{
							
							name = "PzPiBtl 4";
							value = "6";
							
						};

                        class Bataillon_701 
						{
							
							name = "PzPiBtl 701";
							value = "7";
							
						};

                        class Bataillon_8
						{
							
							name = "GebPiBtl 8";
							value = "8";
							
						};

					};
					
				};
				
				class Redd_Tank_Fuchs_1A4_Jg_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Fuchs_1A4_Jg_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Fuchs_Jg_1A4_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

                        class Kompanie_5
						{
							
							name = "5";
							value = "5";
							
						};

					};
					
				};
			
			};

		};

        class Redd_Tank_Fuchs_1A4_Pi_Tropentarn: Redd_Tank_Fuchs_1A4_Pi_Flecktarn
		{
			
			scopeArsenal = 0;
			forceInGarage = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_Pi_D.paa";
			displayName="$STR_Fuchs_1A4_Pi_Tropentarn";

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Anbauteile_Pi_blend_co.paa"

            };

		};

        class Redd_Tank_Fuchs_1A4_Pi_Wintertarn: Redd_Tank_Fuchs_1A4_Pi_Flecktarn
		{
			
			scopeArsenal = 0;
			forceInGarage = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_Pi_Wi.paa";
			displayName="$STR_Fuchs_1A4_Pi_Wintertarn";

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Anbauteile_Pi_blend_co.paa"

            };
			/*
			class EventHandlers: EventHandlers 
            {

               	init = "_this call redd_fnc_Fuchs_W_init";

            };
			*/
		};
        
        //Fuchs_1A4_SanGrp
		class Redd_Tank_Fuchs_1A4_San_Flecktarn: Redd_Tank_Fuchs_1A4_base
		{
			
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_San_W.paa";
			scope=2;
            scopeCurator = 2;
			forceInGarage = 1;
			displayName="$STR_Fuchs_1A4_San_Flecktarn";
			transportSoldier = 4;
			cargoProxyIndexes[] = {7,8,9,10};
			cargoAction[] = {"Redd_Tank_Fuchs_Passenger_2_San","Redd_Tank_Fuchs_Passenger_3_San"};

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_San_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_San_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"
				
            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Fuchs_1A4_San_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_San_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_San_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Fuchs_1A4_San_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_San_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_San_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Fuchs_1A4_San_Wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{

						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_San_Wanne_blend_co.paa",
                		"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_San_Anbauteile_blend_co.paa",
						"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

			class AnimationSources: AnimationSources
		    {

				class hide_Jg_Luke_1 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_2 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Jg_Luke_3
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

                class hide_Int_San_Proxy 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
				
				//MG3 Hide
				class Hide_Fake_MG_Fuss
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_MG_Fuss
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class Hide_otocHlaven
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_RecoilHlaven
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_magazine
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_feedtray_cover
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_ammo_belt
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				//Blaulicht Hide
				class Hide_Rundumleuchte_fuss_2
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class San_hide
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};


            };

			class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret 
				{

                    class Turrets: Turrets 
					{

                        class commander_hatch: commander_hatch //Luke_Kommandant [0,0]
                        {

							gunnerAction="Redd_Tank_Fuchs_Passenger_1_San_TurnOut";
							gunnerInAction = "Redd_Tank_Fuchs_Passenger_1_San";
							proxyIndex=8;

						};
						
                        class ffv_hatch_R: ffv_hatch_R //right [0,1]
                        {

							gunnerName="$STR_Sani_Platz";
							isPersonTurret = 0;
							forceHideGunner = 1;
							gunnerInAction = "Redd_Tank_Fuchs_Passenger_1_1_San";
							proxyIndex=9;

						};

                        class ffv_hatch_L: ffv_hatch_L //left [0,2]
                        {

							gunnerName="$STR_Sani_Platz";
							isPersonTurret = 0;
							forceHideGunner = 1;
							gunnerInAction = "Redd_Tank_Fuchs_Passenger_1_2_San";
							proxyIndex=10;

						};

                        class fake_gunner_turret: fake_gunner_turret// [0,3]
                        {};

                    };

                };

				class Fuchs_Bino_Turret_Com: Fuchs_Bino_Turret_Com //[1]
				{
					proxyIndex=8;
					gunnerInAction="rnt_fuchs_san_out_high";
					personTurretAction="rnt_fuchs_san_out_ffv";
				};

            };

			class UserActions: UserActions
		    {

				class MG3_in
				{
					
					displayName = "$STR_MG3_benutzen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0]];[this,[[0,3],true]] remoteExecCall ['lockTurret'];"; 
					
				};

				class MG3_out
				{
					
					displayName = "$STR_MG3_verlassen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,3]];[this,[[0,3],false]] remoteExecCall ['lockTurret'];"; 
					
				};
				
				class Blaulicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationPhase 'blaulicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart',1];this animate ['blaulicht_an',1];this animate ['blaulicht_an_flare',1]";
					
				};
				class Blaulicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationPhase 'blaulicht_an' > 0.5) and (alive this)";
					statement = "this animate ['blaulicht_an',0];this animate ['blaulicht_an_flare',0];this animate ['BeaconsStart',0]";

				};
				
				class Blaulicht_an_2
				{
					
					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'blaulicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart',1];this animate ['blaulicht_an',1];this animate ['blaulicht_an_flare',1]";
					
				};
				class Blaulicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'blaulicht_an' > 0.5) and (alive this)";
					statement = "this animate ['blaulicht_an',0];this animate ['blaulicht_an_flare',0];this animate ['BeaconsStart',0]";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1]";

				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "this animate ['orangelicht_an',0];this animate ['BeaconsStart_2',0]";

				};

				class Orangelicht_an_2
				{
					
					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1]";

				};
				class Orangelicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "this animate ['orangelicht_an',0];this animate ['BeaconsStart_2',0]";

				};

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_aufbauen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animate ['Hide_Rundumleuchte_fuss_2',0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abbauen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animate ['Hide_Rundumleuchte_fuss_2',1];";

				};
				
		    };

		};

		class Redd_Tank_Fuchs_1A4_San_Tropentarn: Redd_Tank_Fuchs_1A4_San_Flecktarn
		{
			
			scopeArsenal = 0;
			forceInGarage = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_San_D.paa";
			displayName="$STR_Fuchs_1A4_San_Tropentarn";

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_San_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_San_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_D_Reifen_blend_co.paa"

            };

		};

        class Redd_Tank_Fuchs_1A4_San_Wintertarn: Redd_Tank_Fuchs_1A4_San_Flecktarn
		{
			
			scopeArsenal = 0;
			forceInGarage = 0;
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_San_Wi.paa";
			displayName="$STR_Fuchs_1A4_San_Wintertarn";

            hiddenSelectionsTextures[] = 
            {

                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_San_Wanne_blend_co.paa",
                "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_San_Anbauteile_blend_co.paa",
				"\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_W_Reifen_blend_co.paa"

            };

			/*
			class EventHandlers: EventHandlers 
            {

               	init = "_this call redd_fnc_Fuchs_W_init";

            };
			*/
		};

		//Fuchs_1_PARA

    };