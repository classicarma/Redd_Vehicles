	
	
	//Basic parameters
	simulation = carx;
	dampersBumpCoef = 0.05;
	terrainCoef = 0;
	accelAidForceYOffset=-1.5;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 300 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 300;
	
	//Differential parameters
	differentialType = "all_limited";
	frontRearSplit = 0.5;
	frontBias = 1.3;
	rearBias = 1.3;
	centreBias = 1.3;

	//Engine parameters
	maxOmega = 314.16;
	minOmega=83.78;
	enginePower = 358;
	peakTorque = 1700;
	idleRPM = 800;
	redRPM = 3000;
	clutchStrength = 111;
	dampingRateFullThrottle = 0.08;
	dampingRateZeroThrottleClutchEngaged = 2;
	dampingRateZeroThrottleClutchDisengaged = 0.35;	
	maxSpeed = 96;
	thrustDelay = 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef=0.495;
	slowSpeedForwardCoef=0.18;

	torqueCurve[] = 
	{

		{"(0/3000)","(0/1700)"},
		{"(428/3000)","(1600/1700)"},
		{"(857/3000)","(1700/1700)"},
		{"(1400/3000)","(1700/1700)"},
		{"(1714/3000)","(1700/1700)"},
		{"(2142/3000)","(1550/1700)"},
		{"(2500/3000)","(1350/1700)"},
		{"(3000/3000)","(0/1700)"}

	};

	//Floating and sinking
	waterPPInVehicle=0;
	maxFordingDepth = 0.5;
	waterResistance = 0;
	canFloat = 1;
	waterLeakiness = 10;
	waterAngularDampingCoef = 10.0;
	waterResistanceCoef = 0.3;
	engineShiftY = 1.5;

	//Anti-roll bars
	antiRollbarForceCoef = 50;
	antiRollbarForceLimit = 50;
	antiRollbarSpeedMin = 0;
	antiRollbarSpeedMax	= 96;
	
	//Gearbox
	class complexGearbox
	{
		
		GearboxRatios[]    = {"R1",-5.89,"N",0,"D1",9,"D2",4.4,"D3",2.6,"D4",2,"D5",1.55,"D6",1.52};
		TransmissionRatios[]={"High",5};
		gearBoxMode="auto";
		moveOffGear=1;
		driveString="D";
		neutralString="N";
		reverseString="R";
		
	};
	
	changeGearType="rpmratio";
	changeGearOmegaRatios[]=
	{

		1.0,0.45,
		0.6,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		1.0,0.6
		
	};
	
	switchTime = 0;
	latency = 1.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	driveOnComponent[] = {};
	wheelCircumference = 3.94;
	numberPhysicalWheels = 6;
	turnCoef = 2.5;

	class Wheels
	{

		class LF
		{
			boneName="wheel_1_1_damper";
			steering=1;
			side="left";
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			width = 0.19;
			mass=80;
			MOI=16;
			latStiffX=25;
			latStiffY=180;
			longitudinalStiffnessPerUnitGravity=10000;
			dampingRate=0.1;
			dampingRateDamaged=1;
			dampingRateDestroyed=1000;
			maxBrakeTorque = 17000*1.5;
			maxHandBrakeTorque = 17000*4;
			suspTravelDirection[]={0,-1,0};
			suspForceAppPointOffset="wheel_1_1_axis";
			tireForceAppPointOffset="wheel_1_1_axis";
			maxCompression=0.2;
			mMaxDroop=0.2;
			sprungMass=2833;
			springStrength=119694;
			springDamperRate=23939;
			damping=75;
			frictionVsSlipGraph[]={{0,1},{0.5,1},{1,1}};

		};
		class LR: LF
		{
			boneName="wheel_1_2_damper";
			steering=1;
			center="wheel_1_2_axis";
			boundary="wheel_1_2_bound";
			suspForceAppPointOffset="wheel_1_2_axis";
			tireForceAppPointOffset="wheel_1_2_axis";
		};
		class RF: LF
		{
			boneName="wheel_2_1_damper";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			suspForceAppPointOffset="wheel_2_1_axis";
			tireForceAppPointOffset="wheel_2_1_axis";
			steering=1;
			side="right";
		};
		class RR: RF
		{
			boneName="wheel_2_2_damper";
			steering=1;
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			suspForceAppPointOffset="wheel_2_2_axis";
			tireForceAppPointOffset="wheel_2_2_axis";
		};
		class RRR: RF
		{
			boneName="wheel_2_3_damper";
			steering=0;
			center="wheel_2_3_axis";
			boundary="wheel_2_3_bound";
			suspForceAppPointOffset="wheel_2_3_axis";
			tireForceAppPointOffset="wheel_2_3_axis";
		};
		class LRR: LF
		{
			boneName="wheel_1_3_damper";
			steering=0;
			center="wheel_1_3_axis";
			boundary="wheel_1_3_bound";
			suspForceAppPointOffset="wheel_1_3_axis";
			tireForceAppPointOffset="wheel_1_3_axis";
		};

	};