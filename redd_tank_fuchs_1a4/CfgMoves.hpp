

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Fuchs_Driver = "Redd_Tank_Fuchs_Driver";
            KIA_Redd_Tank_Fuchs_Driver = "KIA_Redd_Tank_Fuchs_Driver";

            Redd_Tank_Fuchs_Co_Driver_TurnedIn = "Redd_Tank_Fuchs_Co_Driver_TurnedIn";
            Redd_Tank_Fuchs_Co_Driver_TurnedOut = "Redd_Tank_Fuchs_Co_Driver_TurnedOut";
            Redd_Tank_Fuchs_Co_Driver_TurnedOut_2 = "Redd_Tank_Fuchs_Co_Driver_TurnedOut_2";
            KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn = "KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn";
            KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut = "KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut";
            KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2 = "KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2";
           
            Redd_Tank_Fuchs_Passenger_1 = "Redd_Tank_Fuchs_Passenger_1";
            KIA_Redd_Tank_Fuchs_Passenger_1 = "KIA_Redd_Tank_Fuchs_Passenger_1";

            rnt_fuchs_com_out_bino = "rnt_fuchs_com_out_bino";
            rnt_fuchs_com_out_high = "rnt_fuchs_com_out_high";
            KIA_rnt_fuchs_com_out_high = "KIA_rnt_fuchs_com_out_high";

            Redd_Tank_Fuchs_Passenger_1_1 = "Redd_Tank_Fuchs_Passenger_1_1";
            KIA_Redd_Tank_Fuchs_Passenger_1_1 = "KIA_Redd_Tank_Fuchs_Passenger_1_1";

            Redd_Tank_Fuchs_Passenger_1_2 = "Redd_Tank_Fuchs_Passenger_1_2";
            KIA_Redd_Tank_Fuchs_Passenger_1_2 = "KIA_Redd_Tank_Fuchs_Passenger_1_2";
            
            Redd_Tank_Fuchs_Passenger_2 = "Redd_Tank_Fuchs_Passenger_2";
            Redd_Tank_Fuchs_Passenger_3 = "Redd_Tank_Fuchs_Passenger_3";
            Redd_Tank_Fuchs_Passenger_4 = "Redd_Tank_Fuchs_Passenger_4";
            KIA_Redd_Tank_Fuchs_Passenger_2 = "KIA_Redd_Tank_Fuchs_Passenger_2";
            KIA_Redd_Tank_Fuchs_Passenger_3 = "KIA_Redd_Tank_Fuchs_Passenger_3";
            KIA_Redd_Tank_Fuchs_Passenger_4 = "KIA_Redd_Tank_Fuchs_Passenger_4";

            Redd_Tank_Fuchs_Milan = "Redd_Tank_Fuchs_Milan";
            KIA_Redd_Tank_Fuchs_Milan = "KIA_Redd_Tank_Fuchs_Milan";

            Redd_Tank_Fuchs_Milan_2 = "Redd_Tank_Fuchs_Milan_2";
            KIA_Redd_Tank_Fuchs_Milan_2 = "KIA_Redd_Tank_Fuchs_Milan_2";

            Redd_Tank_Fuchs_Pi = "Redd_Tank_Fuchs_Pi";
            Redd_Tank_Fuchs_Pi_In = "Redd_Tank_Fuchs_Pi_In";
            rnt_fuchs_com_pi_high = "rnt_fuchs_com_pi_high";
            rnt_fuchs_com_pi_bino = "rnt_fuchs_com_pi_bino";
            KIA_Redd_Tank_Fuchs_Pi = "KIA_Redd_Tank_Fuchs_Pi";
            KIA_Redd_Tank_Fuchs_Pi_2 = "KIA_Redd_Tank_Fuchs_Pi_2";

            Redd_Tank_Fuchs_Passenger_1_San = "Redd_Tank_Fuchs_Passenger_1_San";
            Redd_Tank_Fuchs_Passenger_1_San_TurnOut = "Redd_Tank_Fuchs_Passenger_1_San_TurnOut";
            KIA_Redd_Tank_Fuchs_Passenger_1_San = "KIA_Redd_Tank_Fuchs_Passenger_1_San";
            KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut = "KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut";

            rnt_fuchs_com_san_high = "rnt_fuchs_com_san_high";
            rnt_fuchs_com_san_bino = "rnt_fuchs_com_san_bino";
            KIA_rnt_fuchs_san_out = "KIA_rnt_fuchs_san_out";

            Redd_Tank_Fuchs_Passenger_1_1_San = "Redd_Tank_Fuchs_Passenger_1_1_San";
            Redd_Tank_Fuchs_Passenger_1_2_San = "Redd_Tank_Fuchs_Passenger_1_2_San";
            Redd_Tank_Fuchs_Passenger_2_San = "Redd_Tank_Fuchs_Passenger_2_San";
            Redd_Tank_Fuchs_Passenger_3_San = "Redd_Tank_Fuchs_Passenger_3_San";
            KIA_Redd_Tank_Fuchs_Passenger_1_1_San = "KIA_Redd_Tank_Fuchs_Passenger_1_1_San";
            KIA_Redd_Tank_Fuchs_Passenger_1_2_San = "KIA_Redd_Tank_Fuchs_Passenger_1_2_San";
            KIA_Redd_Tank_Fuchs_Passenger_2_San = "KIA_Redd_Tank_Fuchs_Passenger_2_San";
            KIA_Redd_Tank_Fuchs_Passenger_3_San = "KIA_Redd_Tank_Fuchs_Passenger_3_San";

            //ffv poses
            rnt_fuchs_com_out_ffv = "rnt_fuchs_Unarmed_Idle_com";
            rnt_fuchs_pi_out_ffv = "rnt_fuchs_Unarmed_Idle_pi";
            rnt_fuchs_san_out_ffv = "rnt_fuchs_Unarmed_Idle_san";

        };

        class Actions
        {
            class FFV_BaseActions;

            //com_out_ffv
            class rnt_fuchs_Action_Actions_com : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_fuchs_Unarmed_Idle_com";
                stopRelaxed = "rnt_fuchs_Unarmed_Idle_com";
                default = "rnt_fuchs_Unarmed_Idle_com";
                Stand = "rnt_fuchs_Unarmed_Idle_com";
                HandGunOn = "rnt_fuchs_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_fuchs_Unarmed_Idle_com";
                Binoculars = "rnt_fuchs_Binoc_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
                civil = "rnt_fuchs_Unarmed_Idle_com";
            };
            class rnt_fuchs_Action_Actions_pi : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_fuchs_Unarmed_Idle_pi";
                stopRelaxed = "rnt_fuchs_Unarmed_Idle_pi";
                default = "rnt_fuchs_Unarmed_Idle_pi";
                Stand = "rnt_fuchs_Unarmed_Idle_pi";
                HandGunOn = "rnt_fuchs_Unarmed_Idle_pi";
                PrimaryWeapon = "rnt_fuchs_Unarmed_Idle_pi";
                Binoculars = "rnt_fuchs_Binoc_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
                civil = "rnt_fuchs_Unarmed_Idle_pi";
            };
            class rnt_fuchs_Action_Actions_san : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_fuchs_Unarmed_Idle_san";
                stopRelaxed = "rnt_fuchs_Unarmed_Idle_san";
                default = "rnt_fuchs_Unarmed_Idle_san";
                Stand = "rnt_fuchs_Unarmed_Idle_san";
                HandGunOn = "rnt_fuchs_Unarmed_Idle_san";
                PrimaryWeapon = "rnt_fuchs_Unarmed_Idle_san";
                Binoculars = "rnt_fuchs_Binoc_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
                civil = "rnt_fuchs_Unarmed_Idle_san";
            };
            class rnt_fuchs_IdleUnarmed_Actions_com : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_fuchs_Unarmed_Idle_com";
                stopRelaxed = "rnt_fuchs_Unarmed_Idle_com";
                default = "rnt_fuchs_Unarmed_Idle_com";
                Stand = "rnt_fuchs_Unarmed_Idle_com";
                HandGunOn = "rnt_fuchs_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_fuchs_Unarmed_Idle_com";
                Binoculars = "rnt_fuchs_Unarmed_Binoc_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
                civil = "rnt_fuchs_Unarmed_Idle_com";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_fuchs_IdleUnarmed_Actions_pi : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_fuchs_Unarmed_Idle_pi";
                stopRelaxed = "rnt_fuchs_Unarmed_Idle_pi";
                default = "rnt_fuchs_Unarmed_Idle_pi";
                Stand = "rnt_fuchs_Unarmed_Idle_pi";
                HandGunOn = "rnt_fuchs_Unarmed_Idle_pi";
                PrimaryWeapon = "rnt_fuchs_Unarmed_Idle_pi";
                Binoculars = "rnt_fuchs_Unarmed_Binoc_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
                civil = "rnt_fuchs_Unarmed_Idle_pi";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_fuchs_IdleUnarmed_Actions_san : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_fuchs_Unarmed_Idle_san";
                stopRelaxed = "rnt_fuchs_Unarmed_Idle_san";
                default = "rnt_fuchs_Unarmed_Idle_san";
                Stand = "rnt_fuchs_Unarmed_Idle_san";
                HandGunOn = "rnt_fuchs_Unarmed_Idle_san";
                PrimaryWeapon = "rnt_fuchs_Unarmed_Idle_san";
                Binoculars = "rnt_fuchs_Unarmed_Binoc_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
                civil = "rnt_fuchs_Unarmed_Idle_san";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_fuchs_Dead_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_fuchs_Die_com";
                default = "rnt_fuchs_Die_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
            };
            class rnt_fuchs_Dead_Actions_pi : FFV_BaseActions 
            {
                stop = "rnt_fuchs_Die_pi";
                default = "rnt_fuchs_Die_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
            };
            class rnt_fuchs_Dead_Actions_san : FFV_BaseActions 
            {
                stop = "rnt_fuchs_Die_san";
                default = "rnt_fuchs_Die_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
            };
            class rnt_fuchs_DeadPistol_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_fuchs_Die_com";
                default = "rnt_fuchs_Die_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
            };
            class rnt_fuchs_DeadPistol_Actions_pi : FFV_BaseActions 
            {
                stop = "rnt_fuchs_Die_pi";
                default = "rnt_fuchs_Die_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
            };
            class rnt_fuchs_DeadPistol_Actions_san : FFV_BaseActions 
            {
                stop = "rnt_fuchs_Die_san";
                default = "rnt_fuchs_Die_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
            };
            class rnt_fuchs_Pistol_Actions_com : rnt_fuchs_Action_Actions_com 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_fuchs_Pistol_com";
                stopRelaxed = "rnt_fuchs_Pistol_com";
                default = "rnt_fuchs_Pistol_com";
                Binoculars = "rnt_fuchs_Pistol_Binoc_com";
                Stand = "rnt_fuchs_Pistol_Idle_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_fuchs_Pistol_Actions_pi : rnt_fuchs_Action_Actions_pi 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_fuchs_Pistol_pi";
                stopRelaxed = "rnt_fuchs_Pistol_pi";
                default = "rnt_fuchs_Pistol_pi";
                Binoculars = "rnt_fuchs_Pistol_Binoc_pi";
                Stand = "rnt_fuchs_Pistol_Idle_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_fuchs_Pistol_Actions_san : rnt_fuchs_Action_Actions_san
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_fuchs_Pistol_san";
                stopRelaxed = "rnt_fuchs_Pistol_san";
                default = "rnt_fuchs_Pistol_san";
                Binoculars = "rnt_fuchs_Pistol_Binoc_san";
                Stand = "rnt_fuchs_Pistol_Idle_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_fuchs_Binoc_Actions_com : rnt_fuchs_Action_Actions_com 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_fuchs_Pistol_Binoc_com";
                stopRelaxed = "rnt_fuchs_Pistol_Binoc_com";
                default = "rnt_fuchs_Binoc_com";
            };
            class rnt_fuchs_Binoc_Actions_pi : rnt_fuchs_Action_Actions_pi 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_fuchs_Pistol_Binoc_pi";
                stopRelaxed = "rnt_fuchs_Pistol_Binoc_pi";
                default = "rnt_fuchs_Binoc_pi";
            };
            class rnt_fuchs_Binoc_Actions_san : rnt_fuchs_Action_Actions_san 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_fuchs_Pistol_Binoc_san";
                stopRelaxed = "rnt_fuchs_Pistol_Binoc_san";
                default = "rnt_fuchs_Binoc_pi";
            };
            class rnt_fuchs_BinocPistol_Actions_com : rnt_fuchs_Binoc_Actions_com 
            {
                stop = "rnt_fuchs_Pistol_Binoc_com";
                stopRelaxed = "rnt_fuchs_Pistol_Binoc_com";
                default = "rnt_fuchs_Pistol_Binoc_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
            };
            class rnt_fuchs_BinocPistol_Actions_pi : rnt_fuchs_Binoc_Actions_pi 
            {
                stop = "rnt_fuchs_Pistol_Binoc_pi";
                stopRelaxed = "rnt_fuchs_Pistol_Binoc_pi";
                default = "rnt_fuchs_Pistol_Binoc_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
            };
            class rnt_fuchs_BinocPistol_Actions_san : rnt_fuchs_Binoc_Actions_san 
            {
                stop = "rnt_fuchs_Pistol_Binoc_san";
                stopRelaxed = "rnt_fuchs_Pistol_Binoc_san";
                default = "rnt_fuchs_Pistol_Binoc_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
            };
            class rnt_fuchs_BinocUnarmed_Actions_com : rnt_fuchs_Binoc_Actions_com 
            {
                stop = "rnt_fuchs_Unarmed_Binoc_com";
                stopRelaxed = "rnt_fuchs_Unarmed_Binoc_com";
                default = "rnt_fuchs_Unarmed_Binoc_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
            };
            class rnt_fuchs_BinocUnarmed_Actions_pi : rnt_fuchs_Binoc_Actions_pi 
            {
                stop = "rnt_fuchs_Unarmed_Binoc_pi";
                stopRelaxed = "rnt_fuchs_Unarmed_Binoc_pi";
                default = "rnt_fuchs_Unarmed_Binoc_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
            };
            class rnt_fuchs_BinocUnarmed_Actions_san : rnt_fuchs_Binoc_Actions_san
            {
                stop = "rnt_fuchs_Unarmed_Binoc_san";
                stopRelaxed = "rnt_fuchs_Unarmed_Binoc_san";
                default = "rnt_fuchs_Unarmed_Binoc_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
            };
            class rnt_fuchs_Idle_Actions_com : rnt_fuchs_Action_Actions_com 
            {
                upDegree = "ManPosStand";
                stop = "rnt_fuchs_Idle_com";
                stopRelaxed = "rnt_fuchs_Idle_com";
                default = "rnt_fuchs_Idle_com";
                Combat = "rnt_fuchs_Unarmed_Idle_com";
                fireNotPossible = "rnt_fuchs_Unarmed_Idle_com";
                PlayerStand = "rnt_fuchs_Unarmed_Idle_com";
            };
            class rnt_fuchs_Idle_Actions_pi : rnt_fuchs_Action_Actions_pi
            {
                upDegree = "ManPosStand";
                stop = "rnt_fuchs_Idle_pi";
                stopRelaxed = "rnt_fuchs_Idle_pi";
                default = "rnt_fuchs_Idle_pi";
                Combat = "rnt_fuchs_Unarmed_Idle_pi";
                fireNotPossible = "rnt_fuchs_Unarmed_Idle_pi";
                PlayerStand = "rnt_fuchs_Unarmed_Idle_pi";
            };
            class rnt_fuchs_Idle_Actions_san : rnt_fuchs_Action_Actions_san
            {
                upDegree = "ManPosStand";
                stop = "rnt_fuchs_Idle_san";
                stopRelaxed = "rnt_fuchs_Idle_san";
                default = "rnt_fuchs_Idle_san";
                Combat = "rnt_fuchs_Unarmed_Idle_san";
                fireNotPossible = "rnt_fuchs_Unarmed_Idle_san";
                PlayerStand = "rnt_fuchs_Unarmed_Idle_san";
            };
            class rnt_fuchs_IdlePistol_Actions_com : rnt_fuchs_Action_Actions_com 
            {
                Stand = "rnt_fuchs_Pistol_Idle_com";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_fuchs_Pistol_Idle_com";
                stopRelaxed = "rnt_fuchs_Pistol_Idle_com";
                default = "rnt_fuchs_Pistol_Idle_com";
                Combat = "rnt_fuchs_Pistol_com";
                fireNotPossible = "rnt_fuchs_Pistol_com";
                PlayerStand = "rnt_fuchs_Pistol_com";
                die = "rnt_fuchs_Die_com";
                Unconscious = "rnt_fuchs_Die_com";
            };
            class rnt_fuchs_IdlePistol_Actions_pi : rnt_fuchs_Action_Actions_pi 
            {
                Stand = "rnt_fuchs_Pistol_Idle_pi";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_fuchs_Pistol_Idle_pi";
                stopRelaxed = "rnt_fuchs_Pistol_Idle_pi";
                default = "rnt_fuchs_Pistol_Idle_pi";
                Combat = "rnt_fuchs_Pistol_pi";
                fireNotPossible = "rnt_fuchs_Pistol_pi";
                PlayerStand = "rnt_fuchs_Pistol_pi";
                die = "rnt_fuchs_Die_pi";
                Unconscious = "rnt_fuchs_Die_pi";
            };
            class rnt_fuchs_IdlePistol_Actions_san : rnt_fuchs_Action_Actions_san 
            {
                Stand = "rnt_fuchs_Pistol_Idle_san";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_fuchs_Pistol_Idle_san";
                stopRelaxed = "rnt_fuchs_Pistol_Idle_san";
                default = "rnt_fuchs_Pistol_Idle_san";
                Combat = "rnt_fuchs_Pistol_san";
                fireNotPossible = "rnt_fuchs_Pistol_san";
                PlayerStand = "rnt_fuchs_Pistol_san";
                die = "rnt_fuchs_Die_san";
                Unconscious = "rnt_fuchs_Die_san";
            };
        };
    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Tank_Fuchs_Driver: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            
            class Redd_Tank_Fuchs_Co_Driver_TurnedIn: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Co_Driver_TurnedIn.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Co_Driver_TurnedOut: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Co_Driver_TurnedOut.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Co_Driver_TurnedOut_2: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Co_Driver_TurnedOut_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_1_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_1.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_1",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_1", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_1.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_1_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_2", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_2", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_3: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_3.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_3",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_3", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_3: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_3.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_4: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_4.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_4",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_4", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_4: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_4.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Milan: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Milan.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Milan",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Milan", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Milan: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Milan.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Milan_2: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Milan_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Milan_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Milan_2", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Milan_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Milan_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class rnt_fuchs_com_out_high: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\rnt_fuchs_com_out_high.rtm";
                interpolateTo[] = {"KIA_rnt_fuchs_com_out_high",1};
                ConnectTo[]={"KIA_rnt_fuchs_com_out_high", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_rnt_fuchs_com_out_high: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_rnt_fuchs_com_out_high.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Pi: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Pi.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Pi",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Pi", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Pi: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Pi.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class rnt_fuchs_com_pi_high: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\rnt_fuchs_com_pi_high.rtm";
                interpolateTo[] = {"KIA_rnt_fuchs_pi_out",1};
                ConnectTo[]={"KIA_rnt_fuchs_pi_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_rnt_fuchs_pi_out: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_rnt_fuchs_pi_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Pi_In: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Pi_In.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Pi_In",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Pi_In", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Pi_In: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Pi_In.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class rnt_fuchs_com_san_high: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\rnt_fuchs_com_san_high.rtm";
                interpolateTo[] = {"KIA_rnt_fuchs_san_out",1};
                ConnectTo[]={"KIA_rnt_fuchs_san_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_rnt_fuchs_san_out: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_rnt_fuchs_san_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_San_TurnOut: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_San_TurnOut.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_1_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_1_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_1_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_1_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_2_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_2_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_2_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_2_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_2_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_2_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_2_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_2_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_3_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_3_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_3_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_3_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_3_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_3_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            //ffv part
            class vehicle_turnout_1_Aim_Pistol;
            class vehicle_turnout_1_Aim_Pistol_Idling;
            class vehicle_turnout_1_Idle_Pistol;
            class vehicle_turnout_1_Idle_Pistol_Idling;
            class vehicle_turnout_1_Aim_ToPistol;
            class vehicle_turnout_1_Aim_ToPistol_End ;
            class vehicle_turnout_1_Aim_FromPistol;
            class vehicle_turnout_1_Aim_FromPistol_End;
            class vehicle_turnout_1_Aim_Binoc;
            class vehicle_turnout_1_Aim_Pistol_Binoc;
            class vehicle_turnout_1_Aim_ToBinoc;
            class vehicle_turnout_1_Aim_ToBinoc_End;
            class vehicle_turnout_1_Aim_FromBinoc;
            class vehicle_turnout_1_Aim_FromBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc_End;
            class vehicle_turnout_1_Idle_Unarmed;
            class vehicle_turnout_1_Idle_Unarmed_Idling;
            class vehicle_turnout_1_Aim_Unarmed_Binoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc_End;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc_End;
            class vehicle_turnout_1_Die;
            class vehicle_turnout_1_Die_Pistol;

            //com_out_ffv
            class rnt_fuchs_Pistol_com : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_fuchs_Pistol_Actions_com";
                variantsAI[] = {"rnt_fuchs_Pistol_Idling_com", 1};
                variantsPlayer[] = {"rnt_fuchs_Pistol_Idling_com", 1};
                ConnectTo[] = {"rnt_fuchs_Pistol_ToBinoc_com", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_com", 0.1, "rnt_fuchs_Pistol_Idle_com", 0.2, "rnt_fuchs_Unarmed_Idle_com", 0.2, "rnt_fuchs_Die_com", 0.5};
             };
            class rnt_fuchs_Pistol_Idling_com : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_fuchs_Pistol_com", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_com", 0.1, "rnt_fuchs_Pistol_Idle_com", 0.2, "rnt_fuchs_Unarmed_Idle_com", 0.2, "rnt_fuchs_Die_com", 0.5};
            };
            class rnt_fuchs_Pistol_Idle_com : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_fuchs_IdlePistol_Actions_com";
                InterpolateTo[] = {"rnt_fuchs_Pistol_com", 0.1, "rnt_fuchs_FromPistol_com", 0.1, "rnt_fuchs_Unarmed_Idle_com", 0.1, "rnt_fuchs_Die_com", 0.1};
                variantsAI[] = {"rnt_fuchs_Pistol_Idle_Idling_com", 1};
                variantsPlayer[] = {"rnt_fuchs_Pistol_Idle_Idling_com", 1};
            };
            class rnt_fuchs_Pistol_Idle_Idling_com : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_fuchs_Pistol_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_fuchs_Pistol_com", 0.1, "rnt_fuchs_FromPistol_com", 0.1, "rnt_fuchs_Unarmed_Idle_com", 0.1, "rnt_fuchs_Die_com", 0.1};
            };
            class rnt_fuchs_ToPistol_com : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_fuchs_Pistol_Actions_com";
                ConnectTo[] = {"rnt_fuchs_ToPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_ToPistol_End_com : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_fuchs_Pistol_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromPistol_com : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_high.rtm";
                actions = "rnt_fuchs_Pistol_Actions_com";
                ConnectTo[] = {"rnt_fuchs_FromPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromPistol_End_com : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_fuchs_Action_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Binoc_com : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_Binoc_Actions_com";
                InterpolateTo[] = {"rnt_fuchs_FromBinoc_com", 0.1, "rnt_fuchs_Die_com", 0.1};
            };
            class rnt_fuchs_Pistol_Binoc_com : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_fuchs_BinocPistol_Actions_com";
                InterpolateTo[] = {"rnt_fuchs_Pistol_FromBinoc_com", 0.1, "rnt_fuchs_Die_com", 0.1};
            };
            class rnt_fuchs_ToBinoc_com : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_fuchs_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_ToBinoc_End_com : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_fuchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromBinoc_com : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_fuchs_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromBinoc_End_com : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_fuchs_Action_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_ToBinoc_com : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Pistol_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_ToBinoc_End_com : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_fuchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Pistol_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_FromBinoc_com : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Pistol_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_FromBinoc_End_com : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_fuchs_Action_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_Idle_com : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_fuchs_IdleUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_fuchs_FromPistol_End_com", 0.1, "rnt_fuchs_ToPistol_End_com", 0.1, "rnt_fuchs_Unarmed_ToBinoc_com", 0.1, "rnt_fuchs_Die_com", 0.1};
                variantsAI[] = {"rnt_fuchs_Idle_Unarmed_Idling_com", 1};
                variantsPlayer[] = {"rnt_fuchs_Idle_Unarmed_Idling_com", 1};
            };
            class rnt_fuchs_Idle_Unarmed_Idling_com : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_End_com", 0.1, "rnt_fuchs_ToPistol_End_com", 0.1, "rnt_fuchs_Unarmed_ToBinoc_com", 0.1, "rnt_fuchs_Die_com", 0.1};
            };
            class rnt_fuchs_Unarmed_Binoc_com : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_fuchs_Unarmed_FromBinoc_com", 0.1, "rnt_fuchs_Die_com", 0.1};
            };
            class rnt_fuchs_Unarmed_ToBinoc_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Unarmed_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_ToBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_FromBinoc_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Unarmed_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_FromBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_Action_Actions_com";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Die_com : vehicle_turnout_1_Die 
            {
                file = "\redd_tank_fuchs_1a4\anims\KIA_rnt_fuchs_com_out_high.rtm";
                actions = "rnt_fuchs_Dead_Actions_com";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Die_Pistol_com : vehicle_turnout_1_Die_Pistol 
            {
                file = "\redd_tank_fuchs_1a4\anims\KIA_rnt_fuchs_com_out_high.rtm";
                actions = "rnt_fuchs_DeadPistol_Actions_com";
                showHandGun = 1;
            };

            //Pi com ffv
            class rnt_fuchs_Pistol_pi : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_fuchs_Pistol_Actions_pi";
                variantsAI[] = {"rnt_fuchs_Pistol_Idling_pi", 1};
                variantsPlayer[] = {"rnt_fuchs_Pistol_Idling_pi", 1};
                ConnectTo[] = {"rnt_fuchs_Pistol_ToBinoc_pi", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_pi", 0.1, "rnt_fuchs_Pistol_Idle_pi", 0.2, "rnt_fuchs_Unarmed_Idle_pi", 0.2, "rnt_fuchs_Die_pi", 0.5};
             };
            class rnt_fuchs_Pistol_Idling_pi : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_fuchs_Pistol_pi", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_pi", 0.1, "rnt_fuchs_Pistol_Idle_pi", 0.2, "rnt_fuchs_Unarmed_Idle_pi", 0.2, "rnt_fuchs_Die_pi", 0.5};
            };
            class rnt_fuchs_Pistol_Idle_pi : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_fuchs_IdlePistol_Actions_pi";
                InterpolateTo[] = {"rnt_fuchs_Pistol_pi", 0.1, "rnt_fuchs_FromPistol_pi", 0.1, "rnt_fuchs_Unarmed_Idle_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
                variantsAI[] = {"rnt_fuchs_Pistol_Idle_Idling_pi", 1};
                variantsPlayer[] = {"rnt_fuchs_Pistol_Idle_Idling_pi", 1};
            };
            class rnt_fuchs_Pistol_Idle_Idling_pi : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_fuchs_Pistol_Idle_pi", 0.1};
                InterpolateTo[] = {"rnt_fuchs_Pistol_pi", 0.1, "rnt_fuchs_FromPistol_pi", 0.1, "rnt_fuchs_Unarmed_Idle_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
            };
            class rnt_fuchs_ToPistol_pi : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_fuchs_Pistol_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_ToPistol_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_ToPistol_End_pi : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_fuchs_Pistol_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Pistol_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromPistol_pi : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_pi_high.rtm";
                actions = "rnt_fuchs_Pistol_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_FromPistol_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromPistol_End_pi : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_fuchs_Action_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Binoc_pi : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_Binoc_Actions_pi";
                InterpolateTo[] = {"rnt_fuchs_FromBinoc_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
            };
            class rnt_fuchs_Pistol_Binoc_pi : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_fuchs_BinocPistol_Actions_pi";
                InterpolateTo[] = {"rnt_fuchs_Pistol_FromBinoc_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
            };
            class rnt_fuchs_ToBinoc_pi : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_ToBinoc_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_ToBinoc_End_pi : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_fuchs_Binoc_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Binoc_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromBinoc_pi : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_FromBinoc_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromBinoc_End_pi : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_fuchs_Action_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_ToBinoc_pi : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Pistol_ToBinoc_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_ToBinoc_End_pi : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_fuchs_Binoc_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Pistol_Binoc_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_FromBinoc_pi : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Pistol_FromBinoc_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_FromBinoc_End_pi : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_fuchs_Action_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Pistol_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_Idle_pi : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_pi_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_fuchs_IdleUnarmed_Actions_pi";
                InterpolateTo[] = {"rnt_fuchs_FromPistol_End_pi", 0.1, "rnt_fuchs_ToPistol_End_pi", 0.1, "rnt_fuchs_Unarmed_ToBinoc_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
                variantsAI[] = {"rnt_fuchs_Idle_Unarmed_Idling_pi", 1};
                variantsPlayer[] = {"rnt_fuchs_Idle_Unarmed_Idling_pi", 1};
            };
            class rnt_fuchs_Idle_Unarmed_Idling_pi : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_pi_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_pi", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_End_pi", 0.1, "rnt_fuchs_ToPistol_End_pi", 0.1, "rnt_fuchs_Unarmed_ToBinoc_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
            };
            class rnt_fuchs_Unarmed_Binoc_pi : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_pi_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_pi";
                InterpolateTo[] = {"rnt_fuchs_Unarmed_FromBinoc_pi", 0.1, "rnt_fuchs_Die_pi", 0.1};
            };
            class rnt_fuchs_Unarmed_ToBinoc_pi : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_pi_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Unarmed_ToBinoc_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_ToBinoc_End_pi : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Binoc_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_FromBinoc_pi : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Unarmed_FromBinoc_End_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_FromBinoc_End_pi : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_out_bino.rtm";
                actions = "rnt_fuchs_Action_Actions_pi";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_pi", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Die_pi : vehicle_turnout_1_Die 
            {
                file = "\redd_tank_fuchs_1a4\anims\KIA_rnt_fuchs_pi_out.rtm";
                actions = "rnt_fuchs_Dead_Actions_pi";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Die_Pistol_pi : vehicle_turnout_1_Die_Pistol 
            {
                file = "\redd_tank_fuchs_1a4\anims\KIA_rnt_fuchs_pi_out.rtm";
                actions = "rnt_fuchs_DeadPistol_Actions_pi";
                showHandGun = 1;
            };

            //san_out_ffv
            class rnt_fuchs_Pistol_san : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_fuchs_Pistol_Actions_san";
                variantsAI[] = {"rnt_fuchs_Pistol_Idling_san", 1};
                variantsPlayer[] = {"rnt_fuchs_Pistol_Idling_san", 1};
                ConnectTo[] = {"rnt_fuchs_Pistol_ToBinoc_san", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_san", 0.1, "rnt_fuchs_Pistol_Idle_san", 0.2, "rnt_fuchs_Unarmed_Idle_san", 0.2, "rnt_fuchs_Die_san", 0.5};
             };
            class rnt_fuchs_Pistol_Idling_san : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_fuchs_Pistol_san", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_san", 0.1, "rnt_fuchs_Pistol_Idle_san", 0.2, "rnt_fuchs_Unarmed_Idle_san", 0.2, "rnt_fuchs_Die_san", 0.5};
            };
            class rnt_fuchs_Pistol_Idle_san : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_fuchs_IdlePistol_Actions_san";
                InterpolateTo[] = {"rnt_fuchs_Pistol_san", 0.1, "rnt_fuchs_FromPistol_san", 0.1, "rnt_fuchs_Unarmed_Idle_san", 0.1, "rnt_fuchs_Die_san", 0.1};
                variantsAI[] = {"rnt_fuchs_Pistol_Idle_Idling_san", 1};
                variantsPlayer[] = {"rnt_fuchs_Pistol_Idle_Idling_san", 1};
            };
            class rnt_fuchs_Pistol_Idle_Idling_san : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_fuchs_Pistol_Idle_san", 0.1};
                InterpolateTo[] = {"rnt_fuchs_Pistol_san", 0.1, "rnt_fuchs_FromPistol_san", 0.1, "rnt_fuchs_Unarmed_Idle_san", 0.1, "rnt_fuchs_Die_san", 0.1};
            };
            class rnt_fuchs_ToPistol_san : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_fuchs_Pistol_Actions_san";
                ConnectTo[] = {"rnt_fuchs_ToPistol_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_ToPistol_End_san : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_fuchs_Pistol_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Pistol_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromPistol_san : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_high.rtm";
                actions = "rnt_fuchs_Pistol_Actions_san";
                ConnectTo[] = {"rnt_fuchs_FromPistol_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromPistol_End_san : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_fuchs_Action_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Binoc_san : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_bino.rtm";
                actions = "rnt_fuchs_Binoc_Actions_san";
                InterpolateTo[] = {"rnt_fuchs_FromBinoc_san", 0.1, "rnt_fuchs_Die_san", 0.1};
            };
            class rnt_fuchs_Pistol_Binoc_san : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_fuchs_BinocPistol_Actions_san";
                InterpolateTo[] = {"rnt_fuchs_Pistol_FromBinoc_san", 0.1, "rnt_fuchs_Die_san", 0.1};
            };
            class rnt_fuchs_ToBinoc_san : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_san";
                ConnectTo[] = {"rnt_fuchs_ToBinoc_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_ToBinoc_End_san : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_fuchs_Binoc_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Binoc_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromBinoc_san : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_san";
                ConnectTo[] = {"rnt_fuchs_FromBinoc_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_FromBinoc_End_san : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_fuchs_Action_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_ToBinoc_san : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Pistol_ToBinoc_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_ToBinoc_End_san : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_fuchs_Binoc_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Pistol_Binoc_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_FromBinoc_san : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_fuchs_Binoc_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Pistol_FromBinoc_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Pistol_FromBinoc_End_san : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_fuchs_Action_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Pistol_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_Idle_san : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_fuchs_IdleUnarmed_Actions_san";
                InterpolateTo[] = {"rnt_fuchs_FromPistol_End_san", 0.1, "rnt_fuchs_ToPistol_End_san", 0.1, "rnt_fuchs_Unarmed_ToBinoc_san", 0.1, "rnt_fuchs_Die_san", 0.1};
                variantsAI[] = {"rnt_fuchs_Idle_Unarmed_Idling_san", 1};
                variantsPlayer[] = {"rnt_fuchs_Idle_Unarmed_Idling_san", 1};
            };
            class rnt_fuchs_Idle_Unarmed_Idling_san : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_san", 0.1};
                InterpolateTo[] = {"rnt_fuchs_FromPistol_End_san", 0.1, "rnt_fuchs_ToPistol_End_san", 0.1, "rnt_fuchs_Unarmed_ToBinoc_san", 0.1, "rnt_fuchs_Die_san", 0.1};
            };
            class rnt_fuchs_Unarmed_Binoc_san : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_san";
                InterpolateTo[] = {"rnt_fuchs_Unarmed_FromBinoc_san", 0.1, "rnt_fuchs_Die_san", 0.1};
            };
            class rnt_fuchs_Unarmed_ToBinoc_san : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Unarmed_ToBinoc_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_ToBinoc_End_san : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Binoc_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_FromBinoc_san : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_bino.rtm";
                actions = "rnt_fuchs_BinocUnarmed_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Unarmed_FromBinoc_End_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Unarmed_FromBinoc_End_san : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\redd_tank_fuchs_1a4\anims\rnt_fuchs_com_san_bino.rtm";
                actions = "rnt_fuchs_Action_Actions_san";
                ConnectTo[] = {"rnt_fuchs_Unarmed_Idle_san", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Die_san : vehicle_turnout_1_Die 
            {
                file = "\redd_tank_fuchs_1a4\anims\KIA_rnt_fuchs_san_out.rtm";
                actions = "rnt_fuchs_Dead_Actions_san";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_fuchs_Die_Pistol_san : vehicle_turnout_1_Die_Pistol 
            {
                file = "\redd_tank_fuchs_1a4\anims\KIA_rnt_fuchs_san_out.rtm";
                actions = "rnt_fuchs_DeadPistol_Actions_san";
                showHandGun = 1;
            };
            
        };

    };