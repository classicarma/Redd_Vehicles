
	
	class SensorTemplateActiveRadar;
	class SensorTemplateDataLink;
	class DefaultVehicleSystemsDisplayManagerLeft
	{
		class components;
	};
	class DefaultVehicleSystemsDisplayManagerRight
	{
		class components;
	};
	class VehicleSystemsTemplateLeftCommander: DefaultVehicleSystemsDisplayManagerLeft
	{
		class components;
	};
	class VehicleSystemsTemplateRightCommander: DefaultVehicleSystemsDisplayManagerRight
	{
		class components;
	};
	class VehicleSystemsTemplateLeftGunner: DefaultVehicleSystemsDisplayManagerLeft
	{
		class components;
	};
	class VehicleSystemsTemplateRightGunner: DefaultVehicleSystemsDisplayManagerRight
	{
		class components;
	};
	
	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;

		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/
		class Redd_Tank_Gepard_1A2_base: Tank_F
		{
			
			#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
			displayname = "$STR_Gepard_1A2";
			picture="\A3\armor_f_beta\APC_Tracked_01\Data\UI\APC_Tracked_01_AA_ca.paa";
			icon="\A3\armor_f_beta\APC_Tracked_01\Data\ui\map_APC_Tracked_01_aa_ca.paa";
			side = 1;
			crew = "B_crew_F";
			author = "ReddNTank";
			model = "\Redd_Tank_Gepard_1A2\Redd_Tank_Gepard_1A2";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_AA";
			smokeLauncherGrenadeCount = 8;
			smokeLauncherVelocity = 14;
			smokeLauncherOnTurret = 1;
			smokeLauncherAngle = 142.5;
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="Redd_Gepard_Driver_out";
			driverInAction="Redd_Gepard_Driver";
			armor = 400;
			armorStructural = 8;
			cost = 1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_3_bound";
			dustFrontRightPos = "wheel_2_3_bound";
			dustBackLeftPos = "wheel_1_8_bound";
			dustBackRightPos = "wheel_2_8_bound";
			viewDriverInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1100; //Pilot
			enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos driver";
			forceHideDriver = 0;
			driverWeaponsInfoType="Redd_RCS_Driver";
			headGforceLeaningFactor[]={0.00075,0,0.0075};

			radarType=2;
			reportRemoteTargets=1;
			receiveRemoteTargets=1;

			threat[]={0.9,0.6,0.9};
			audible = 6;
			camouflage = 7;	
			
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver","commander","gunner"};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver","commander","gunner"};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "commander", "gunner"};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 1;
			acre_infantryPhoneDisableRinging = 0;
			acre_infantryPhoneCustomRinging[] = {};
			acre_infantryPhoneIntercom[] = {"all"};
			acre_infantryPhoneControlActions[] = {"all"};
			acre_eventInfantryPhone = "QFUNC(noApiFunction)";
			//Ende ACRE2
			
			hiddenSelections[] = 
			{
				
				"details",
				"fahrwerk",
				"kleinteile",
				"turm",
				"guns",
				"innen_teile",
				"innen_details",
				"rest",
				"wanne",

				"plate_1",//9
				"plate_2",
				"plate_3",
				"plate_4",
				"plate_5",
				"plate_6",

				"Reg_Btl",//15
				"Komp"//16
				
			};

			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Details.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Details_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Details_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbau.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbau_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbau_destruct.rvmat",
					
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_grob_u_Kleinteile.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_grob_u_Kleinteile_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_grob_u_Kleinteile_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Gross_Turm.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Gross_Turm_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Gross_Turm_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Guns.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Guns_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Guns_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innen_Teile.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innen_Teile_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innen_Teile_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innen_Teile_Details.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innen_Teile_Details_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innen_Teile_Details_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innenraum_Rest.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innenraum_Rest_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Innenraum_Rest_destruct.rvmat",

					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Wanne.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Wanne_damage.rvmat",
					"Redd_Tank_Gepard_1A2\mats\Redd_Tank_Gepard_1A2_Wanne_destruct.rvmat"

				};
				
        	};

			class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};

			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust 1";
					direction = "exhaust_dir 1";
					effect = "ExhaustEffectTankBack";
					
				};

				class Exhaust2
				{
					
					position = "exhaust 2";
					direction = "exhaust_dir 2";
					effect = "ExhaustEffectTankBack";
					
				};
				
			};

			class Reflectors 
			{

				class Left 
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
					intensity = 1;
					useFlare = 0;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };

				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Orangelight
				{

					color[]={236, 99, 17};
					ambient[]={10,10,10};
					position="pos_orangelicht";
					direction="dir_orangelicht";
					hitpoint="";
					selection="pos_orangelicht";
					size=2;
					innerAngle=0;
					outerAngle=100;
					coneFadeCoef=1;
					intensity=250;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Orangelight_flare
				{

					color[]={236, 99, 17};
					ambient[]={0,0,0};
					position="pos_orangelicht_flare";
					direction="dir_orangelicht_flare";
					hitpoint="";
					selection="pos_orangelicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=250;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};

			};

			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull 
				{	
				
					armor=2;
					material=-1;
					name="hull";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.3;
					armorComponent="";

				};

				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="DamageVisual";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.21;
					armorComponent="";

				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="engine";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.3;
					armorComponent="";

				};
				
				class HitLTrack: HitLTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					armorComponent="";

				};
				
				class HitRTrack: HitRTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					armorComponent="";

				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};
				
			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};
			
			class Components
			{

				class TransportCountermeasuresComponent {};
				
				class SensorsManagerComponent
				{

					class Components
					{

						class ActiveRadarSensorComponent: SensorTemplateActiveRadar
						{

							class AirTarget
							{
								minRange=16000;
								maxRange=16000;
								objectDistanceLimitCoef=-1;
								viewDistanceLimitCoef=-1;
							};
							class GroundTarget
							{
								minRange=1;
								maxRange=1;
								objectDistanceLimitCoef=-1;
								viewDistanceLimitCoef=-1;
							};
							
							typeRecognitionDistance=10000;
							angleRangeHorizontal=360;
							angleRangeVertical=100;
							aimDown=-45;
							minSpeedThreshold=30;
							maxSpeedThreshold=90;

						};

						class DataLinkSensorComponent: SensorTemplateDataLink {};

					};

				};

			};
			
			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets: Turrets 
					{
						
						class CommanderOptics: CommanderOptics //[0,0] Commander
						{
						
							gunnerForceOptics=0;
							gunnerAction = "Redd_Gepard_Commander_Out";
							gunnerInAction="Redd_Gepard_Commander";
							initElev = 0;
							minElev = -10;
							maxElev = 90;
							initTurn = 0;
							minTurn = -360;
							maxTurn = 360;
							viewGunnerInExternal = 1;
							hideProxyInCombat = 1;
							startEngine = 0;
							gunnerGetInAction="GetInHigh";
							gunnerGetOutAction="GetOutHigh";
							turretInfoType = "Redd_RCS_Turret";
							stabilizedInAxes = 3;
							lodTurnedIn = 1000; //Gunner
							lodTurnedOut = 1000; //Gunner
							soundAttenuationTurret = "TankAttenuation";
							gunnerCompartments= "Compartment2";
							weapons[] = {"Redd_SmokeLauncher"};
							magazines[] = {"Redd_SmokeLauncherMag"};
							animationSourceHatch = "";
							ace_fcs_Enabled = 0;
							gunnerOutOpticsModel="";
							outGunnerMayFire=0;
							soundServo[]={};
							soundServoVertical[]={};
							animationSourceStickX="com_turret_control_x";
							animationSourceStickY="com_turret_control_y";
							gunnerRightHandAnimName="com_turret_control";

							class ViewOptics: ViewOptics
							{
								
								visionMode[] = {"Normal", "NVG", "TI"};
								thermalMode[] = {2,3};
								initFov = 0.4;
								minFov = 0.08;
								maxFov = 0.03;
								
							};
							
							class OpticsIn
							{

								class Day1
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.27;
									maxFov = 0.27;
									minFov = 0.27;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Gepard";
									gunnerOpticsEffect[] = {};
												
								};
								
								class Day2
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.06;
									maxFov = 0.06;
									minFov = 0.06;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Gepard";
									gunnerOpticsEffect[] = {};
									
								};
								
							};

							class HitPoints 
							{
								
								class HitTurret	
								{
									
									armor = 1;
									material = -1;
									name = "vezVelitele";
									visual="DamageVisual";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.09;
									armorComponent="";
									isTurret=1;

								};
								
								class HitGun	
								{
									
									armor = 1;
									material = -1;
									name = "zbranVelitele";
									visual="DamageVisual";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.09;
									armorComponent="";
									isGun=1;

								};
								
							};
							
							class Components
							{

								class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftCommander
								{

									class Components: components
									{

										class SensorDisplay
										{

											componentType="SensorsDisplayComponent";
											range[]={16000,8000,4000};
											resource="RscCustomInfoSensors";

										};

									};

								};

								class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightCommander
								{

									defaultDisplay="SensorDisplay";

									class Components: components
									{

										class SensorDisplay
										{

											componentType="SensorsDisplayComponent";
											range[]={16000,8000,4000};
											resource="RscCustomInfoSensors";

										};

									};

								};

							};
							
						};

					};

					//Mainturret [0]
					weapons[] =
					{
						
						"Redd_Gesichert",
						"Redd_35mm"

					};

					magazines[] =
					{
						
						"Redd_35mm_HE_Mag",
						"Redd_35mm_AP_Mag"

					};

					memoryPointGun[]=
					{

						"usti hlavne 1",
						"usti hlavne 2"

					};
					
					startEngine = 0;
					stabilizedInAxes = 3;
					forceHideGunner = 0;
					initElev = 0;
					minElev = -10;
					maxElev = 90;
					initTurn = 0;
					minTurn = -360;
					maxTurn = 360;
					maxHorizontalRotSpeed = 3.2;// 1 = 45°/sec
					maxVerticalRotSpeed = 1.33;
					gunnerForceOptics=0;
					gunnerInAction="Redd_Gepard_Gunner";
					gunnerAction="Redd_Gepard_Gunner_Out";
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetInAction="GetInHigh";
					gunnerGetOutAction="GetOutHigh";
					turretInfoType = "Redd_RCS_Turret";
					discreteDistance[] = {200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,2400,2600,2800,3000,3200,3400,3600,3800,4000,4200,4400,4600,4800,5000};
					discreteDistanceInitIndex = 3;
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					soundAttenuationTurret = "TankAttenuation";
					gunnerCompartments= "Compartment2";
					lockWhenDriverOut = 1;
					animationSourceHatch = "";
					ace_fcs_Enabled = 0;
					gunnerOutOpticsModel="";
					animationSourceStickX="turret_control_x";
					animationSourceStickY="turret_control_y";
					gunnerLeftHandAnimName="turret_control";

					soundServo[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner",
						0.56234133,
						1,
						10
						
					};
					
					soundServoVertical[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner_vertical",
						0.56234133,
						1,
						30
						
					};

					class TurnIn
					{

						limitsArrayTop[]=
						{

							{90, -180},
							{90, 180}
							
						};

						limitsArrayBottom[]=
						{
							
							{0, -180},
							{0, -145},
							{-10, -130},
							{-10,0},
							{-10, 130},
							{0, 145},
							{0, 180}

						};
					
					};

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {2,3};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
						
					class OpticsIn
					{

						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.27;
							maxFov = 0.27;
							minFov = 0.27;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Gepard";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.06;
							maxFov = 0.06;
							minFov = 0.06;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Gepard";
							gunnerOpticsEffect[] = {};
							
						};
						
					};

					class HitPoints 
					{
						
						class HitTurret	
						{
							
							armor = 1;
							material = -1;
							name = "vez";
							visual="DamageVisual";
							passThrough = 0;
							minimalHit = 0.02;
							explosionShielding = 0.3;
							radius = 0.43;
							armorComponent="";
							isTurret=1;

						};
						
						class HitGun	
						{
							
							armor = 1;
							material = -1;
							name = "zbran";
							visual="DamageVisual";
							passThrough = 0;
							minimalHit = 0;
							explosionShielding = 1;
							radius = 0.20;
							armorComponent="";
							isGun=1;

						};
					
					};
					
					class Components
					{

						class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftGunner
						{

							class Components: components
							{

								class SensorDisplay
								{

									componentType="SensorsDisplayComponent";
									range[]={16000,8000,4000};
									resource="RscCustomInfoSensors";

								};

							};

						};

						class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightGunner
						{

							defaultDisplay="SensorDisplay";

							class Components: components
							{

								class SensorDisplay
								{

									componentType="SensorsDisplayComponent";
									range[]={16000,8000,4000};
									resource="RscCustomInfoSensors";

								};

							};

						};
						
					};
					
				};

				class Gepard_Bino_Turret_Com: NewTurret //[1]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 1;
					proxyType= "CPCommander";
					gunnerName="Bino_Com";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutMedium";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment3";
					LODTurnedIn = 1202; //Cargo 2
					LODTurnedOut= 1202; //Cargo 2
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="Redd_Gepard_Commander_Out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="Redd_Gepard_Commander_out_ffv";
					memoryPointsGetInGunner = "pos commander";
                    memoryPointsGetInGunnerDir = "pos commander dir";


					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};

			};

			class AnimationSources
			{
				
				//Radar turn
				class Radar_Rot_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};
				
				//FolgeRadar Hide
				class Hide_Folgeradar_Inaktiv_Source
				{
			
					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};
				class Hide_Folgeradar_Aktiv_Source
				{
			
					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};
				
				//SuchRadar Hide
				class Hide_Suchradar_Inaktiv_Source
				{
			
					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};
				class Hide_Suchradar_Aktiv_Source
				{
			
					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				//Driver hatch
				class HatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				//Turret hatch
				class Turmluke_Rot_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				//Lights
				class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Spiegel_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};

				//Rundumleuchte Hide
				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class Beacons
				{

					source = "user";
					animPeriod = 1;
					initPhase = 0;

				};
				class turn_left
            	{

					source = "user";
					animPeriod = 0.001;
					initPhase = 0;

           		};

				class orangelicht_an: turn_left{};

				class orangelicht_an_flare: turn_left{};

				class flash_35mm_source 
				{
					
					source = "reload"; 
					weapon = "Redd_35mm";
					initPhase = 0;
					
				};
				
				class Gurt_Deckel_source
				{
					 
					source = "user";
					initPhase = 0;
					animPeriod = 0.1;
					
				};

				//Netze
				class netzFleck_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				class netzTrope_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				class netzWinter_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
			};

			class UserActions
			{

				class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'Turmluke_Rot_Source' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Gepard_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Gepard_Bino_In', false, true];"; 
				
				};

				class Spiegel_einklappen
				{
					
					displayName = "$STR_Spiegel_einklappen";
					position = "actionPoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 1];";
				
				};
				
				class Spiegel_ausklappen
				{
					
					displayName = "$STR_Spiegel_ausklappen";
					position = "actionPoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 0];";
				
				};

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_montieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "((player in [commander this]) and (this animationSourcePhase 'Turmluke_Rot_Source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 1) and (alive this)) or ((player in [gunner this]) and (this animationSourcePhase 'Turmluke_Rot_Source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 1) and (alive this))";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animateSource ['Hide_Rundumleuchte_fuss', 0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abmontieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'Turmluke_Rot_Source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this) or ((player in [gunner this]) and (this animationSourcePhase 'Turmluke_Rot_Source' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this))";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animateSource ['Hide_Rundumleuchte_fuss', 1];";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_Gepard_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_Gepard_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_Gepard_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,3] call Redd_fnc_Gepard_flags";

				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet')";
					statement = "[this,'camonet',player] call Redd_fnc_gepard_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet')";
					statement = "[this,'camonet',player] call Redd_fnc_gepard_camonet";

				};

				class fixTurretBug
				{
					displayName = "Fix GetIn Bug"; 
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(alive this)";
					statement = "if (this turretUnit [1] isEqualTo objNull) then {this setVariable ['Redd_Gepard_Bino_In', false, true];[this,[[0,0],false]] remoteExecCall ['lockTurret'];};";
				};
				
			};

			class EventHandlers: EventHandlers 
			{
				
				init = "_this call redd_fnc_Gepard_Init";
				fired = "_this call redd_fnc_Gepard_Fired";
				getOut = "_this call redd_fnc_Gepard_GetOut";
				getIn = "_this call redd_fnc_Gepard_GetIn";
				turnIn = "_this call redd_fnc_Gepard_TurnIn";
				turnOut = "_this call redd_fnc_Gepard_TurnOut";
				
			};

			class Attributes 
			{

				class Redd_Tank_Gepard_1A2_Rgt_Btl_Attribute 
				{

					displayName = "$STR_Rgt_Btl";
					tooltip = "$STR_Rgt_Btl";
					property = "Redd_Tank_Gepard_1A2_Rgt_Btl_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Gepard_1A2_Rgt_Btl', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};

						class Rgt_Btl_1 
						{
							
							name = "1";
							value = "1";
							
						};

						class Rgt_Btl_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Rgt_Btl_3 
						{
							
							name = "3";
							value = "3";
							
						};

						class Rgt_Btl_4
						{
							
							name = "4";
							value = "4";
							
						};

						class Rgt_Btl_5 
						{
							
							name = "5";
							value = "5";
							
						};

						class Rgt_Btl_6 
						{
							
							name = "6";
							value = "6";
							
						};

						class Rgt_Btl_7 
						{
							
							name = "7";
							value = "7";
							
						};

						class Rgt_Btl_8 
						{
							
							name = "8";
							value = "8";
							
						};

						class Rgt_Btl_10 
						{
							
							name = "10";
							value = "10";
							
						};

						class Rgt_Btl_11
						{
							
							name = "11";
							value = "11";
							
						};

						class Rgt_Btl_12
						{
							
							name = "12";
							value = "12";
							
						};

						class Rgt_Btl_13 
						{
							
							name = "13";
							value = "13";
							
						};

						class Rgt_Btl_14 
						{
							
							name = "14";
							value = "14";
							
						};

						class Rgt_Btl_111 
						{
							
							name = "111";
							value = "111";
							
						};

						class Rgt_Btl_112 
						{
							
							name = "112";
							value = "112";
							
						};

						class Rgt_Btl_131 
						{
							
							name = "131";
							value = "131";
							
						};

						class Rgt_Btl_132 
						{
							
							name = "132";
							value = "132";
							
						};

						class Rgt_Btl_141 
						{
							
							name = "141";
							value = "141";
							
						};

						class Rgt_Btl_142 
						{
							
							name = "142";
							value = "142";
							
						};

						class Rgt_Btl_620 
						{
							
							name = "620";
							value = "620";
							
						};

					};

				};

				class Redd_Tank_Gepard_1A2_Kompanie_Attribute 
				{

					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Gepard_1A2_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Gepard_1A2_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

					};

				};

			};

		};
		
		/*	Public class Flecktarn	*/
		class Redd_Tank_Gepard_1A2_Flecktarn: Redd_Tank_Gepard_1A2_base
		{
			
			editorPreview="\Redd_Tank_Gepard_1A2\pictures\Gepard_F_Pre_Picture.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Gepard_1A2_Flecktarn";

			hiddenSelectionsTextures[] = 
			{
				
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Details_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbaut_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_grob_u_Kleinteile_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Gross_Turm_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Guns_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_Details_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innenraum_Rest_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Wanne_blend_co"
				
			};
			
			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Gepard_1A2_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{
						
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Details_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbaut_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_grob_u_Kleinteile_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Gross_Turm_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Guns_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_Details_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innenraum_Rest_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Wanne_blend_co"
						
					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Gepard_1A2_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{
						
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Details_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Fahrwerk_u_Anbaut_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_grob_u_Kleinteile_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Gross_Turm_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Guns_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Innen_Teile_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_Details_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Innenraum_Rest_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Wanne_blend_co"
						
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Gepard_1A2_Winter";
					author = "ReddNTank";
					
					textures[]=
					{
						
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Details_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbaut_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_grob_u_Kleinteile_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Gross_Turm_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Guns_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Innen_Teile_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_Details_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Innenraum_Rest_blend_co",
						"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Wanne_blend_co"
						
					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};
			
		};
		
		/*	Public class Tropentarn	*/
		class Redd_Tank_Gepard_1A2_Tropentarn: Redd_Tank_Gepard_1A2_Flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Gepard_1A2\pictures\Gepard_D_Pre_Picture.paa";
			displayName="$STR_Gepard_1A2_Tropentarn";

			hiddenSelectionsTextures[] = 
			{
				
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Details_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Fahrwerk_u_Anbaut_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_grob_u_Kleinteile_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Gross_Turm_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Guns_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Innen_Teile_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_Details_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Innenraum_Rest_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_D_Wanne_blend_co"
				
			};
			
		};
		
		/*	Public class Wintertarn	*/
		class Redd_Tank_Gepard_1A2_Wintertarn: Redd_Tank_Gepard_1A2_Flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Gepard_1A2\pictures\Gepard_W_Pre_Picture.paa";
			displayName="$STR_Gepard_1A2_Winter";

			hiddenSelectionsTextures[] = 
			{
				
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Details_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Fahrwerk_u_Anbaut_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_grob_u_Kleinteile_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Gross_Turm_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Guns_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Innen_Teile_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_Innen_Teile_Details_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Innenraum_Rest_blend_co",
				"Redd_Tank_Gepard_1A2\data\Redd_Tank_Gepard_1A2_W_Wanne_blend_co"
				
			};
			
		};
		
	};