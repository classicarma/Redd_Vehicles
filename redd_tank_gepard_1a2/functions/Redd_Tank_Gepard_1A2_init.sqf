

	//Gepard init

	params ["_veh"];

	[_veh] spawn redd_fnc_Gepard_Plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_Gepard_Bat_Komp;//spawns function to set battalion and company numbers

	[_veh,[[1],true]] remoteExecCall ['lockTurret']; //Locks Bino turret

	_veh setVariable ['Redd_Gepard_Bino_In', false, true];
	_veh setVariable ['Redd_Gepard_Commander_Out', false,true];//initiates for commander turnout
	_veh setVariable ['Redd_Gepard_Gunner_Out', false,true];//initiates for gunner turnout
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet

	[_veh] spawn Redd_fnc_Gepard_Radar;
	//[_veh] spawn Redd_fnc_Gepard_cartridge_cover;

	