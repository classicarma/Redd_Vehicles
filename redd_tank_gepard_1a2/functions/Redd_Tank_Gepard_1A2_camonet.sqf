

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Sets camonet
	//			 
	//	Example: ;
	//				 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  1: STRING - Camonet, Camonet_large
	//				  2: OBJECT - Player
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_vehicle","_type","_unit"];
	
	_fuelEventhandler = 0; 

	if (_type isEqualTo "camonet") then
	{

		if !(_vehicle getVariable 'has_camonet') then
		{

			_vehicle setVariable ['has_camonet', true,true];

			_vehicle_class = typeOf _vehicle;

			switch (_vehicle_class) do
			{
				
				case "Redd_Tank_Gepard_1A2_Flecktarn":
				{

					_vehicle animateSource ["netzFleck_hide_Source",0];

				};

				case "Redd_Tank_Gepard_1A2_Tropentarn": 
				{

					_vehicle animateSource ["netzTrope_hide_Source",0];

				};

				case "Redd_Tank_Gepard_1A2_Wintertarn":
				{

					_vehicle animateSource ["netzWinter_hide_Source",0];

				};

			};

			{

				_unit_class = typeOf _x;
				_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
				_camouflage_new = _camouflage/100*80;
				_x setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

			}
			forEach crew _vehicle;

		}
		else
		{

			_vehicle setVariable ['has_camonet', false,true];

			_vehicle_class = typeOf _vehicle;

			switch (_vehicle_class) do
			{
				
				case "Redd_Tank_Gepard_1A2_Flecktarn":
				{

					_vehicle animateSource ["netzFleck_hide_Source",1];

				};

				case "Redd_Tank_Gepard_1A2_Tropentarn": 
				{

					_vehicle animateSource ["netzTrope_hide_Source",1];

				};

				case "Redd_Tank_Gepard_1A2_Wintertarn":
				{

					_vehicle animateSource ["netzWinter_hide_Source",1];

				};

			};
			
			{

				_unit_class = typeOf _x;
				_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
				_x setUnitTrait ["camouflageCoef", _camouflage];

			}
			forEach crew _vehicle;

		};

	};

	true