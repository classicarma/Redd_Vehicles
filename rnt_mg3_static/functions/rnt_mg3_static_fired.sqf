

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: FiredEventHandler for MG3
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: N/A
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_veh"];

if (!(_veh getVariable ["firing",false])) then {
	_veh animateSource ["trigger_source",1];
	_veh setVariable ["firing",true,true];
	waitUntil {inputAction "defaultAction" < 1};
	_veh animateSource ["trigger_source", 0];
	_veh setVariable ["firing",false,true];
};