

class CfgWeapons {

    class ItemCore;

    class rnt_mg3_kasten_fake: ItemCore {
        author = "rnt";
		scope = 2;
		displayName = "$STR_rnt_mg3_kasten_fake";
		model = "\rnt_mg3_static\rnt_munkasten_mg3";
		picture = "\redd_vehicles_main\data\rnt_munkasten_mg3_ca";
		descriptionUse = "<t color='#9cf953'>Use: </t>Reload static MG3";
		descriptionShort = "Caliber: 7.62x51mm<br/>Rounds: 100<br/>Used in: MG3";
        
        class ItemInfo {
			allowedSlots[] = {901};
			type = 401;
            mass = 40;
        };
    };
};

class CfgVehicles {

	class Item_Base_F;

	class Item_rnt_mg3_kasten_fake: Item_Base_F {
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_rnt_mg3_kasten_fake";
		author = "rnt";
		editorCategory = "EdCat_Equipment";
		editorSubcategory = "Redd_Cases";
		vehicleClass = "Items";
		model = "\rnt_mg3_static\rnt_munkasten_mg3_h";

		class TransportItems {

			class _xx_rnt_mg3_kasten_fake {
				name = "rnt_mg3_kasten_fake";
				count = 1;
			};
		};
	};

	class LandVehicle;
	class StaticWeapon: LandVehicle{
        class Turrets;
		class MainTurret;
    };
	class StaticMGWeapon: StaticWeapon{};
    class HMG_01_base_F: StaticMGWeapon{};
    class HMG_01_high_base_F: HMG_01_base_F{};

	class rnt_mg3_static: HMG_01_high_base_F {
        displayName="$STR_rnt_mg3_static";
		editorCategory = "Redd_Vehicles";
        editorSubcategory = "Redd_Static";
		model = "\rnt_mg3_static\rnt_mg3_static";
		author="rnt";
		editorPreview="\rnt_mg3_static\pictures\rnt_mg3_static_pre.paa";
		scope=2;
		side=1;
		faction="BLU_F";
		crew="B_Soldier_F";
			
		class Damage {
			tex[]={};
			mat[]= {
				"rnt_mg3_static\mats\rnt_mg3_static.rvmat",
				"rnt_mg3_static\mats\rnt_mg3_static_damage.rvmat",
				"rnt_mg3_static\mats\rnt_mg3_static_destruct.rvmat",

				"rnt_mg3_static\mats\rnt_munkasten_mg3.rvmat",
				"rnt_mg3_static\mats\rnt_munkasten_mg3_damage.rvmat",
				"rnt_mg3_static\mats\rnt_munkasten_mg3_destruct.rvmat",

				"rnt_mg3_static\mats\rnt_sandsack_mg3.rvmat",
				"rnt_mg3_static\mats\rnt_sandsack_mg3_damage.rvmat",
				"rnt_mg3_static\mats\rnt_sandsack_mg3_destruct.rvmat",

				"redd_vehicles_main\mats\Redd_Tank_MGDetails.rvmat",
				"redd_vehicles_main\mats\Redd_Tank_MGDetails_damage.rvmat",
				"redd_vehicles_main\mats\Redd_Tank_MGDetails_destruct.rvmat"
			};
		};

		class assembleInfo {
			primary=0;
			base="";
			assembleTo="";
			dissasembleTo[]= {
				"rnt_mg3_static_barell",
				"rnt_mg3_static_tripod"
			};
			displayName="";
		};

		class Turrets: Turrets {

			class MainTurret: MainTurret {
				weapons[]= {
                    "Redd_MG3_Static"
				};
				magazines[]= {};
				
				initElev = 0;
				minElev = -6;
				maxElev = 6;
				initTurn = 0;
				minTurn = -9.72;
				maxTurn = 9.72;
				memoryPointGun="usti hlavne";
				gunBeg = "usti hlavne3";
				gunEnd = "konec hlavne3";
				maxHorizontalRotSpeed = 0.8;
				maxVerticalRotSpeed = 0.8;
				turretInfoType="Redd_RSC_MG3";
				gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_mg3";
				memoryPointGunnerOptics = "gunnerview2";
				gunnerAction = "rnt_mg3_g";
				gunnerRightHandAnimName = "trigger_1";

				class HitPoints {

					class HitGun {
						armor = 0.6;
						material = -1;
						name = "gun";
						visual = "damage_visual";
						passThrough = 0;
						radius = 0.038;
					};
				};

				class OpticsIn {
					
					class Day1 {
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 0.08;
						maxFov = 0.08;
						minFov = 0.08;
						visionMode[] = {"Normal", "NVG"};
						thermalMode[] = {};
						gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_mg3";
						gunnerOpticsEffect[] = {};
					};
				};
			};
		};

		class AnimationSources {

			//Höhe
			class mg3_up_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 2;
            };

			class mg3_down_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 2;
            };

			//Drehen
			class rotate_mg3_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 2;
            };
		
			//MG3
            class ReloadAnim{
                source="reload";
                weapon = "Redd_MG3_Static";
            };
            class ReloadMagazine {
                source="reloadmagazine";
                weapon = "Redd_MG3_Static";
            };
            class flash_mg3_source {
                source = "reload"; 
                weapon = "Redd_MG3_Static";
                initPhase = 0;
            };
			class trigger_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 0.2;
            };

			class sandsack_hide_Source {	
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

			class revolving {
                source = "revolving";
                weapon = "Redd_MG3_Static";
            };

			class ammo_belt_hide_sorce {	
                source = "user";
                initPhase = 0;
                animPeriod = 0;
            };

			class ammo_belt_2_hide_sorce {	
                source = "user";
                initPhase = 0;
                animPeriod = 0;
            };

			class ammo_belt_3_c_hide_sorce {	
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

			class ammo_belt_3_s_hide_sorce {	
                source = "user";
                initPhase = 0;
                animPeriod = 0;
            };

			class ammo_belt_4_hide_sorce {	
                source = "user";
                initPhase = 0;
                animPeriod = 0;
            };

			class ammo_belt_5_c_hide_sorce {	
                source = "user";
                initPhase = 0;
                animPeriod = 0;
            };

			class ammo_belt_5_s_hide_sorce {	
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

			class ammo_belt_6_hide_sorce {	
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

			class ammo_belt_7_hide_sorce {	
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };
		};

		class UserActions {

			//MG3 höher ext
            class MG3_up_ext {
                displayName = "$STR_mg3_hohe_justieren_hoeher";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
				statement = "[this,'up'] spawn redd_fnc_mg3_adjust_height";
            };

			//MG3 tiefer ext
            class MG3_down_ext {
                displayName = "$STR_mg3_hohe_justieren_tiefer";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
				statement = "[this,'down'] spawn redd_fnc_mg3_adjust_height";
            };

			//MG3 links ext
            class MG3_left_ext {
                displayName = "$STR_mg3_drehen_links";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'left'] spawn redd_fnc_mg3_turn";
            };

			//MG3 rechts ext
            class MG3_right_ext {
                displayName = "$STR_mg3_drehen_rechts";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'right'] spawn redd_fnc_mg3_turn";
            };

			//MG3 höher int
            class MG3_up_int {
                displayName = "$STR_mg3_hohe_justieren_hoeher";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
				statement = "[this,'up'] spawn redd_fnc_mg3_adjust_height";
            };

			//MG3 tiefer int
            class MG3_down_int {
                displayName = "$STR_mg3_hohe_justieren_tiefer";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
				statement = "[this,'down'] spawn redd_fnc_mg3_adjust_height";
            };

			//MG3 links int 
            class MG3_left_int {
                displayName = "$STR_mg3_drehen_links";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'left'] spawn redd_fnc_mg3_turn";
            };

			//MG3 rechts int
            class MG3_right_int {
                displayName = "$STR_mg3_drehen_rechts";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'right'] spawn redd_fnc_mg3_turn";
            };

			//MG3 laden
			class MG3_load {
                displayName = "$STR_mg3_laden";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
				condition = "('rnt_mg3_kasten_fake' in backpackItems player)";
				statement = "[this, player, 'rnt_mg3_kasten_fake','Redd_Mg3_Belt_100'] call redd_fnc_mg3_reload";
            };
		};

		class EventHandlers {
            init = "_this call redd_fnc_mg3_init";
            fired = "_this spawn redd_fnc_mg3_fired";
			getIn = "_this call redd_fnc_mg3_getIn";
        };
	};

	class rnt_mg3_static_ai: rnt_mg3_static {
		displayName="$STR_rnt_mg3_static_ai";

		class Turrets: Turrets {
			class MainTurret: MainTurret {
				weapons[]= {
                    "Redd_MG3_Static_ai"
				};
				magazines[]= {
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai",
					"Redd_Mg3_Belt_100_ai"
				};
			};
		};

		class assembleInfo: assembleInfo {
			primary=0;
			base="";
			assembleTo="";
			dissasembleTo[]= {
				"rnt_mg3_static_barell_ai",
				"rnt_mg3_static_tripod_ai"
			};
			displayName="";
		};

		class AnimationSources: AnimationSources {
			
			//MG3
            class ReloadAnim{
                source="reload";
                weapon = "Redd_MG3_Static_ai";
            };
            class ReloadMagazine {
                source="reloadmagazine";
                weapon = "Redd_MG3_Static_ai";
            };
            class flash_mg3_source {
                source = "reload"; 
                weapon = "Redd_MG3_Static_ai";
                initPhase = 0;
            };

			class revolving {
                source = "revolving";
                weapon = "Redd_MG3_Static_ai";
            };
		};

		class UserActions: UserActions {

			//MG3 laden
			class MG3_load {
                displayName = "$STR_mg3_laden";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "false";
				statement = "[this, player, 'Redd_Mg3_Belt_100_fake','Redd_Mg3_Belt_100'] call redd_fnc_mg3_reload";
            };
		};
	};
};