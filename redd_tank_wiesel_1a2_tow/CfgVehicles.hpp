

	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;
			
		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/
		class Redd_Tank_Wiesel_1A2_TOW_base: Tank_F
		{
			
			#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
			displayname = "$STR_Wiesel_1A2_TOW";
			picture="\A3\armor_f_beta\APC_Tracked_01\Data\UI\APC_Tracked_01_base_ca.paa";
			icon="\A3\armor_f_beta\APC_Tracked_01\Data\UI\map_APC_Tracked_01_CA.paa";
			side = 1;
			crew = "B_crew_F";
			author = "ReddnTank";
			model = "\Redd_Tank_Wiesel_1A2_TOW\Redd_Tank_Wiesel_1A2_TOW";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Waffentraeger";
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
			driverInAction="Redd_Tank_Wiesel_1A2_TOW_Driver";
			armor = 150;
			armorStructural = 6;
			cost = 1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_3_bound";
			dustFrontRightPos = "wheel_2_3_bound";
			dustBackLeftPos = "wheel_1_5_bound";
			dustBackRightPos = "wheel_2_5_bound";
			viewDriverInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1100; //Pilot
			enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos gunner";
			forceHideDriver = 0;
			driverWeaponsInfoType="Redd_RCS_Driver";
			headGforceLeaningFactor[]={0.001,0,0.0075};
			
			slingLoadCargoMemoryPoints[] = {"SlingLoadCargo1","SlingLoadCargo2","SlingLoadCargo3","SlingLoadCargo4"};

			threat[]={0.6,0.9,0.6};
			audible = 4;
			camouflage = 5;	

			TFAR_AdditionalLR_Turret[] = {{1},{0,1}};

			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner", {"Turret",{0,1},{1}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"Turret",{0,1},{1}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "commander", "gunner",{"Turret",{0,1},{1}}};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 0;
			//Ende ACRE2

			hiddenSelections[] = 
			{

				"wanne", //0
				"plate_1", //1
				"plate_2", //2
				"plate_3", //3
				"plate_4", //4
				"plate_5", //5
				"plate_6", //6

				"TakZeichen", //7
				"Bataillon", //8
				"Kompanie" //9
				
			};
			
			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"Redd_Tank_Wiesel_1A2_TOW\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne.rvmat",
					"Redd_Tank_Wiesel_1A2_TOW\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne_damage.rvmat",
					"Redd_Tank_Wiesel_1A2_TOW\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne_destruct.rvmat"

				};
				
        	};
			
			class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};

			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust";
					direction = "exhaust_dir";
					effect = "ExhaustsEffect";
					
				};
				
			};

			class Reflectors 
			{

				class Left 
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
                    position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
                    size = 1;
                    innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
                    intensity = 1;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };

				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

			};

			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull 
				{	
				
					armor=2;
					material=-1;
					name="hull";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.15;
					armorComponent="";

				};
				
				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="DamageVisual";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.02;
					armorComponent="";

				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="engine";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.15;
					armorComponent="";

				};
				
				class HitLTrack: HitLTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.1;
					armorComponent="";

				};
				
				class HitRTrack: HitRTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.1;
					armorComponent="";

				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks {

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};

			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets: Turrets 
					{

						class CommanderOptic: CommanderOptics //[0,0] Commander
						{
							
							gunnerName = "$STR_Kommandant";
							gunnerForceOptics=0;
							gunnerAction = "Redd_Tank_Wiesel_1A2_TOW_Commander_Out";
							gunnerInAction="Redd_Tank_Wiesel_1A2_TOW_Commander";
							body = "";
							gun = "";
							animationSourceBody = "";
							animationSourceGun = "";
							animationSourceHatch = "Hatch_L_Rear_Source";
							proxyIndex = 1;
							proxyType= "CPGunner";
							viewGunnerInExternal = 1;
							hideProxyInCombat = 1;
							gunnerGetInAction="GetInMedium";
							gunnerGetOutAction="GetOutMedium";
							lodTurnedIn = 1000; //Gunner
							lodTurnedOut = 1000; //Gunner
							soundAttenuationTurret = "TankAttenuation";
							gunnerCompartments= "Compartment2";
							memoryPointGunnerOptics = "commanderview";
							gunnerOpticsModel = "\a3\weapons_f\reticle\Optics_Driver_01_F";
							memoryPointsGetInGunner= "pos commander";
							memoryPointsGetInGunnerDir= "pos commander dir";
							turretInfoType = "";
							weapons[] = {};
							magazines[] = {};
							gunnerOutOpticsModel="";
							soundServo[]={};
							soundServoVertical[]={};
							startEngine = 0;

							class ViewOptics: ViewOptics 
							{
								
								initFov = 1;
								maxFov = 1;
								minFov = 1;
								visionMode[] = {"Normal","NVG"};
								
							};

						};

						class LoaderOptic: CommanderOptic //[0,1] Loader
						{
							
							gunnerName = "$STR_Ladeschuetze";
							gunnerAction = "Redd_Tank_Wiesel_1A2_TOW_Loader_Out";
							gunnerInAction="Redd_Tank_Wiesel_1A2_TOW_Loader";
							animationSourceHatch = "Hatch_R_Rear_Source";
							proxyIndex = 2;
							memoryPointGunnerOptics = "loaderview";
							memoryPointsGetInGunner= "pos loader";
							memoryPointsGetInGunnerDir= "pos loader dir";
							primaryObserver = 0;
							commanding = 0;
						
							class ViewOptics: ViewOptics 
							{
								
								initFov = 1;
								maxFov = 1;
								minFov = 1;
								visionMode[] = {"Normal","NVG"};
								
							};
							
						};

					};

					//Mainturret [0]
					body = "TOW_Turret";
					gun = "TOW_Gun";
					animationSourceBody = "TOW_Turret_Source";
					animationSourceGun = "TOW_Gun_Source";
					proxyindex = 1;
					gunnerName = "$STR_TOW";
					primaryObserver = 0;
					weapons[] ={"Redd_TOW"};
					magazines[] = 
					{	
					
						"Redd_TOW_Mag",
						"Redd_TOW_Mag",
						"Redd_TOW_Mag",
						"Redd_TOW_Mag",
						"Redd_TOW_Mag",
						"Redd_TOW_Mag"
				
					};
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -15;
					maxTurn = 30;
					initTurn = 0;
					stabilizedInAxes = 3;
					gunnerGetOutAction = "GetOutMedium";
					turretInfoType = "Redd_RSC_Milan";
					gunnerForceOptics=0;
					canHideGunner = 0; //0 für GunnerOut 
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment3";
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					startEngine = 0;
					gunnerRightHandAnimName = "TOW_Turret";
					gunnerLeftHandAnimName = "TOW_Turret";
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerAction="Redd_Tank_Wiesel_1A2_TOW_Commander_TOW";
					memoryPointGunnerOutOptics = "TOW_View";
					memoryPointsGetInGunner= "pos commander";
					memoryPointsGetInGunnerDir= "pos commander dir";
					lockWhenDriverOut = 1;
					ace_fcs_Enabled = 0;
					outGunnerMayFire = 1;
					gunnerOutOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_TOW_T2"; //Rein für Gunner Out


					class OpticsOut //So addaptieren für ändern für Gunner Out
					{
						
						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 1.25;
							minFov = 0.25;
							visionMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_TOW_T2";
							gunnerOpticsEffect[] = {};
										
						};
						
					};

					class OpticsIn
					{
						
						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.175;
							maxFov = 0.175;
							minFov = 0.175;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_TOW_T2";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.02;
							maxFov = 0.02;
							minFov = 0.02;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_TOW_T2";
							gunnerOpticsEffect[] = {};
							
						};

						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.125;
							maxFov = 0.125;
							minFov = 0.125;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_TOW_T2";
							gunnerOpticsEffect[] = {};
							
						};

						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.02;
							maxFov = 0.02;
							minFov = 0.02;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_TOW_T2";
							gunnerOpticsEffect[] = {};
							
						};
						
					};

				};

				class Loader_MG3: NewTurret //[1]
				{
					
					body = "MG_Turret";
					gun = "MG_Gun";
					animationSourceBody = "MG_Turret_Source";
					animationSourceGun = "MG_Gun_Source";
					weapons[] = {"Redd_MG3"};
                    magazines[] = 
                    {

                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120",
                        "Redd_Mg3_Mag_120"
                    
                    };
					gunnerName = "$STR_MG3";
					proxyIndex = 2;
					gunnerAction = "Redd_Tank_Wiesel_1A2_TOW_Loader_MG";
					stabilizedInAxes = 3;
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetOutAction="GetOutMedium";
					commanding = 0;
					primaryGunner = 0;
					dontCreateAi = 1;
					gunnerOutOpticsModel = "\A3\weapons_f\reticle\optics_empty";
					memoryPointGunnerOutOptics= "gunnerview2";
					turretInfoType="Redd_RSC_MG3";
					discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
					discreteDistanceInitIndex = 4;
					gunnerRightHandAnimName = "MG_Recoil";
				    gunnerLeftHandAnimName = "MG_Gun";
					memoryPointsGetInGunner= "pos loader";
					memoryPointsGetInGunnerDir= "dir loader dir";
					forceHideGunner = 1;
					gunnerCompartments= "Compartment4";
					//lodTurnedIn = 1000;//Gunner
					lodTurnedOut = 1000;//Gunner
					initElev = 0;
					minElev = -7;
					maxElev = 50;
					initTurn = 0;
					minTurn = -45;
					maxTurn = 38;
					disableSoundAttenuation = 1;
					canHideGunner = 0;
					outGunnerMayFire=1;
					startEngine = 0;
					lockWhenDriverOut = 1;

					class TurnIn
					{

						limitsArrayTop[]=
						{

							{50, -45},
							{50, 38}
							
						};

						limitsArrayBottom[]=
						{

							{-15, -45},
							{-15,-10},
							{-7, 0},
							{-7, 38}

						};
					
					};
					
                    class ViewOptics: ViewOptics 
			        {
				
				        initFov = 0.4;
			        	maxFov = 0.5;
				        minFov = 0.1;
				        visionMode[] = {"Normal","NVG"};
				
			        };

					class OpticsOut //So addaptieren für ändern für Gunner Out
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 1.25;
							minFov = 0.25;
							visionMode[] = {};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};

					};

					class OpticsIn
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.4;
							maxFov = 0.4;
							minFov = 0.4;
							visionMode[] = {"Normal", "NVG"};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};

						class Narrow
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.2;
							maxFov = 0.2;
							minFov = 0.2;
							visionMode[] = {"Normal", "NVG"};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};

					};

                };

				class Bino_Turret_Com: NewTurret //[2]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 1;
					proxyType= "CPGunner";
					gunnerName="Bino_Com";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutMedium";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment5";
					LODTurnedIn = 1000; //Gunner
					LODTurnedOut= 1000; //Gunner
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="rnt_wieselTOW_com_out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="rnt_wieselTOW_com_out_ffv";
					memoryPointsGetInGunner= "pos commander";
					memoryPointsGetInGunnerDir= "pos commander dir";

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};

				class Bino_Turret_Loader: NewTurret //[3]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 2;
					proxyType= "CPGunner";
					gunnerName="Bino_Loader";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutMedium";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment6";
					LODTurnedIn = 1000; //Gunner
					LODTurnedOut= 1000; //Gunner
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="rnt_wieselTOW_loader_out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="rnt_wieselTOW_loader_out_ffv";
					memoryPointsGetInGunner= "pos loader";
					memoryPointsGetInGunnerDir= "pos loader dir";

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};

			};

			class AnimationSources
			{
				
				class HatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class Hatch_L_Rear_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};

				class Hatch_R_Rear_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};
				
				class Seat_R_Trans
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0.5;
					
				};

				class Seat_L_Trans
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0.5;
					
				};

				class ReloadAnim
                {

                    source="reload";
                    weapon = "Redd_MG3";

                };

			    class ReloadMagazine
                {

                    source="reloadmagazine";
                    weapon = "Redd_MG3";
                    
                };

				class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class TOW_Hide_Rohr
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class flash_mg3_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MG3";
					initPhase = 0;
					
				};

				//Netze
				class netz_boden_f_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				class netz_boden_d_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				class netz_boden_w_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};

				class netz_wanne_f_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				class netz_wanne_d_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				class netz_wanne_w_hide_Source
				{
					source = "user";
					initPhase = 1;
					animPeriod = 0;
				};
				
			};

			class UserActions
			{
				
				class TOW_in
				{
					
					displayName = "$STR_TOW_benutzen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'Hatch_L_Rear_Source' == 1) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this animate ['Seat_L_Trans', 1];this animate ['TOW_Hide_Rohr', 0];";
					
				};

				class TOW_aus
				{
					
					displayName = "$STR_TOW_verlassen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this animate ['Seat_L_Trans', 0];this animate ['TOW_Hide_Rohr', 1];";
					
				};

				class MG3_in
				{
					
					displayName = "$STR_MG3_benutzen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,1] == player) and (this animationSourcePhase 'Hatch_R_Rear_Source' == 1) and (alive this)"; 
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,1],true]] remoteExecCall ['lockTurret'];this animate ['Seat_R_Trans', 1];";
     
				};

				class MG3_aus
				{
					
					displayName = "$STR_MG3_verlassen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,1]];[this,[[0,1],false]] remoteExecCall ['lockTurret'];this animate ['Seat_R_Trans', 0];";
					
				};

				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'Hatch_L_Rear_Source' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [2]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_WieselTOW_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [2] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_WieselTOW_Bino_In', false, true];"; 
				
				};

				class Bino2_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,1] == player) and (this animationSourcePhase 'Hatch_R_Rear_Source' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [3]];[this,[[0,1],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_WieselTOW_Bino2_In', true, true];"; 
				
				};

				class Bino2_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [3] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,1]];[this,[[0,1],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_WieselTOW_Bino2_In', false, true];"; 
				
				};

                class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this];";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and (this getVariable 'has_flag')) or ((player in [commander this]) and (this animationSourcePhase 'Hatch_L_Rear_Source' == 1) and (this getVariable 'has_flag'))";
					statement = "[this,0] call Redd_fnc_wiesel_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and !(this getVariable 'has_flag')) or ((player in [commander this]) and (this animationSourcePhase 'Hatch_L_Rear_Source' == 1) and !(this getVariable 'has_flag'))";
					statement = "[this,1] call Redd_fnc_wiesel_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and !(this getVariable 'has_flag')) or ((player in [commander this]) and (this animationSourcePhase 'Hatch_L_Rear_Source' == 1) and !(this getVariable 'has_flag'))";
					statement = "[this,2] call Redd_fnc_wiesel_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(!(player in this) and !(this getVariable 'has_flag')) or ((player in [commander this]) and (this animationSourcePhase 'Hatch_L_Rear_Source' == 1) and !(this getVariable 'has_flag'))";
					statement = "[this,3] call Redd_fnc_wiesel_flags";

				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_wiesel_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_wiesel_camonet";

				};

				class Tarnnetz_Boden_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_wiesel_camonet";

				};

				class Tarnnetz_Boden_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and (this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_wiesel_camonet";

				};
				
				class fixTurretBug
				{

					displayName = "Fix GetIn Bug"; 
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(alive this)";
					statement = "if (this turretUnit [0] isEqualTo objNull) then {[this,[[0,0],false]] remoteExecCall ['lockTurret']};if (this turretUnit [1] isEqualTo objNull) then {[this,[[0,1],false]] remoteExecCall ['lockTurret']};";

				};
				
			};

			class EventHandlers: EventHandlers 
			{

				init = "_this call redd_fnc_Wiesel_Init";
				getOut = "_this call redd_fnc_Wiesel_GetOut";
				getOut = "_this call redd_fnc_Wiesel_GetIn";

			};

			class Attributes 
			{
				
				class Redd_Tank_Fuchs_1A4_Jg_Waffengattung_Attribute 
				{

					displayName = "$STR_Waffengattung";
					tooltip = "$STR_Waffengattung";
					property = "Redd_Tank_Wiesel_1A2_TOW_Waffengattung_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A2_TOW_Waffengattung', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Jg
						{
							
							name = "Jaeger";
							value = "1";
							
						};

                        class GebJg 
						{
							
							name = "Gebirgsjaeger";
							value = "2";
							
						};

                        class Fallis 
						{
							
							name = "Fallschirmjaeger";
							value = "3";
							
						};

                    };

				};

				class Redd_Tank_Wiesel_1A2_TOW_Bataillon_Attribute 
				{

					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Tank_Wiesel_1A2_TOW_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A2_TOW_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_1 
						{
							
							name = "JgBtl 1";
							value = "1";
							
						};

						class Bataillon_91
						{
							
							name = "JgBtl 91";
							value = "2";
							
						};

						class Bataillon_291 
						{
							
							name = "JgBtl 291";
							value = "3";
							
						};

						class Bataillon_292 
						{
							
							name = "JgBtl 292";
							value = "4";
							
						};
						
						class Bataillon_413 
						{
							
							name = "JgBtl 413";
							value = "5";
							
						};

                        class Bataillon_231
						{
							
							name = "GebJgBtl 231";
							value = "6";
							
						};

                        class Bataillon_232 
						{
							
							name = "GebJgBtl 232";
							value = "7";
							
						};

                        class Bataillon_233
						{
							
							name = "GebJgBtl 233";
							value = "8";
							
						};
						
						class Bataillon_26
						{
							
							name = "FJgReg 26";
							value = "9";
							
						};

						class Bataillon_31
						{
							
							name = "FJgReg 31";
							value = "10";
							
						};

					};

				};

				class Redd_Tank_Wiesel_1A2_TOW_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Wiesel_1A2_TOW_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A2_TOW_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
				
						class Kompanie_5
						{
							
							name = "5";
							value = "1";
							
						};

						class Kompanie_7
						{
							
							name = "7";
							value = "2";
							
						};
						
					};
					
				};

			};

		};
			
		/*	Faction variants	*/
		class Redd_Tank_Wiesel_1A2_TOW_Flecktarn: Redd_Tank_Wiesel_1A2_TOW_base 
		{
		
			editorPreview="\Redd_Tank_Wiesel_1A2_TOW\pictures\Wiesel_TOW_F_Pre_Picture.paa";
			scope = 2;
            scopeCurator = 2;
			displayName="$STR_Wiesel_1A2_TOW_Flecktarn";
			
			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_blend_co.paa"

			};
			
			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Wiesel_1A2_TOW_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};
				class Tropen
				{
					displayName = "$STR_Wiesel_1A2_TOW_Tropentarn";
					author = "ReddNTank";
					textures[]=
					{

						"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_D_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Wiesel_1A2_TOW_Winter"; 
					author = "ReddNTank";
					textures[]=
					{

						"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_W_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};
			
		};
		
		/*	Public class Tropentarn	*/
		class Redd_Tank_Wiesel_1A2_TOW_Tropentarn: Redd_Tank_Wiesel_1A2_TOW_Flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Wiesel_1A2_TOW\pictures\Wiesel_TOW_D_Pre_Picture.paa";
			displayName="$STR_Wiesel_1A2_TOW_Tropentarn";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_D_blend_co.paa"

			};

			
		};
		
		/*	Public class Wintertarn	*/
		class Redd_Tank_Wiesel_1A2_TOW_Wintertarn: Redd_Tank_Wiesel_1A2_TOW_Flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Tank_Wiesel_1A2_TOW\pictures\Wiesel_TOW_W_Pre_Picture.paa";
			displayName="$STR_Wiesel_1A2_TOW_Winter";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_W_blend_co.paa"

			};

		};
		
	};