

	//Wiesel init

	params["_veh"];

	[_veh] spawn redd_fnc_Wiesel_Plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_Wiesel_Bat_Komp;//spawns function to set battalion and company numbers
	
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags
	_veh setVariable ['Redd_WieselTOW_Bino_In', false, true];
	_veh setVariable ['Redd_WieselTOW_Bino2_In', false, true];
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet
	_veh setVariable ['has_camonet_large', false,true];//initiates varibale for large camonet

	[_veh,[[0],true]] remoteExecCall ["lockTurret"]; //TOW
	[_veh,[[1],true]] remoteExecCall ["lockTurret"]; //MG3
	[_veh,[[2],true]] remoteExecCall ["lockTurret"]; //Bino
	[_veh,[[3],true]] remoteExecCall ["lockTurret"]; //Bino2