

First of all, it is not perfect, but we are getting better ;)
Secondly, our vehicles are never meant to be played with AI, we are focusing on coop content. This means, some functions may not work with AI

Redd'n'Tank Vehicles Version 1.12.162
21.01.2020

The Team:
Redd: Config, functions, model, textures
Tank: Model, textures

Redd'n'Tank official website:
http://reddiem.portfoliobox.net/

Discuss AW-Forum (german): 
https://armaworld.de/index.php?thread/3258-redd-vehicles-marder-1a5-release/

Discuss BI-Forum (english):
https://forums.bistudio.com/forums/topic/210296-redd-vehicles-marder-1a5-release/

Steam Workshop:
http://steamcommunity.com/sharedfiles/filedetails/?id=1128145626

YouTube:
https://www.youtube.com/playlist?list=PLJVKqjrLClHTeQcHtncGIJdRyHiTblsdT

Imgur:
https://imgur.com/a/Uv1Ei

GitLab: (See the whole Changelog)
https://gitlab.com/TTTRedd/Redd_Vehicles
pls report issues here

Contact:
redd.arma.modding@gmail.com

Addon Pack Contains:
- Trench Modules - Consists of modular parts for building trenches, can be found under Empty->Structures->RnT Structures
- Static GMW (No AI Vehicle) and Static GMW - AI 
- Static MG3 (No AI Vehicle) and Static MG3 - AI 
- LKW 5t mil gl KAT I transport (No AI Vehicle)
- LKW 5t mil gl KAT I fuel (No AI Vehicle)
- LKW 7t mil gl KAT I transport (No AI Vehicle)
- LKW 7t mil gl KAT I ammunition (No AI Vehicle)
- LKW 10t mil gl KAT I repair (No AI Vehicle)
- Mantis GDF-020
- Mantis Sensor
- SpPz 2A2 "Luchs" (No AI Vehicle)
- LKW leicht gl "Wolf"
- M120 Tampella
- Gepard 1A2
- Wiesel 1A4 MK20 (No AI Vehicle)
- Wiesel 1A2 TOW (No AI Vehicle)
- Fuchs 1A4 Infantry (No AI Vehicle)
- Fuchs 1A4 Infantry-Milan (No AI Vehicle)
- Fuchs 1A4 Engineers (No AI Vehicle)
- Fuchs 1A4 Medic Vehicle (No AI Vehicle)
- Marder 1A5 (No AI Vehicle)
- Static Milan 
- Redd Vehicles Main
- RnT ACE compatibility (If you dont play with ACE delete this pbo from addons folder)

Features:
Trench Modules consists of 
- Bunker module
- Corner module
- End module
- Straight module
- Stand module
- Position module
- 'T' module
Static GMW and Static GMW - AI
- Both versions can be assembled and disassembled
- Both versions have three different heights which can be adjusted via the action menu
- Both versions can be turned left or right via the action menu
- The non AI version has only one magazine at the same time. It needs GMW ammo boxes. Player needs the GMW ammo boxes in his inventory to reload the non AI version. GMW ammo boxes can be carried in only backpacks
- The AI version can be reloaded like a regular vanilla static weapons and has 5 magazines
- To find the boxes search in your Eden editor under empty for "GMW"
Static MG3 and Static MG3 - AI
- Both versions can be assembled and disassembled
- Both versions have three different heights which can be adjusted via the action menu
- Both versions can be turned left or right via the action menu
- The non AI version has only one magazine at the same time. It needs MG3 ammo boxes. Player needs the MG3 ammo boxes in his inventory to reload the non AI version. MG3 ammo boxes can be carried in backpacks
- The AI version can be reloaded like a regular vanilla static weapons and has 8 magazines
- To find the boxes search in your Eden editor under empty for "MG3"
LKW 5t mil gl KAT I transport and fuel
- Woodland, desert and winter camo
- Each version has animated doors
- Each version has animated windows
- Transport version has a removable cover
- Each version has the "forceFlagTexture" ability
LKW 7t mil gl KAT I transport and ammunition
- Woodland, desert and winter camo
- Each version has animated doors
- Each version has animated windows
- Transport version has a removable cover
- Each version has the "forceFlagTexture" ability
LKW 10t mil gl KAT I repair
- Woodland, desert and winter camo
- Animated doors
- Animated windows
- "forceFlagTexture" ability
Mantis GDF-020 and Mantis Sensor
- Autonumus AA gun and radar unit
- Place and enjoy
SpPz 2A2 "Luchs"
- Woodland, desert and winter camo
- Commander has the ability "Climp up" to use Binoculars when turned out
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Customisable Battalion and Company numbers
- Animated door
- Animated Surge Plate
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
LKW leicht gl "Wolf"
- Woodland, desert and winter camo
- ViV transportable
- Slingloadable
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Each version has animated doors
- Each version has animated windows
- Each version has a removable cover
- Each version has the "forceFlagTexture" ability
- Each version has animated antennas
- Commanding and mortar version have mountable rotating beacons
- Medic and MP version have rotating blue lights
- Mortar version has M120 Tampella in its cargo
M120 Tampella
- M120 can be assembled and disassembled
- Custom ammoboxes for each ammo type (To work with ACE)
- ACE compatible
- "forceFlagTexture" ability
Gepard 1A2
- Full animated radar
- Woodland, desert and winter camo
- Customisable Battalion/Regiment and Company numbers
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Commander has the ability "Climp up" to use Binoculars when turned out
Wiesel 1A4 MK20
- Woodland, desert and winter camo
- ViV transportable
- Slingloadable
- Customisable Battalion and Company numbers
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Commander has the ability "Climp up" to use Binoculars when turned out
Wiesel 1A2 TOW
- Woodland, desert and winter camo
- ViV transportable
- Slingloadable
- Customisable Battalion and Company numbers
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Commander has the ability "Climp up" to use Binoculars when turned out
Fuchs 1A4 Medic Vehicle
- Actual Fuchs has two brake pedals, try the difference between normal brake (S) and handbrake (X). We tried to simulate pushing both pedals with the handbrake (X)
- Rotating blue light
- Woodland, desert and winter camo
- Animated doors
- Animated Surge Plate
- Animated Bullet Shield
- Customisable Battalion and Company numbers
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Commander has the ability "Climp up" to use Binoculars when turned out
Fuchs 1A4 Infantry, Infantry-Milan and Engineers
- FFV left and right rear hatch
- Actual Fuchs has two brake pedals, try the difference between normal brake (S) and handbrake (X). We tried to simulate pushing both pedals with the handbrake (X)
- Mountable rotating beacon
- Mountable Milan for middle hatch (Only Infantry-Milan)
- Woodland, desert and winter camo
- Animated doors
- Animated Surge Plate
- Animated Bullet Shield
- A few more little animations
- Customisable Battalion and Company numbers
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Commander has the ability "Climp up" to use Binoculars when turned out
Marder 1A5
- Rear ramp can be opened and closed by driver or rear FFV-seat
- Turned out Commander can assemble Milan on turret
- FFV, rear-back-seat, left-back-seat, right-back-seat
- Marder has Static Milan in its cargo
- Woodland, desert and winter camo
- Customisable Battalion and Company numbers
- "forceFlagTexture" ability
- Animated antennas
- Mountable rotating beacon
- Each version can be covered with a small and/or a large camo net (Vehicle cannot drive with large camo net)
- Commander has the ability "Climp up" to use Binoculars when turned out
Static Milan
- Static Milan can be assembled and disassembled
- "forceFlagTexture" ability

Known issues:
- Localization only english, german and japanese  all other languages display english names
- Thermal vision always starts in black/white after changing the polarity once it has the correct color

Changelog:
Version 1.12.162
- Fixed issue with wrong class dependency in rnt_ace_compatibility.pbo
Version 1.12.161
- Added Trench Modules
- Fixed some .rpt errors in terms of wrong animations
- Fixed an .rpt error in terms of an double stringtable entry
- Updated Mantis ammo count to 2x 252 rounds
- Updated turretInfoType for MG3 on Fuchs, Wiesel TOW, Luchs, 5t, 7t and 10t
Version 1.11.157
- Addes static GMW and static GMW - AI
- Changed function for large camo nets, the function will no longer set the fuel of the vehicle to 0
- Changed rnt_ace_compatibility.pbo moved to addons folder, if you dont play with ACE, just delete it
- Updated japanese translation
Version 1.10.154
- Addes static MG3 and static MG3 - AI
- Fixed typo in stringtable.xml
Version 1.9.153
- Added LKW 5t mil gl KAT I transport and fuel
- Added LKW 7t mil gl KAT I transport and ammunition
- Added LKW 10t mil gl KAT I repair
- Added rnt_ace_compatibility.pbo, if you dont play with ACE, just delete it
when playing with ACE3 copy it and the key to the addons folder to the rest of the pobs otherwise some functions may not work correctly
- Added new editor subclass "Cars" and moved "Wolf" to "Cars"
- Fixed issue with Fuchs glass
- Fixed issue with Wolf glass
Version 1.8.149
- Added Mantis GDF-020
- Added Mantis Sensor
Version 1.7.149
- Added the ability to use a binocular in the "climp up" turret to all vehicle commanders
- Added a black base texture to all wreck models
- Added new turned out animations for all Fuchs middel hatches
- Added camonets to all vehicles (at least one net, either the small or the large one, some will have both)
- Updated Japanese translations (Thanks to classic)
- Adjusted "threat", "audible" and "camouflage" values of some vehicles 
- Fixed flipped clan sign on Luchs
Version 1.7.142
- Added SpPz 2A2 "Luchs"
- Changed Fuchs now spawnes with closed Bullet Shield
- Changed M120 reloadtime
- Fixed bug where wolf wouldn't be refueled after removing the camonet
- Fixed Wolf slingload points where to low, so Wolf tends to turn on the roof while slingloading 
- Fixed added missing TI textures to Wolf
- Fixed added missing TI textures to Wiesel MK20
- Fixed added missing TI textures to all damage and destruct materials
- Fixed model issue with marder commander hatch and milan
- Tweaked all vehicles PhysX
- Tweaked all functions (Thx to commy2) 
Version 1.6.131
- Added the ACE ability to be reloaded from ammotruck to M120 (Thx to madpat3)
- Added the ACE ability to check ammo to M120 (Thx to madpat3)
Version 1.6.129
- Added "Toggle MILS" function to M120 Tampella
- Added warhead type "TandemHEAT" from Tanks DLC to TOW and Milan
- Changed moved all strings in one stringtable in main pbo
- Changed adjusted Fuchs wheels hitpoint parameters
- Fixed added missing TFAR radios to Wolf driver, co-driver and medic seat
- Fixed Wolf clan sign is now connected to rear door 
- Fixed missing military police sign in Wolf military police version distance LODs
- Fixed missing proxies for large winter camonet in Wolf cargo LOD and distance LODs
- Fixed Wolf flipped clan sign
- Fixed missing Wolf versions to zeus
- Fixed Gepard flipped clan sign
- Fixed missing mortar ammoboxes to zeus
- Fixed typo in "Anti-Aircraft" editor subclass
Version 1.6.116
- Added LKW leicht gl "Wolf" as command version, mortar carrier, medic vehicle and military police version
- Added M120 Tampella mortar
- Added a third B.O. lights option to all vehicles, now you can turn on the front and the rear B.O. lights at the same time
- Added mountable beacon for Marder and Gepard (Commander can assemble and disassemble when turned out, driver can turn on and off)
- Added the "forceFlagTexture" ability to all vehicles. Flags can be set by user action, from commander when turned out or from outside of the vehicle, depening on the flag position
- Added missing editor pre pictures
- Added turretstick animations to Marder and Gepard
- Added Milan backpacks ui pre images
- Added missing shortnames for ACRE2 racks
- Added some Japanese translations (Thanks to classic)
- Added missing Marder indicators (speed,rpm,fuel and custom)
- Added textured emitted front lights to all vehicles
- Added cover for periscope mirrors to all vehicles
- Added muzzle flash to all vehicles
- Added animated antennas to Marder, Gepard, Wiesel MK20 and Wiesel TOW
- Added animated cover for cartridge belt ejection to Gepard
- Readded roadway LOD to Marder, Fuchs and Gepard to stand on top of the vehicles
- Changed 35mm sound
- Changed MG3 sound
- Changed Fuchs engine sound
- Changed exterior periscope mirror texture to a glass texture for all vehicles
- Changed some crew animations
- Changed the front B.O. lights for all vehicles, now they have a small light cone instead of just emitted faces
- Changed frontlight flare position for all vehicles
- Changed beacon flares for all vehicles
- Changed proxies for light cones to all vehicles, now they point more downwards
- Changed classnames for static Milan backpacks
- Fixed missing holes in MG3 Barrel
- Fixed wrong Fuchs PI commander turned in position
- Fixed wrong Fuchs Medic cargo turned in positions
- Fixed custom indicator animations to work in MP
- Fixed wrong Marder texture in Gepard model
Version 1.4.86
- Fixed some texture issues on Gepard 1A2
Version 1.4.85
- Added Gepard 1A2
- Added all wreck models, they can be found under "empty->wrecks->Redd'n'Tank Wrecks"
- Added empty Milan tube, can be found under "empty->things->objects"
- Readded static Milan
- Readded Milan to Marder and Fuchs
- Changed static Milan model and Marder Milan model to Tanks model
- Fixed "Fix GetIn Bug" user action radius was 25 meters, now its 5 meters
Version 1.3.79
- Added some Tank DLC config stuff
- Added distance LODs to Fuchs interior
- Added the "Fix GetIn Bug" user action, so you can now manually unlock the positions which sometimes wouldn't unlock and you cannot enter e.g. Fuchs co-driver anymore
- Added second Animation for Wiesel MK20 commander, now he can switch between a low and a high turned out animation (Low is default)
- Changed RSC UI optics for all vehicles and weapons
- Changed Fuchs "Commander Hatch" is now called "Middle Hatch" 
- Changed Marder and Wiesel MK20 turret rotating speed
- Changed new radio model in Fuchs interior
- Tweaked Marder, Wiesel TOW and Wiesel MK20 PhysX
- Tweaked all soundshader
- Tweaked some selection names and removed unnecessary config entries
- Removed Milan on Marder and Fuchs, due to a change in the engine it is not possible to aim with the missile if they are not part of the main turret (Will be fixed by BI)
- Removed static Milan due to an issue with the model.cfg for the backpacks
Version 1.3.66
- Fixed broken Main pbo BI Key
- Fixed broken static Milan backpacks
Version 1.3.64
- Fixed broken FFV positions
- Fixed Marder broken sandbags
Version 1.3.62
- Added Wiesel 1A4 MK20
- Added ACRE2 SEM90 Rack with two SEM70 to all vehicles
- Added support for virtual garage to all vehicles
- Added new optics for all weapons
- Changed all optics and separated day and night vision from thermal vision
- Changed MG3 250 rounds magazine to 120 rounds
- Changed Wiesel TOW now has 6 shots
- Changed Wiesel interior engine sound
- Changed reload animation for all Milan weapons
- Fixed Wiesel TOW can now be slingloaded again
- Cleaned all configs from unnecessary entries
Version 1.2.51
- Fixed Wiesel can now be dropped via parachute from ViV-transport
Version 1.2.50
- Added Wiesel 1A2 TOW
- Changed Milan tube backpack now carries the ammo for static milan
- Fixed MG3 soundshader
- Fixed MK20 soundshader
- Fixed Milan soundshader
- Fixed error in Fuchs stringtable.xml
- Fixed spelling mistake in Static Milan memorypoints "pos_gunner" and "pos_gunner_dir"
Version 1.1.44
- Added simple thermal texture to Fuchs 1A4
- Removed the function to force players to open doors and hatches manually before entering a vehicle for all vehicles. Seats are no longer locked. 
  User actions to open doors or hatches are still available, so you can decide by yourself if you want to open a door first or enter a vehicle vanilla style.
- Fixed Marder can now be refueled via ACE3
- Tweaked B.O. lights for all vehicles
- Tweaked some animations
Version 1.1.38
- Addes Fuchs 1A4 Infantry Group-Milan
- Addes Fuchs 1A4 Medic Vehicle
- Added a function to let players only enter vehicles if the doors/hatches are open (player has to open doors/hatches manually. Changing seats inside while doors are closed is no longer possible)
- Added Fuchs mountable rotating beacon (Commander can assemble and disassemble while turned out, Driver and Co-Driver can turn on and off)
- Added Marder "EngineMOI" PhysX parameter 
- Fixed, set launching positions for Marder smokegrenades on the correct position
- Removed stabilization for Marder maingun
- Changed, MG3 closeShot sound
- Changed german displayname "Beschussschild" -> "Beschussklappe"
- Changed position for Marder gunner to enter the vehicle to the rear ramp (rear ramp has to be open for entering)
- Tweaked Fuchs Gearbox so it will not stuck at 30 km/h at normal acceleration (W)
- Tweaked Marder Gearbox a bit
- Tweaked Marder engine parameters a bit
Version 1.1.25
- Added Fuchs 1A4 Infantry Group
- Added Fuchs 1A4 Engineer Group
- Added new PhysX parameters to Marder
- Added some MG3 mid and distance sounds from BW-Mod (Thanks to Ironie)
- Added 500 rounds MG3 magazines
- Fixed Marder tracks can now be repaired via ACE3
- Changed, Marder maingun is now stabilized in all axes
- Changed, Marder countermeasures moved to commander 
- Changed the spelling of all folders to lower case
- Changed MG3 tracercount
- Changed Redd_smokeLauncher.sqf so it will work for vehicles without turrets
- Changed SoundShaders for MG3
- Tweaked Marder engine parameters
Version 1.0.14
- Added new Redd'n'Tank logo
- Added Marder Drivewheel animation
- Fixed issue where choosing 2nd company shows the letter 1 
- Tweaked Marder Geo LOD, GeoPhysX LOD...again, tell me if it still flips
Version 1.0.10
- Added simple thermal texture to Marder 1A5
- Changed order for all optics to, "Normal" -> "NVG" -> "TI"
- Changed materials for glass
- Tweaked Geo LOD, GeoPhysX LOD and FireGeo LOD a bit
- Tweaked PhysX.hpp now it should drive much more smoother and flips less
Version 1.0.5
- Fixed issue where crew can get killed by small arms from outside of the vehicles, now it is possible to shoot or throw grenades through the rearramp
- Fixed missing modelpart
- Fixed missing track animation
Version 1.0.2
- Added strings for Marder 1A5 Eden attributes in stringtable.xml
- Fixed some commander animation issues

License:
Licensed Under Attribution-NonCommercial-NoDerivs CC BY-NC-ND
https://creativecommons.org/licenses/by-nc-nd/4.0/

Thanks:
- Thanks to T4nk for joining me
- Thanks to Tactical Training Team and TF47 for testing and support
- Thanks to Schubert for all background informations
- Thanks to Pazuzu for original Fuchs config
- Thanks to commy2 for smoke launcher script and general help
- Thanks to Ironie for MG3 sounds
- Thanks to Nobilis for help with support for virtual garage
- Thanks to Golani for promo shots
- Thanks to Herr_KaLeun for Wolf Model 
- Thanks to LittleBassman for Fuchs and Luchs engine sound
- Thanks to classic for Japanese translation 
- Thanks to reyhard for help with the Binocular-turned-out animation
- Last but not least, thanks to our wifes, Redds daughter and Tanks cat for giving us the time to do all this stuff :*