

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: 1. Tracks the height of a mortarshell to delete it 20 meter above ground and spawns an bounding mine on its old position to simulate ari burst mortar ammunition
	//				 2. Creates alightpoint to a flare round
	//			 
	//	Example: this addEventHandler ["fired", {_this spawn Redd_fnc_m120_fired}];
	//			
	//			 		 
	//	Parameter(s): 0: OBJECT - Mortar
	//				  4: Config - Ammo type
	//				  6: Config - Ammo type
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];

	if (_ammo == "Redd_Sh_120mm_AMOS_annz") then
	{

		if !(local _gunner) exitWith {};

		uiSleep 1;
		_posATL = getPosATL  _projectile;
		_height = _posATL select 2;

		while {_height > 10} do
		{

			uiSleep 0.05;
			_posATL = getPosATL  _projectile;
			_height = _posATL select 2;
			
		};
		
		_posATL = getPosATL  _projectile;
		deleteVehicle _projectile;
		
		_fake_ammo = "Redd_APERSBoundingMine_Range_Ammo" createVehicleLocal _posATL;
		hideObjectGlobal _fake_ammo;
		_fake_ammo setDamage 1;
		
	};
	
	if (_ammo == "Redd_Flare_120mm_AMOS_White") then
	{

		[[_projectile, 18, 2, 120000, [0.95,0.95,1]],redd_fnc_m120_flares] remoteExec ["spawn",0];

	};

	true