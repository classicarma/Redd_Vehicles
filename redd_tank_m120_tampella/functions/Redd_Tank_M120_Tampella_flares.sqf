

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd using code from https://armaworld.de/index.php?thread/3665-funktionieren-die-illum-m%C3%B6rsergranaten-vom-mk6-m%C3%B6rser-bei-euch/&postID=29154&highlight=Flare#post29154
	//
	//	Description: Creates a lightpoint to a flare round 
	//			 
	//	Example: [[_projectile, 18, 2, 120000, [0.95,0.95,1]],redd_fnc_m120_flares] remoteExec ["spawn",0];
	//			
	//			 		 
	//	Parameter(s): 0: OBJECT - Projectile
	//				  1: Number - Flare size
	//				  2: Number - Flare brightness
	//				  3: Number - Flare intensity
	//				  4: Number - Flare color
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////

	params ["_projectile", "_flareSize", "_flareBrightness", "_flareIntensity", "_flareColor"];
	
	uiSleep 1;
	_posATL = getPosATL  _projectile;
	_height = _posATL select 2;

	while {_height > 10} do
	{

		uiSleep 0.05;
		_posATL = getPosATL  _projectile;
		_height = _posATL select 2;

	};

	_dummy_pos = getPosATL _projectile;
	deleteVehicle _projectile;
	_dummy_flare = "F_20mm_White_Infinite" createVehicleLocal [_dummy_pos select 0, _dummy_pos select 1, 200];
	_light = "#lightpoint" createVehicleLocal [_dummy_pos select 0, _dummy_pos select 1, 200];
	_light attachTo [_dummy_flare];
	_dummy_flare setVelocity [0,0,-2];
	_light setLightUseFlare true;
	_light setLightFlareSize _flareSize;
	_light setLightBrightness _flareBrightness;
	_light setLightIntensity _flareIntensity;
	_light setLightColor _flareColor;
	_light setLightAmbient _flareColor;
	_light setLightDayLight true;
	_light setLightFlareMaxDistance 12000;
	uiSleep 60;
	deleteVehicle _light;
	deleteVehicle _dummy_flare;