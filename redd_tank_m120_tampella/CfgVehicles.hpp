

	class CfgVehicles
	{
		
		class LandVehicle;

		class StaticWeapon: LandVehicle
		{

			class Turrets
			{

				class MainTurret;

			};

		};

		class StaticMortar: StaticWeapon
		{

			class Turrets: Turrets
			{

				class MainTurret: MainTurret
				{

					class ViewOptics;

				};

			};

		};

		class Mortar_01_base_F: StaticMortar 
		{

			class EventHandlers;
			class assembleInfo;
			
		};

		class Redd_Tank_M120_Tampella_Base: Mortar_01_base_F
		{

			picture="\A3\Static_f\Mortar_01\data\UI\Mortar_01_ca.paa";
			icon="\A3\Static_f\Mortar_01\data\UI\map_Mortar_01_CA.paa";
			side=1;
			crew = "B_Soldier_F";
			author = "Redd,Tank";
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Static";
			model="\Redd_Tank_M120_Tampella\Redd_Tank_M120_Tampella.p3d";
			gunnerCanSee = "31+32+14";
			availableForSupportTypes[]={"Artillery"};
			transportSoldier = 1;
			typicalCargo[] = {"B_Soldier_F"};
			memoryPointsGetInCargo = "pos cargo";
			memoryPointsGetInCargoDir = "pos cargo dir";

			hiddenSelections[]={"moerser"};
			hiddenSelectionsTextures[]={"Redd_Tank_M120_Tampella\data\Redd_Tank_M120_Tampella_blend_co.paa"};
			
			class Damage
			{

				tex[]={};
				mat[]=
				{

					"Redd_Tank_M120_Tampella\mats\Redd_Tank_M120_Tampella_Moerser.rvmat",
					"Redd_Tank_M120_Tampella\mats\Redd_Tank_M120_Tampella_Moerser_damage.rvmat",
					"Redd_Tank_M120_Tampella\mats\Redd_Tank_M120_Tampella_Moerser_destruct.rvmat"
					
				};

			};

			class Turrets: Turrets
			{
				class MainTurret: MainTurret
				{
					
					gunnergetInAction="GetInMortar";
					getOutAction="GetOutLow";
					minelev=-27;
					maxelev=10;
					maxHorizontalRotSpeed = 0.35;
					maxVerticalRotSpeed = 0.35;
					hideWeaponsGunner = 0;
					memoryPointsGetInGunner = "pos gunner";
					memoryPointsGetInGunnerDir = "pos gunner dir";
					
					weapons[]=
					{

						"Redd_mortar_120mm"

					};

					magazines[]=
					{
						
						"Redd_8Rnd_120mm_Mo_shells",
						"Redd_8Rnd_120mm_Mo_shells",
						"Redd_8Rnd_120mm_Mo_shells",
						"Redd_8Rnd_120mm_Mo_annz_shells",
						"Redd_8Rnd_120mm_Mo_annz_shells",
						"Redd_8Rnd_120mm_Mo_annz_shells",
						"Redd_8Rnd_120mm_Mo_Flare_white",
						"Redd_8Rnd_120mm_Mo_Smoke_white"

					};

					class ViewOptics: ViewOptics
					{

						initFov=0.5;
						minFov=0.1;
						maxFov=0.5;

						visionMode[]=
						{
							"Normal"
						};

					};

					class OpticsIn
					{

						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 0.75;
							minFov = 0.75;
							visionMode[] = {"Normal"};
							thermalMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_m120";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.08;
							maxFov = 0.08;
							minFov = 0.08;
							visionMode[] = {"Normal"};
							thermalMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_m120";
							gunnerOpticsEffect[] = {};
							
						};

					};

					class HitPoints
					{

						class HitGun
						{

							armor = 0.3;
							material = -1;
							name = "Turret";
							visual = "damage_visual";
							passThrough = 0;
							radius = 0.07;

						};

					};

				};

			};

			class assembleInfo: assembleInfo
			{

				dissasembleTo[] = 
				{

					"Redd_Tank_M120_Tampella_Barrel",
					"Redd_Tank_M120_Tampella_Tripod"
					
				};

			};
			class ACE_Actions
			{

				class M120_ace_unloadMagazine
				{

					displayName = "$STR_Redd_entladen";
					distance = 2;
					condition = "_this call ace_mk6mortar_fnc_canUnloadMagazine";
					statement = "[_target,_player,5] call ace_mk6mortar_fnc_unloadMagazineTimer";
					icon = "";
					selection = "muendung";
				};

				class M120_ace_LoadActions
				{

					displayName = "$STR_Redd_laden";
					distance = 2;
					condition = "[_target,_player] call ace_mk6mortar_fnc_canLoadMagazine";
					statement = "";
					icon = "";
					selection = "muendung";

					class M120_ace_lade_HE
					{

						displayName = "$STR_Redd_lade_HE";
						condition = "[_target,_player,'Redd_1Rnd_120mm_Mo_shells'] call ace_mk6mortar_fnc_canLoadMagazine";
						statement = "[_target,_player,2.5,'Redd_1Rnd_120mm_Mo_shells'] call ace_mk6mortar_fnc_loadMagazineTimer";
						icon = "";

					};

					class M120_ace_lade_HE_annz

					{

						displayName = "$STR_Redd_lade_HE_annz";
						condition = "[_target,_player,'Redd_1Rnd_120mm_Mo_annz_shells'] call ace_mk6mortar_fnc_canLoadMagazine";
						statement = "[_target,_player,2.5,'Redd_1Rnd_120mm_Mo_annz_shells'] call ace_mk6mortar_fnc_loadMagazineTimer";
						icon = "";

					};

					class M120_ace_lade_flare
					{

						displayName = "$STR_Redd_lade_flare";
						condition = "[_target,_player,'Redd_1Rnd_120mm_Mo_Flare_white'] call ace_mk6mortar_fnc_canLoadMagazine";
						statement = "[_target,_player,5,'Redd_1Rnd_120mm_Mo_Flare_white'] call ace_mk6mortar_fnc_loadMagazineTimer";
						icon = "";

					};

					class M120_ace_lade_nebel
					{

						displayName = "$STR_Redd_lade_nebel";
						condition = "[_target,_player,'Redd_1Rnd_120mm_Mo_Smoke_white'] call ace_mk6mortar_fnc_canLoadMagazine";
						statement = "[_target,_player,2.5,'Redd_1Rnd_120mm_Mo_Smoke_white'] call ace_mk6mortar_fnc_loadMagazineTimer";
						icon = "";

					};

				};

				class ACE_MainActions
				{
					displayName="$STR_ACE_Interaction_MainAction";
					distance=2;
					condition="";
					statement="";

					class M120_ace_rearm
					{

						displayName = "$STR_ACE_Rearm_Rearm";
						condition="[_target,_player] call ace_rearm_fnc_canRearm";
						statement="[_target,_player] call ace_rearm_fnc_rearm";
						icon = "\z\ace\addons\rearm\ui\icon_rearm_interact.paa";

					};

					class M120_ace_reload_CheckAmmo
					{

						displayName="$STR_ACE_Reload_checkAmmo";
						condition="[_player, _target] call ace_reload_fnc_canCheckAmmo";
						statement="[_player, _target] call ace_reload_fnc_checkAmmo";

					};

				};	
				
			};

			class ACE_SelfActions
			{

				class M120_ace_toggle_mils
				{

					displayName = "$STR_Redd_Toggle_MILS";
					condition = "true";
					statement = "_this call ace_mk6mortar_fnc_toggleMils";
					exceptions[] = {};
					
				};

       		};

			class UserActions
			{

				class M120_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_m120_flags";

				};

				class M120_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_m120_flags";

				};

				class M120_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_m120_flags";

				};

			};

			class EventHandlers: EventHandlers 
			{
				
				init = "_this call redd_fnc_m120_init";
				fired = "_this spawn redd_fnc_m120_fired";
				
			};

		};

		class Redd_Tank_M120_Tampella: Redd_Tank_M120_Tampella_Base
		{

			editorPreview="Redd_Tank_M120_Tampella\pictures\m_120_pre.paa";
			scope=2;
			scopeCurator = 2;
			displayname = "$STR_Redd_M120_Tampella";

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Redd_M120_Tampella";
					author = "Redd,Tank";

					textures[]={"Redd_Tank_M120_Tampella\data\Redd_Tank_M120_Tampella_blend_co.paa"};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,

			};

		};

	};