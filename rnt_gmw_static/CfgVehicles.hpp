

class CfgWeapons {

    class ItemCore;

    class rnt_gmw_kasten_fake: ItemCore {
        author = "rnt";
		scope = 2;
		displayName = "$STR_rnt_gmw_kasten_fake";
		model = "\rnt_gmw_static\rnt_munkasten_gmw";
		picture = "\redd_vehicles_main\data\rnt_munkasten_gmw_ca";
		descriptionUse = "<t color='#9cf953'>Use: </t>Reload static GMW";
		descriptionShort = "Caliber: 40x53mm<br/>Rounds: 32<br/>Used in: GMW 40mm";
        
        class ItemInfo {
			allowedSlots[] = {901};
			type = 401;
            mass = 160;
        };
    };
};

class CfgVehicles {

	class Item_Base_F;

	class Item_rnt_gmw_kasten_fake: Item_Base_F {
		scope = 2;
		scopeCurator = 2;
		displayName = "$STR_rnt_gmw_kasten_fake";
		author = "rnt";
		editorCategory = "EdCat_Equipment";
		editorSubcategory = "Redd_Cases";
		vehicleClass = "Items";
		model = "\rnt_gmw_static\rnt_munkasten_gmw_h";

		class TransportItems {

			class _xx_rnt_gmw_kasten_fake {
				name = "rnt_gmw_kasten_fake";
				count = 1;
			};
		};
	};

	class LandVehicle;
	class StaticWeapon: LandVehicle{
        class Turrets;
		class MainTurret;
    };
	class StaticGrenadeLauncher: StaticWeapon{};
    class GMG_TriPod: StaticGrenadeLauncher{};
    class GMG_01_base_F: GMG_TriPod{};

    class rnt_gmw_static: GMG_01_base_F {
        displayName="$STR_rnt_gmw_static";
		editorCategory = "Redd_Vehicles";
        editorSubcategory = "Redd_Static";
		model = "\rnt_gmw_static\rnt_gmw_static";
		author="rnt";
		editorPreview="\rnt_gmw_static\pictures\rnt_gmw_static_pre.paa";
		scope=2;
		side=1;
		faction="BLU_F";
		crew="B_Soldier_F";
			
		class Damage {
			tex[]={};
			mat[]= {
				"rnt_gmw_static\mats\rnt_gmw_static.rvmat",
                "rnt_gmw_static\mats\rnt_gmw_static_damage.rvmat",
                "rnt_gmw_static\mats\rnt_gmw_static_destruct.rvmat"
			};
		};

		class assembleInfo {
			primary=0;
			base="";
			assembleTo="";
			dissasembleTo[]= {
				"rnt_gmw_static_barell",
				"rnt_gmw_static_tripod"
			};
			displayName="";
		};

		class Turrets: Turrets {

			class MainTurret: MainTurret {
				weapons[]= {
                    "Redd_gmw_Static"
				};
				magazines[]= {};

				initElev = 0;
				minElev = -15;
				maxElev = 15;
				initTurn = 0;
				minTurn = -20;
				maxTurn = 20;
				memoryPointGun="usti hlavne";
				gunBeg = "usti hlavne3";
				gunEnd = "konec hlavne3";
				maxHorizontalRotSpeed = 0.8;
				maxVerticalRotSpeed = 0.8;
				turretInfoType="Redd_RSC_MG3";
				gunnerOpticsModel = "\rnt_gmw_static\data\gmw_optic";
				memoryPointGunnerOptics = "gunnerview2";
				gunnerAction = "rnt_gmw_g";
				gunnerRightHandAnimName = "trigger";
				gunnerLeftHandAnimName = "trigger";
				discreteDistance[]={500};
				discreteDistanceInitIndex=0;
				memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";

				class HitPoints {

					class HitGun {
						armor = 0.6;
						material = -1;
						name = "gun";
						visual = "damage_visual";
						passThrough = 0;
						radius = 0.024;
					};
				};

				class OpticsIn {
					
					class Day1 {
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 0.5;
						maxFov = 0.5;
						minFov = 0.5;
						visionMode[] = {"Normal", "NVG"};
						thermalMode[] = {};
						gunnerOpticsModel = "\rnt_gmw_static\data\gmw_optic";
						gunnerOpticsEffect[] = {};
					};
				};
			};
		};

		class AnimationSources {
			
			//GMW
            class ReloadAnim {
                source="reload";
                weapon = "Redd_gmw_Static";
            };
			
            class ReloadMagazine {
                source="reloadmagazine";
                weapon = "Redd_gmw_Static";
            };

			class revolving {
                source = "revolving";
                weapon = "Redd_gmw_Static";
            };

			//Höhe
			class gmw_up_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 2;
            };

			class gmw_down_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 2;
            };

			//Drehen
			class rotate_gmw_source {	
                source = "user";
                initPhase = 0;
                animPeriod = 2;
            };
		};

		class UserActions {
			//gmw höher ext
            class gmw_up_ext {
                displayName = "$STR_gmw_hohe_justieren_hoeher";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
				statement = "[this,'up'] spawn redd_fnc_gmw_adjust_height";
            };

			//gmw tiefer ext
            class gmw_down_ext {
                displayName = "$STR_gmw_hohe_justieren_tiefer";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
				statement = "[this,'down'] spawn redd_fnc_gmw_adjust_height";
            };

			//gmw links ext
            class gmw_left_ext {
                displayName = "$STR_gmw_drehen_links";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'left'] spawn redd_fnc_gmw_turn";
            };

			//gmw rechts ext
            class gmw_right_ext {
                displayName = "$STR_gmw_drehen_rechts";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'right'] spawn redd_fnc_gmw_turn";
            };

			//gmw höher int
            class gmw_up_int {
                displayName = "$STR_gmw_hohe_justieren_hoeher";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
				statement = "[this,'up'] spawn redd_fnc_gmw_adjust_height";
            };

			//gmw tiefer int
            class gmw_down_int {
                displayName = "$STR_gmw_hohe_justieren_tiefer";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
				statement = "[this,'down'] spawn redd_fnc_gmw_adjust_height";
            };

			//gmw links int 
            class gmw_left_int {
                displayName = "$STR_gmw_drehen_links";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'left'] spawn redd_fnc_gmw_turn";
            };

			//gmw rechts int
            class gmw_right_int {
                displayName = "$STR_gmw_drehen_rechts";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				statement = "[this,'right'] spawn redd_fnc_gmw_turn";
            };

			//gmw laden
			class gmw_load {
                displayName = "$STR_gmw_laden";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
				condition = "('Redd_gmw_Belt_32_fake' in backpackItems player)";
				statement = "[this, player, 'Redd_gmw_Belt_32_fake','Redd_gmw_Belt_32'] call redd_fnc_gmw_reload";
            };
		};

		class EventHandlers {
			init = "_this call redd_fnc_gmw_init";
			getIn = "_this call redd_fnc_gmw_getIn";
        };
	};

	class rnt_gmw_static_ai: rnt_gmw_static {
		displayName="$STR_rnt_gmw_static_ai";

		class Turrets: Turrets {
			class MainTurret: MainTurret {
				weapons[]= {
                    "Redd_gmw_Static_ai"
				};
				magazines[]= {
					"Redd_gmw_Belt_32_ai",
					"Redd_gmw_Belt_32_ai",
					"Redd_gmw_Belt_32_ai",
					"Redd_gmw_Belt_32_ai",
					"Redd_gmw_Belt_32_ai"
				};
			};
		};

		class assembleInfo: assembleInfo {
			primary=0;
			base="";
			assembleTo="";
			dissasembleTo[]= {
				"rnt_gmw_static_barell_ai",
				"rnt_gmw_static_tripod_ai"
			};
			displayName="";
		};

		class AnimationSources: AnimationSources {

			//GMW
            class ReloadAnim {
                source="reload";
                weapon = "Redd_gmw_Static_ai";
            };
			
            class ReloadMagazine {
                source="reloadmagazine";
                weapon = "Redd_gmw_Static_ai";
            };

			class revolving {
                source = "revolving";
                weapon = "Redd_gmw_Static_ai";
            };
		};

		class UserActions: UserActions {

			//gmw laden
			class gmw_load {
                displayName = "$STR_gmw_laden";
                position = "actionPoint";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "false";
				statement = "[this, player, 'Redd_gmw_Belt_32_fake','Redd_gmw_Belt_32'] call redd_fnc_gmw_reload";
            };
		};
	};
};