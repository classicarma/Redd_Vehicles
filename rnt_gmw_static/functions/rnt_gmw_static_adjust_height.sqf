

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Adjust height for gmw
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: N/A
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_veh","_upDown"];

_veh setVariable ["isInAnimation",true,true];

if (_veh getVariable ["middle",false] && (_upDown == "up")) then {
	_veh setVariable ["middle",false,true];
	_veh setVariable ["up",true,true];
	_veh animateSource ['gmw_up_source', 1];
	waitUntil {_veh animationSourcePhase "gmw_up_source" == 1};
	if (!isNull gunner _veh) then {
		[gunner _veh, "rnt_gmw_g_high"] remoteExec ["switchMove", 0];
	};
} else { 
	if (_veh getVariable ["up",false] && (_upDown == "down")) then {
		_veh setVariable ["middle",true,true];
		_veh setVariable ["up",false,true];
		_veh animateSource ['gmw_up_source', 0];
		waitUntil {_veh animationSourcePhase 'gmw_up_source' == 0};
		if (!isNull gunner _veh) then {
			[gunner _veh, "rnt_gmw_g"] remoteExec ["switchMove", 0];
		};
	} else {
		if (_veh getVariable ["middle",false] && (_upDown == "down")) then{
			_veh setVariable ["middle",false,true];
			_veh setVariable ["down",true,true];
			_veh animateSource ['gmw_down_source', 1];
			waitUntil {_veh animationSourcePhase "gmw_down_source" == 1};
			if (!isNull gunner _veh) then {
				[gunner _veh, "rnt_gmw_g_low"] remoteExec ["switchMove", 0];
			};
		} else {
			if (_veh getVariable ["down",false] && (_upDown == "up")) then {
				_veh setVariable ["middle",true,true];
				_veh setVariable ["down",false,true];
				_veh animateSource ['gmw_down_source', 0];
				waitUntil {_veh animationSourcePhase 'gmw_down_source' == 0};
				if (!isNull gunner _veh) then {
					[gunner _veh, "rnt_gmw_g"] remoteExec ["switchMove", 0];
				};
			};
		};
	};
};

_veh setVariable ["isInAnimation",false,true];