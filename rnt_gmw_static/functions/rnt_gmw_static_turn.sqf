

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Turns the gmw
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: N/A
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_veh","_direction"];

_veh setVariable ["isInAnimation",true,true];
_animationSourcePhase = _veh animationSourcePhase "rotate_gmw_source";

if (_direction == "left") then {
	_veh animateSource ["rotate_gmw_source", _animationSourcePhase + rad 15];
	sleep 0.6;
};

if (_direction == "right") then {
	_veh animateSource ["rotate_gmw_source", _animationSourcePhase - rad 15];
	sleep 0.6;
};

_veh setVariable ["isInAnimation",false,true];