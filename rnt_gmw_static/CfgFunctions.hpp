

class CfgFunctions {

    class Redd {
        tag = "Redd";

        class Functions {
            
            class gmw_init {
                file = "\rnt_gmw_static\functions\rnt_gmw_static_init.sqf";
            };

            class gmw_adjust_height {
                file = "\rnt_gmw_static\functions\rnt_gmw_static_adjust_height.sqf";
            };

            class gmw_getIn {
                file = "\rnt_gmw_static\functions\rnt_gmw_static_getIn.sqf";
            };

            class gmw_turn {
                file = "\rnt_gmw_static\functions\rnt_gmw_static_turn.sqf";
            };

            class gmw_reload {
                file = "\rnt_gmw_static\functions\rnt_gmw_static_reload.sqf";
            };
        };
    };
};