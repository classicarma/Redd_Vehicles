

    class CfgMovesBasic {

        class DefaultDie;
        class ManActions {
            rnt_gmw_g = "rnt_gmw_g";
            KIA_rnt_gmw_g = "KIA_rnt_gmw_g";
            rnt_gmw_g_high = "rnt_gmw_g_high";
            KIA_rnt_gmw_g_high = "KIA_rnt_gmw_g_high";
            rnt_gmw_g_low = "rnt_gmw_g_low";
            KIA_rnt_gmw_g_low = "KIA_rnt_gmw_g_low";
        };
    };

    class CfgMovesMaleSdr: CfgMovesBasic {

        class States {

            class Crew;

            class rnt_gmw_g: Crew {
                file = "\rnt_gmw_static\anims\rnt_gmw_g.rtm";
                interpolateTo[] = {"KIA_rnt_gmw_g",1};
                ConnectTo[]={"KIA_rnt_gmw_g", 1};
            };
            class KIA_rnt_gmw_g: DefaultDie{
                file = "\rnt_gmw_static\anims\KIA_rnt_gmw_g.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};
            }; 
            class rnt_gmw_g_high: Crew {
                file = "\rnt_gmw_static\anims\rnt_gmw_g_high.rtm";
                interpolateTo[] = {"KIA_rnt_gmw_g_high",1};
                ConnectTo[]={"KIA_rnt_gmw_g_high", 1};
            };
            class KIA_rnt_gmw_g_high: DefaultDie{
                file = "\rnt_gmw_static\anims\KIA_rnt_gmw_g_high.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};
            }; 
            class rnt_gmw_g_low: Crew {
                file = "\rnt_gmw_static\anims\rnt_gmw_g_low.rtm";
                interpolateTo[] = {"KIA_rnt_gmw_g_low",1};
                ConnectTo[]={"KIA_rnt_gmw_g_low", 1};
            };
            class KIA_rnt_gmw_g_low: DefaultDie{
                file = "\rnt_gmw_static\anims\KIA_rnt_gmw_g_low.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};
            }; 
        };
    };