

    class CfgVehicles 
    {

        class LandVehicle;

        class StaticWeapon: LandVehicle 
        {

            class Turrets;
            class MainTurret;
            class ViewOptics;
            class EventHandlers;

        };

        class StaticATWeapon: StaticWeapon {};

        class Redd_Milan_Static_Base: StaticATWeapon 
        {
            
            icon = "iconStaticAA";
            picture = "\A3\Static_F_Gamma\data\UI\gear_StaticTurret_AT_CA.paa";

            side=1;
            crew = "B_Soldier_F";
            typicalCargo[] = {"B_Soldier_F"};
            author = "Redd";
            editorCategory = "Redd_Vehicles";
            editorSubcategory = "Redd_Static";
			model = "\Redd_Milan_Static\Redd_Milan_Static";
			gunnerCanSee = "31+32+14";
            threat[] = {0.3,1.0,0.3};
		    cost = 150000;
            armorStructural = 10.0;

            class Turrets: Turrets 
            {

                class MainTurret: MainTurret 
                {

                    initElev = 0;
                    minElev = -15;
                    maxElev = 15;
                    initTurn = 0;
                    minTurn = -180;
                    maxTurn = 180;
                    weapons[] = {"Redd_Milan"};
                    magazines[] = {"Redd_Milan_Mag", "Redd_Milan_Mag", "Redd_Milan_Mag", "Redd_Milan_Mag"};
                    gunnerAction  = "Redd_Milan_Gunner";
                    turretInfoType = "Redd_RSC_Milan";
                    hideWeaponsGunner = 0;
                    gunnergetInAction = "";
				    gunnergetOutAction = "";
				    gunnerLeftHandAnimName = "gun";

                    class OpticsIn 
                    {

                        class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.15;
							maxFov = 0.15;
							minFov = 0.15;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.037;
							maxFov = 0.037;
							minFov = 0.037;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.15;
							maxFov = 0.15;
							minFov = 0.15;
							visionMode[] = {"Ti"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.037;
							maxFov = 0.037;
							minFov = 0.037;
							visionMode[] = {"Ti"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};

                    };

                    class HitPoints
				    {

                        class HitGun
                        {

                            armor = 0.3;
                            material = -1;
                            name = "Turret";
                            visual = "hitTurret";
                            passThrough = 0;
                            radius = 0.07;

                        };

					};

				};
   
            };

            class assembleInfo 
            {

                primary = 0;
                base = "";
                assembleTo = "";
                dissasembleTo[] = 
                {

                    "Redd_Milan_Static_Barrel", 
                    "Redd_Milan_Static_Tripod"
                    
                };
                displayName = "";

            };

            class AnimationSources 
            {
            
                class ReloadMagazine
                {

                    source="reloadmagazine";
                    weapon = "Redd_Milan";
                    
                };
            
            };

			class UserActions
			{

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "this getVariable 'has_flag'";
					statement = "[this,0] call Redd_fnc_milan_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_milan_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_milan_flags";

				};

			};

            class EventHandlers: EventHandlers 
            {
            
				init = "_this call redd_fnc_milan_init";
                fired = "_this call redd_fnc_milan_fired"; 
            
            };
            
            class Damage
		    {

		    	tex[]={};
			    mat[]=
                {

			    	"Redd_Vehicles_Main\mats\Redd_Milan.rvmat",
				    "Redd_Vehicles_Main\mats\Redd_Milan.rvmat",
				    "Redd_Vehicles_Main\mats\Redd_Milan_destruct.rvmat"

			    };

		    };
        
        };

        class Redd_Milan_Static: Redd_Milan_Static_Base 
        {
        
            editorPreview="\Redd_Milan_Static\pictures\Milan_Pre_Picture.paa";	
            scope=2;
            scopeCurator = 2;
            displayname = "$STR_Redd_Milan";

            class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Redd_Milan";
					author = "Redd";

					textures[]={};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,

			};

        };

    };
