	
	
	//Basic parameters
	simulation = carx;
	dampersBumpCoef = 0.05;
	terrainCoef = 0;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 200 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 200;
	
	//Differential parameters
	differentialType = "all_limited";
	frontRearSplit = 0.5;
	frontBias = 1.3;
	rearBias = 1.3;
	centreBias = 1.3;

	//Engine parameters
	maxOmega = 628.32;
	minOmega = 104.72;
	enginePower = 72;
	peakTorque = 200;
	idleRPM = 1000;
	redRPM = 6000;
	clutchStrength = 2;
	dampingRateFullThrottle = 0.08;
	dampingRateZeroThrottleClutchEngaged = 0.35;
	dampingRateZeroThrottleClutchDisengaged = 0.35;	
	maxSpeed = 123;
	thrustDelay = 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef=0.395;
	slowSpeedForwardCoef=0.14;

	torqueCurve[] = 
	{

		{"(0/6000)","(0/200)"},
		{"(857/6000)","(200/200)"},
		{"(1714/6000)","(200/200)"},
		{"(2571/6000)","(200/200)"},
		{"(3429/6000)","(180/200)"},
		{"(4586/6000)","(151/200)"},
		{"(5143/6000)","(125/200)"},
		{"(6000/6000)","(0/200)"}

	};


	//Floating and sinking
	waterPPInVehicle=0;
	maxFordingDepth = -0.85;
	waterResistance = 0;
	canFloat = 0;
	waterLeakiness = 10;

	//Anti-roll bars
	antiRollbarForceCoef = 50;
	antiRollbarForceLimit = 50;
	antiRollbarSpeedMin = 0;
	antiRollbarSpeedMax	= 123;
	
	//Gearbox
	class complexGearbox
	{
		
		GearboxRatios[] = {"R1",-6.2,"N",0,"D1",8,"D2",4.5,"D3",2.8,"D4",2,"D5",1.49};
		TransmissionRatios[]={"High",4};
		gearBoxMode="auto";
		moveOffGear=1;
		driveString="D";
		neutralString="N";
		reverseString="R";
		
	};
	
	changeGearType="rpmratio";
	
	changeGearOmegaRatios[]=
	{

		1.0,0.45,
		0.6,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		1.0,0.6
		
	};
	
	switchTime = 0.75;
	latency = 1.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	driveOnComponent[] = {};
	wheelCircumference = 2.4;
	numberPhysicalWheels = 4;
	turnCoef = 2.5;

	class Wheels
	{

		class LF
		{
			boneName="wheel_1_1_damper";
			steering=1;
			side="left";
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			width = 0.19;
			mass=20;
			MOI=4;
			latStiffX=25;
			latStiffY=180;
			longitudinalStiffnessPerUnitGravity=10000;
			dampingRate=0.1;
			dampingRateDamaged=1;
			dampingRateDestroyed=1000;
			maxBrakeTorque = 2000;
			maxHandBrakeTorque = 0;
			suspTravelDirection[]={0,-1,0};
			suspForceAppPointOffset="wheel_1_1_axis";
			tireForceAppPointOffset="wheel_1_1_axis";
			maxCompression=0.12;
			mMaxDroop=0.12;
			sprungMass=675;
			springStrength=28519;
			springDamperRate=5704;
			damping=75;
			frictionVsSlipGraph[]={{0,1},{0.5,1},{1,1}};

		};
		class LR: LF
		{
			boneName="wheel_1_2_damper";
			steering=0;
			center="wheel_1_2_axis";
			boundary="wheel_1_2_bound";
			maxHandBrakeTorque = 2000*4;
			suspForceAppPointOffset="wheel_1_2_axis";
			tireForceAppPointOffset="wheel_1_2_axis";
			
		};
		class RF: LF
		{
			boneName="wheel_2_1_damper";
			steering=1;
			side="right";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			maxHandBrakeTorque = 0;
			suspForceAppPointOffset="wheel_2_1_axis";
			tireForceAppPointOffset="wheel_2_1_axis";
			
		};
		class RR: RF
		{
			boneName="wheel_2_2_damper";
			steering=0;
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			maxHandBrakeTorque = 2000*4;
			suspForceAppPointOffset="wheel_2_2_axis";
			tireForceAppPointOffset="wheel_2_2_axis";
			

		};
		
	};