    

    class CfgVehicles
	{

		class LandVehicle;

		class Car: LandVehicle 
		{

			class NewTurret;

		};

		class Car_F: Car
		{

			class EventHandlers;
			class AnimationSources;
			class ViewOptics;

			class Turrets 
			{

            	class MainTurret: NewTurret {};

        	};


			class HitPoints
			{

				class HitEngine;
                class HitFuel;
                class HitHull;
				class HitLFWheel;
				class HitLF2Wheel;
				class HitRFWheel;
				class HitRF2Wheel;
				class HitGlass1;
				class HitGlass2;
				class HitGlass3;

			};

		};

        class Redd_Tank_LKW_leicht_gl_Wolf_Base: Car_F
	    {
            
           	#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
            displayName = "$STR_LKW_leicht_gl_Wolf";
			icon = "\A3\Soft_F_Exp\Offroad_02\Data\UI\map_Offroad_02_base_CA.paa";
			picture = "\A3\Soft_F_Exp\Offroad_02\Data\UI\Offroad_02_base_CA.paa";
			side = 1;
			crew = "B_Soldier_F";
            author = "Tank, Redd";
            model = "\Redd_Tank_LKW_leicht_gl_Wolf\Redd_Tank_LKW_leicht_gl_Wolf";
            editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Trucks";
            getInAction = "GetInLow";
		    getOutAction = "GetOutLow"; 
            driverAction = "Redd_Tank_Wolf_Driver";
            driverInAction="Redd_Tank_Wolf_Driver";
            armor = 30;
			dustFrontLeftPos = "TrackFLL";
			dustFrontRightPos = "TrackFRR";
			dustBackLeftPos = "TrackBLL";
			dustBackRightPos = "TrackBRR";
            viewDriverInExternal = 1;
            enableManualFire = 0;
			driverCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos cargo";
			headGforceLeaningFactor[]={0.00075,0,0.0075};
            weapons[] = {"TruckHorn"};
			wheelDamageThreshold = 0.49;
			wheelDestroyThreshold = 0.99;
			wheelDamageRadiusCoef = 0.75;
			wheelDestroyRadiusCoef = 0.475;
			destrType = "DestructWreck";

			typicalCargo[] = {"B_Soldier_F"};
			transportSoldier = 3;
			cargoAction[] = {"Redd_Tank_Wolf_CoDriver","Redd_Tank_Wolf_Passenger1","Redd_Tank_Wolf_Passenger2"};
            cargoProxyIndexes[] = {1,2,3};
			viewCargoInExternal = 1;
			cargoCompartments[] = {"Compartment1"};
			memoryPointTrackFLL = "TrackFLL";
		    memoryPointTrackFLR = "TrackFLR";
		    memoryPointTrackBLL = "TrackBLL";
		    memoryPointTrackBLR = "TrackBLR";
		    memoryPointTrackFRL = "TrackFRL";
		    memoryPointTrackFRR = "TrackFRR";
		    memoryPointTrackBRL = "TrackBRL";
		    memoryPointTrackBRR = "TrackBRR";
			hideWeaponsDriver = 0;
			slingLoadCargoMemoryPoints[] = {"SlingLoadCargo1","SlingLoadCargo2","SlingLoadCargo3","SlingLoadCargo4"};

			threat[]={0.6,0.6,0.6};
            audible = 3;
			camouflage = 3;

			tf_hasLRradio=1;
			tf_isolatedAmount=0.25;
			TFAR_AdditionalLR_Cargo[] = {0};
			
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", {"cargo", 0}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", {"cargo", 0}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};
			
			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					allowedPositions[] = {};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};
			
			acre_hasInfantryPhone = 0;
			//Ende ACRE2
			
            hiddenSelections[] = 
            {
                
                "Reifen_Fahrg_sp",
				"Verdeck_u_kleint_sp",
				"Moerser_Rolle_Teile_Heck_sp",
				"Body_sp",
				"Motorhaube_Tueren_Unterb_sp",
				"Konsolen_sp",
				"San_u_Rahmenteile_sp",
				"aussen_anbaut_sp",
				"ladefl_sitz_sp",
				"innent_detail_sp",

				"plate_1",//10
				"plate_2",
				"plate_3",
				"plate_4",
				"plate_5",
				"plate_6"
				
            };
			
            class PlayerSteeringCoefficients
		    {

				turnIncreaseConst=0.2;
				turnIncreaseLinear=1.5;
				turnIncreaseTime=0.5;

				turnDecreaseConst=3;
				turnDecreaseLinear=0.5;
				turnDecreaseTime=0;

				maxTurnHundred=0.9;
				
            };

			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{
					
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Verdeck_u_kleint_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Verdeck_u_kleint_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Verdeck_u_kleint_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Body_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Body_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Body_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Motorhaube_Tueren_Unterb_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Motorhaube_Tueren_Unterb_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Motorhaube_Tueren_Unterb_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_aussen_anbaut_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_aussen_anbaut_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_aussen_anbaut_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_innent_detail_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_innent_detail_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_innent_detail_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Konsolen_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Konsolen_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Konsolen_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_ladefl_sitz_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_ladefl_sitz_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_ladefl_sitz_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Moerser_Rolle_Teile_Heck_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Moerser_Rolle_Teile_Heck_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Moerser_Rolle_Teile_Heck_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_San_u_Rahmenteile_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_San_u_Rahmenteile_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_San_u_Rahmenteile_sp_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\reflective_glass.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\reflective_glass_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\reflective_glass_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\glass.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\glass_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\glass_destruct.rvmat",

					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Reifen_Fahrg_sp.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Reifen_Fahrg_sp_damage.rvmat",
					"redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_Reifen_Fahrg_sp_destruct.rvmat"

				};
				
        	};

            class Exhausts
		    {

			    class Exhaust1
			    {

				    position = "exhaust1_pos"; 
				    direction = "exhaust1_dir";
				    effect = "ExhaustsEffect";

			    };

		    };

            class Reflectors
		    {
				
                class Left
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
                    position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
                    size = 1;
                    innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
                    intensity = 1;
                    useFlare =0;
                    dayLight = 1;
                    flareSize = 1;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 30;
                        hardLimitEnd = 60;
                   
                    };

                };

                class Right: Left
			    {

                    position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";

			    };
				
				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };

				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Bluelight
				{

					color[]={100, 100, 1000};
					ambient[]={10,10,10};
					position="pos_blaulicht";
					direction="dir_blaulicht";
					hitpoint="";
					selection="pos_blaulicht";
					size=2;
					innerAngle=0;
					outerAngle=110;
					coneFadeCoef=2;
					intensity=1000;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Bluelight_Flare
				{

					color[]={100, 100, 1000};
					ambient[]={0,0,0};
					position="pos_blaulicht_flare";
					direction="dir_blaulicht_flare";
					hitpoint="";
					selection="pos_blaulicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=1000;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};
				
				class Orangelight
				{

					color[]={236, 99, 17};
					ambient[]={10,10,10};
					position="pos_orangelicht";
					direction="dir_orangelicht";
					hitpoint="";
					selection="pos_orangelicht";
					size=2;
					innerAngle=0;
					outerAngle=110;
					coneFadeCoef=2;
					intensity=250;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Orangelight_Flare
				{

					color[]={236, 99, 17};
					ambient[]={0,0,0};
					position="pos_orangelicht_flare";
					direction="dir_orangelicht_flare";
					hitpoint="";
					selection="pos_orangelicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=250;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};
				
			};

			class HitPoints: HitPoints//ToDo
			{

				class HitEngine: HitEngine
                {

                    armor = 0.5;
                    material = -1;
                    name = "engine";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 0.2;
                    radius = 0.16;
					armorComponent="";

                };

				class HitFuel: HitFuel
                {

                    armor = 0.5;
                    material = -1;
                    name = "fuel";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 0.5;
                    radius = 0.17;
					armorComponent="";

                };

				class HitHull: HitHull
                {

                    armor = 2.0;
                    material = -1;
                    name = "karoserie";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 1.5;
                    radius = 0.22;
					armorComponent="";

                };

				class HitLFWheel: HitLFWheel
                {

                    armor = 1;
					material = -1;
                    name = "wheel_1_1_steering";
					visual = "damage_LF_visual";
                    minimalHit = 0.02;
                    explosionShielding = 4;
                    radius = 0.07;

                };

				class HitLF2Wheel: HitLF2Wheel
                {

                    armor = 1;
					material = -1;
                    name = "wheel_1_2_steering";
					visual = "damage_LF2_visual";
                    minimalHit = 0.02;
                    explosionShielding = 4;
                    radius = 0.07;

                };

				class HitRFWheel: HitRFWheel
                {

                    armor = 1;
					material = -1;
                    name = "wheel_2_1_steering";
					visual = "damage_RF_visual";
                    minimalHit = 0.02;
                    explosionShielding = 4;
                    radius = 0.07;

                };

				class HitRF2Wheel: HitRF2Wheel
                {

                    armor = 1;
					material = -1;
                    name = "wheel_2_2_steering";
					visual = "damage_RF2_visual";
                    minimalHit = 0.02;
                    explosionShielding = 4;
                    radius = 0.07;

                };

				class HitGlass1
				{
					name = "glass1";
					visual = "glassF";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;
					material = -1;
					minimalHit = 0.01;
					armorComponent="";

				};

                class HitGlass2
				{
					name = "glass2";
					visual = "glassFS";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;
					material = -1;
					minimalHit = 0.01;
					armorComponent="";
					
				};

                class HitGlass3
				{

					name = "glass3";
					visual = "glassBS";
					explosionShielding = 1;
					armor = 0.75;
					passThrough = 0;
					radius = 0.05;
					material = -1;
					minimalHit = 0.01;
					armorComponent="";

				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};
				
			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};
			/*
            class MFD 
		    {

			    class ClockHUD
			    {

			        #include "cfgHUD.hpp"

			    };
		    };
            */

			class Turrets{};
            
            class AnimationSources
		    {

				class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Glas_links_open_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 3.5;
					
				};

				class Glas_rechts_open_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 3.5;
					
				};

				class Plane_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Trage_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Koffer_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Waffenhalter_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Kiste_Taschen_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Tropf_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Ruecksitz_Links_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Ruecksitz_Rechts_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Beifahrersitz_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Beifahrersitz_San_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Antenne_hide_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Moersertraeger_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class moerser_rohr_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class moerser_dreibein_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class moerser_bodenplatte_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class munkiste_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				//Rundumleuchte Hide
				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class Beacons
				{

					source = "user";
					animPeriod = 1;
					initPhase = 0;

				};
				class turn_left
            	{

					source = "user";
					animPeriod = 0.001;
					initPhase = 0;

           		};

            	class blaulicht_an: turn_left{};

				class orangelicht_an: turn_left{};

				class blaulicht_an_flare: turn_left{};

				class orangelicht_an_flare: turn_left{};

				class door_left_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;


                };

                class door_right_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

                class door_rear_Source
                {

                    source = user;
                	initPhase = 0;
                    animPeriod = 2;

                };

				class door_rear_plane_hide_source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

				class door_rear_plane_hide_open_source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class HitLFWheel
				{

					source = "Hit";
					hitpoint = "HitLFWheel";
					raw = 1;

				};
				class HitLF2Wheel: HitLFWheel{hitpoint = "HitLF2Wheel";};
				class HitRFWheel: HitLFWheel{hitpoint = "HitRFWheel";};
				class HitRF2Wheel: HitLFWheel{hitpoint = "HitRF2Wheel";};

				class HitGlass1
				{

					source = "Hit";
					hitpoint = "HitGlass1";

				};
				class HitGlass2: HitGlass1 {hitpoint = "HitGlass2";};
				class HitGlass3: HitGlass1 {hitpoint = "HitGlass3";};

				class tarnnetz_fzg_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class tarnnetz_fzg_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class tarnnetz_fzg_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class tarnnetz_boden_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class tarnnetz_boden_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class tarnnetz_boden_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class san_kreuz_hide_source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class FJg_schild_hide_source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

		    };

            class UserActions
		    {
				

				class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class Glas_links_open
                {

                    displayName = "$STR_Fenster_oeffnen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "player == (driver this) and (alive this) and (this animationSourcePhase 'Glas_links_open_Source') == 0";
                    statement = "this animateSource ['Glas_links_open_Source',1]";
                    
                };

				class Glas_rechts_open
                {

                    displayName = "$STR_Fenster_oeffnen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "(player in this) and ((this getCargoIndex player) == 0) and (alive this) and (this animationSourcePhase 'Glas_rechts_open_Source') == 0";
                    statement = "this animateSource ['Glas_rechts_open_Source',1]";
                    
                };

				class Glas_links_close
                {

                    displayName = "$STR_Fenster_schliessen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "player == (driver this) and (alive this) and (this animationSourcePhase 'Glas_links_open_Source') == 1";
                    statement = "this animateSource ['Glas_links_open_Source',0]";
                    
                };

				class Glas_rechts_close
                {

                    displayName = "$STR_Fenster_schliessen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "(player in this) and ((this getCargoIndex player) == 0) and (alive this) and (this animationSourcePhase 'Glas_rechts_open_Source') == 1";
                    statement = "this animateSource ['Glas_rechts_open_Source',0]";
                    
                };

				class Plane_hide
                {

                    displayName = "$STR_Plane_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'Plane_hide_Source' == 0) and (alive this)";
					statement = "this animateSource ['Plane_hide_Source', 1];this setVariable ['cover_on', false,true];if (this getVariable ['rear_door_open', false]) then {this animateSource ['door_rear_plane_hide_open_source', 1];} else {this animateSource ['door_rear_plane_hide_source', 1];};";
					
                };

				class Plane_unhide
                {

                    displayName = "$STR_Plane_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'Plane_hide_Source' == 1) and (alive this)";
					statement = "this animateSource ['Plane_hide_Source', 0];this setVariable ['cover_on', true,true];if (this getVariable ['rear_door_open', false]) then {this animateSource ['door_rear_plane_hide_open_source', 0];} else {this animateSource ['door_rear_plane_hide_source', 0];};";
					
                };

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_montieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 1) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animateSource ['Hide_Rundumleuchte_fuss', 0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abmontieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animateSource ['Hide_Rundumleuchte_fuss', 1];";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class Orangelicht_an_2
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and ((this getCargoIndex player) == 0) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and ((this getCargoIndex player) == 0) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class Driver_Door_Open
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'door_left_Source' == 0) and (alive this)";
					statement = "this animateSource ['door_left_Source', 1];";

                };

                class Driver_Door_Close
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'door_left_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['door_left_Source', 0];";

                };

				class Driver_Door_Open_ext
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "door_open_links";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door_left_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['door_left_Source', 1];";

                };

                class Driver_Door_Close_ext
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "door_open_links";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door_left_Source' > 0) and (alive this)";
					statement = "this animateSource ['door_left_Source', 0];";

                };

				class Co_Driver_Door_Open
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and ((this getCargoIndex player) == 0) and (this animationSourcePhase 'door_right_Source' == 0) and  (alive this)";
					statement = "this animateSource ['door_right_Source', 1];";

                };

                class Co_Driver_Door_Close
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and ((this getCargoIndex player) == 0) and (this animationSourcePhase 'door_right_Source' > 0)  and (alive this)"; 
					statement = "this animateSource ['door_right_Source', 0];";

                };

                class Co_Driver_Door_Open_ext
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "door_open_rechts";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door_right_Source' == 0) and (alive this)";
					statement = "this animateSource ['door_right_Source', 1];";

                };

                class Co_Driver_Door_Close_ext
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "door_open_rechts";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door_right_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['door_right_Source', 0];";

                };

				class Rear_Door_Open_1
                {

                    displayName = "$STR_HeckTuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 0) and !(player in [driver this]) and !((this getCargoIndex player) == 0) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 1];this setVariable ['rear_door_open', true,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_source', 1];this animateSource ['door_rear_plane_hide_open_source', 0];};";
					
                };
				
                class Rear_Door_Close_1
                {

                    displayName = "$STR_HeckTuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 1) and !(player in [driver this]) and !((this getCargoIndex player) == 0) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 0];this setVariable ['rear_door_open', false,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_open_source', 1];this animateSource ['door_rear_plane_hide_source', 0];};";
					
                };

				class Rear_Door_Open_ext
                {

                    displayName = "$STR_HeckTuere_oeffnen";
					position = "door_open_hinten";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door_rear_Source' == 0) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 1];this setVariable ['rear_door_open', true,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_source', 1];this animateSource ['door_rear_plane_hide_open_source', 0];};";
					
			    };

                class Rear_Door_Close_ext
                {

                    displayName = "$STR_HeckTuere_schliessen";
					position = "door_open_hinten";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this animationSourcePhase 'door_rear_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['door_rear_Source', 0];this setVariable ['rear_door_open', false,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_open_source', 1];this animateSource ['door_rear_plane_hide_source', 0];};";
					
                };

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_wolf_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_wolf_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_wolf_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,3] call Redd_fnc_wolf_flags";

				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_wolf_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_wolf_camonet";

				};

				class Tarnnetz_Boden_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_wolf_camonet";

				};

				class Tarnnetz_Boden_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and (this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_wolf_camonet";

				};
				
		    };
			
            class EventHandlers: EventHandlers 
            {

               init = "_this call redd_fnc_wolf_init";
			   getIn = "_this call redd_fnc_wolf_getIn";
			   getOut = "_this call redd_fnc_wolf_getOut";

            };

        };

        //Wolf FüFu
        class Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FueFu: Redd_Tank_LKW_leicht_gl_Wolf_Base
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_fuefu_f.paa";
			scope=2;
            scopeCurator=2;
			displayName="$STR_LKW_leicht_gl_Wolf_Flecktarn_FueFu";

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Flecktarn_FueFu";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Tropentarn_FueFu";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Wintertarn_FueFu";
					author = "ReddNTank";
					
					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

            class AnimationSources: AnimationSources
		    {

                class Waffenhalter_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Ruecksitz_Links_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Ruecksitz_Rechts_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Beifahrersitz_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Antenne_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

            };

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_FueFu: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FueFu
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_fuefu_d.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Tropentarn_FueFu";
			scopeArsenal = 0;

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"

            };

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_FueFu: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FueFu
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_fuefu_w.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Wintertarn_FueFu";
			scopeArsenal = 0;
			/*
			class EventHandlers: EventHandlers 
            {

               init = "_this call redd_fnc_wolf_w_init";

            };
			*/
            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

            };

		};

		//Wolf Moerser
        class Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_Moerser: Redd_Tank_LKW_leicht_gl_Wolf_Base
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_moerser_f.paa";
			scope=2;
            scopeCurator=2;
			displayName="$STR_LKW_leicht_gl_Wolf_Flecktarn_Moerser";
			
			transportSoldier = 1;
			cargoAction[] = {"Redd_Tank_Wolf_CoDriver"};
            cargoProxyIndexes[] = {1};
			hideWeaponsDriver = 1;
			hideWeaponsCargo = 1;

			class TransportBackpacks: TransportBackpacks
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

				class _xx_m120_barrel
				{
					
					backpack = "Redd_Tank_M120_Tampella_Barrel";
					count = 1;
					
				};

				class _xx_m120_tripod
				{
					
					backpack = "Redd_Tank_M120_Tampella_Tripod";
					count = 1;
					
				};
				
			};

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Flecktarn_Moerser";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Tropentarn_Moerser";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Wintertarn_Moerser";
					author = "ReddNTank";
					
					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

            class AnimationSources: AnimationSources
		    {

				class Beifahrersitz_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Moersertraeger_hide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

				class moerser_rohr_hide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

				class moerser_dreibein_hide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

				class moerser_bodenplatte_hide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

				class munkiste_hide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

            };

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_Moerser: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_Moerser
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_moerser_d.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Tropentarn_Moerser";
			scopeArsenal = 0;

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"

            };

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_Moerser: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_Moerser
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_moerser_w.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Wintertarn_Moerser";
			scopeArsenal = 0;
			/*
			class EventHandlers: EventHandlers 
            {

               init = "_this call redd_fnc_wolf_w_init";

            };
			*/
            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

            };

		};

		//Wolf San
        class Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_San: Redd_Tank_LKW_leicht_gl_Wolf_Base
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_san_f.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_LKW_leicht_gl_Wolf_Flecktarn_San";

			transportSoldier = 2;
            cargoAction[] = {"Redd_Tank_Wolf_Passenger1","Redd_Tank_Wolf_Passenger_San"};
			cargoProxyIndexes[] = {5,6};
			hideWeaponsDriver = 1;
			hideWeaponsCargo = 1;

			TFAR_AdditionalLR_Cargo[] = {};

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

            };

			class Turrets: Turrets 
			{

				class San_Turret: NewTurret
				{

					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					enabledByAnimationSource = "";
					proxyType= "CPGunner";
					proxyIndex = 1;
					gunnerName = "$STR_Sani_Platz";
					primaryGunner = 0;
					gunnerGetInAction="GetInLow";
					gunnerGetOutAction="GetOutLow";
					hideWeaponsGunner = 1;
					inGunnerMayFire=0;
					viewGunnerInExternal = 1;
					gunnerCompartments= "Compartment1";
					lodTurnedIn = 1200; //Cargo
					startEngine = 0;
					dontCreateAi = 1;
					soundAttenuationTurret = "TankAttenuation";
					gunnerAction = "Redd_Tank_Wolf_Passenger1";
					memoryPointsGetInGunner= "pos cargo";
					memoryPointsGetInGunnerDir= "pos cargo dir";
					isPersonTurret = 0;
					forceHideGunner = 1;

					class ViewGunner: ViewOptics 
					{

						minFov = 0.75;
						maxFov = 0.75;
						initFov = 0.75;
						
					};

					class TurnIn 
					{

						limitsArrayTop[] = 
						{

							{45, -120}, 
							{45, 120}
							
						};

						limitsArrayBottom[] = 
						{

							{-45, -120}, 
							{-45, 120}
							
						};

					};
					
					class TurnOut 
					{

						limitsArrayTop[] = 
						{

							{45, -78},
							{45, 120}
							
						};

						limitsArrayBottom[] = 
						{
							{-5, -78},
							{-20, 0},
							{-20, 47},
							{7, 57},
							{7, 100},
							{0, 110},
							{-5, 115},
							{-5, 120}
							
						};
					
					};

				};
				
			};

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Flecktarn_San";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Tropentarn_San";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Wintertarn_San";
					author = "ReddNTank";
					
					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

            class AnimationSources: AnimationSources
		    {

				class Trage_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Koffer_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Kiste_Taschen_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Tropf_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Ruecksitz_Links_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Beifahrersitz_San_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class san_kreuz_hide_source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

            };

			class userActions: userActions
			{

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_montieren";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abmontieren";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Orangelicht_an_2
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				class Orangelicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Blaulicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'blaulicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart',1];this animate ['blaulicht_an',1];this animate ['blaulicht_an_flare',1]";
					
				};
				class Blaulicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'blaulicht_an' > 0.5) and (alive this)";
					statement = "this animate ['blaulicht_an',0];this animate ['blaulicht_an_flare',0];this animate ['BeaconsStart',0]";

				};
				
				class Blaulicht_an_2
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				class Blaulicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				
				class Glas_rechts_open
                {

                    displayName = "$STR_Fenster_oeffnen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "false";
					statement = "this animateSource ['Glas_rechts_open_Source',1]";
                    
                };

				class Glas_rechts_close
                {

                    displayName = "$STR_Fenster_schliessen";
                    position = "actionPoint";
                    radius = 25;
                    onlyforplayer = 1;
                    showWindow = 0;
                    condition = "false";
					statement = "this animateSource ['Glas_rechts_open_Source',0]";
                    
                };

				class Co_Driver_Door_Open
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "this animateSource ['door_right_Source', 1];";

                };

                class Co_Driver_Door_Close
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "this animateSource ['door_right_Source', 0];";

                };

				class Rear_Door_Open_1
                {

                    displayName = "$STR_HeckTuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 0) and !(player in [driver this]) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 1];this setVariable ['rear_door_open', true,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_source', 1];this animateSource ['door_rear_plane_hide_open_source', 0];};";
					
			    };
				
                class Rear_Door_Close_1
                {

                    displayName = "$STR_HeckTuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 1) and !(player in [driver this]) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 0];this setVariable ['rear_door_open', false,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_open_source', 1];this animateSource ['door_rear_plane_hide_source', 0];};";
					
                };

			};

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_San: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_San
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_san_d.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Tropentarn_San";
			scopeArsenal = 0;

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"

            };

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_San: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_San
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_san_w.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Wintertarn_San";
			scopeArsenal = 0;

			/*
			class EventHandlers: EventHandlers 
            {

               init = "_this call redd_fnc_wolf_w_init";

            };
			*/

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

            };

		};

		//Wolf Feldjäger
		class Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FJg: Redd_Tank_LKW_leicht_gl_Wolf_Base
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_fjg_f.paa";
			scope=2;
            scopeCurator=2;
			displayName="$STR_LKW_leicht_gl_Wolf_Flecktarn_FJg";

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Flecktarn_FJg";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Tropentarn_FJg";
					author = "ReddNTank";

					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_LKW_leicht_gl_Wolf_Wintertarn_FJg";
					author = "ReddNTank";
					
					textures[]=
					{

						/*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
						/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
						/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
						/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
						/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
						/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
						/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
						/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
						/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
						/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

            class AnimationSources: AnimationSources
		    {

                class Waffenhalter_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Ruecksitz_Links_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Ruecksitz_Rechts_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Beifahrersitz_hide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class FJg_schild_hide_source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

            };

			class userActions: userActions
			{

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_montieren";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abmontieren";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Orangelicht_an_2
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				class Orangelicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Blaulicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'blaulicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart',1];this animate ['blaulicht_an',1];this animate ['blaulicht_an_flare',1]";
					
				};
				class Blaulicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'blaulicht_an' > 0.5) and (alive this)";
					statement = "this animate ['blaulicht_an',0];this animate ['blaulicht_an_flare',0];this animate ['BeaconsStart',0]";

				};
				
				class Blaulicht_an_2
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};
				class Blaulicht_aus_2
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "false";
					statement = "";

				};

				class Rear_Door_Open_1
                {

                    displayName = "$STR_HeckTuere_oeffnen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 0) and !(player in [driver this]) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 1];this setVariable ['rear_door_open', true,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_source', 1];this animateSource ['door_rear_plane_hide_open_source', 0];};";
					
			    };
				
                class Rear_Door_Close_1
                {

                    displayName = "$STR_HeckTuere_schliessen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 1) and !(player in [driver this]) and (alive this)";
					statement = "this animateSource ['door_rear_Source', 0];this setVariable ['rear_door_open', false,true];if (this getVariable ['cover_on', true]) then {this animateSource ['door_rear_plane_hide_open_source', 1];this animateSource ['door_rear_plane_hide_source', 0];};";
					
                };

			};

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_FJg: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FJg
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_fjg_d.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Tropentarn_FJg";
			scopeArsenal = 0;

            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_wueste_innent_detail_sp_blend_co.paa"

            };

		};

		class Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_FJg: Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FJg
		{
			
			editorPreview="\redd_tank_lkw_leicht_gl_wolf\pictures\wolf_pre_picture_fjg_w.paa";
			displayName="$STR_LKW_leicht_gl_Wolf_Wintertarn_FJg";
			scopeArsenal = 0;
			
			/*
			class EventHandlers: EventHandlers 
            {

               init = "_this call redd_fnc_wolf_w_init";

            };
			*/
			
            hiddenSelectionsTextures[] = 
            {

                /*"Reifen_Fahrg_sp",*/  "\redd_tank_lkw_leicht_gl_wolf\data\rnt_Wolf_GL_Winter_Reifen_Fahrg_sp_blend_co.paa",
				/*"Verdeck_u_kleint_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Verdeck_u_kleint_sp_blend_co.paa",
				/*"Moerser_Rolle_Teile_Heck_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Moerser_Rolle_Teile_Heck_sp_blend_co.paa",
				/*"Body_sp",*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Body_sp_blend_co.paa",
				/*"Motorhaube_Tueren_Unterb_sp"*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_Motorhaube_Tueren_Unterb_sp_blend_co.paa",
				/*Konsolen_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_Konsolen_sp_blend_co.paa",
				/*San_u_Rahmenteile_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_San_u_Rahmenteile_sp_blend_co.paa",
				/*aussen_anbaut_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_aussen_anbaut_sp_blend_co.paa",
				/*ladefl_sitz_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_ladefl_sitz_sp_blend_co.paa",
				/*innent_detail_sp*/ "\redd_tank_lkw_leicht_gl_wolf\data\redd_tank_wolf_gl_winter_innent_detail_sp_blend_co.paa"

            };

		};
		
    };