

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Init for M120
	//			 
	//	Example:
	//						 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params["_veh"];

	[_veh] spawn redd_fnc_wolf_plate;//spawns function to randomise license plates

	_veh setVariable ['has_flag', false,true];//initiates varibale for flags
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet
	_veh setVariable ['has_camonet_large', false,true];//initiates varibale for large camonet

	_veh setObjectMaterialGlobal [0, "redd_tank_lkw_leicht_gl_wolf\mats\redd_tank_wolf_w_Reifen_Fahrg_sp.rvmat"];

	true