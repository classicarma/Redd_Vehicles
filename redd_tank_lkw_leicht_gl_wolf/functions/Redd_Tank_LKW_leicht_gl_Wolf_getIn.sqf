

	//Triggerd by BI eventhandler "getIn"
	
	params ["_vehicle", "_role", "_unit", "_turret"];

	if (_vehicle getVariable 'has_camonet_large') then
	{

		_unit setUnitTrait ["camouflageCoef", 0.1];

	};

	if (_vehicle getVariable 'has_camonet') then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
		_camouflage_new = _camouflage/100*80;
		_unit setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

	};



