class CfgPatches {
    class rnt_ace_compatibility {
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {
            "Redd_Vehicles_Main",
            "rnt_t_base",
            "rnt_lkw_5t_mil_gl_kat_i",
            "rnt_lkw_7t_mil_gl_kat_i",
            "rnt_lkw_10t_mil_gl_kat_i",
            "rnt_mg3_static",
            "rnt_gmw_static"
            //"rnt_m113_pzm"
        };
    };
};

class DefaultEventHandlers;
class WeaponFireGun;
class WeaponCloudsGun;
class WeaponFireMGun;
class WeaponCloudsMGun;

class CfgVehicles {
    
    class LandVehicle;
	class Car: LandVehicle{};
	class Car_F: Car {};
	class Truck_F: Car_F {};

    //5t
    class rnt_lkw_5t_mil_gl_kat_i_base: Truck_F {};
    class rnt_lkw_5t_mil_gl_kat_i_fuel_fleck: rnt_lkw_5t_mil_gl_kat_i_base {
        transportFuel=0;
        ace_refuel_fuelCargo = 4600;
        ace_refuel_hooks[] = {{-0.8,-4.2,-0.35}};
    };
    class rnt_lkw_5t_mil_gl_kat_i_fuel_trope: rnt_lkw_5t_mil_gl_kat_i_fuel_fleck {};
    class rnt_lkw_5t_mil_gl_kat_i_fuel_winter: rnt_lkw_5t_mil_gl_kat_i_fuel_fleck {};

    //7t
    class rnt_lkw_7t_mil_gl_kat_i_base: Truck_F {};
    class rnt_lkw_7t_mil_gl_kat_i_mun_fleck: rnt_lkw_7t_mil_gl_kat_i_base {
        transportAmmo=0;
        ace_rearm_defaultSupply = 1200;
    };
    class rnt_lkw_7t_mil_gl_kat_i_mun_trope: rnt_lkw_7t_mil_gl_kat_i_mun_fleck {};
    class rnt_lkw_7t_mil_gl_kat_i_mun_winter: rnt_lkw_7t_mil_gl_kat_i_mun_fleck {};

    //10t
    class rnt_lkw_10t_mil_gl_kat_i_base: Truck_F {};
    class rnt_lkw_10t_mil_gl_kat_i_repair_fleck: rnt_lkw_10t_mil_gl_kat_i_base {
        transportRepair=0;
        ace_repair_canRepair = 1;
    };
    class rnt_lkw_10t_mil_gl_kat_i_repair_trope: rnt_lkw_10t_mil_gl_kat_i_repair_fleck {};
    class rnt_lkw_10t_mil_gl_kat_i_repair_winter: rnt_lkw_10t_mil_gl_kat_i_repair_fleck {};

    //MG3
    class StaticWeapon: LandVehicle {
        class Turrets;
		class MainTurret;
        class ACE_Actions {
            class ACE_MainActions;
        };
        class ACE_SelfActions;
    };

	class StaticMGWeapon: StaticWeapon {
    };

    class HMG_01_base_F: StaticMGWeapon{};
    class HMG_01_high_base_F: HMG_01_base_F{};

	class rnt_mg3_static: HMG_01_high_base_F {

        class UserActions {
            delete MG3_up_ext;
            delete MG3_down_ext;
            delete MG3_left_ext;
            delete MG3_right_ext;
            delete MG3_up_int;
            delete MG3_down_int;
            delete MG3_left_int;
            delete MG3_right_int;
            delete MG3_load;
        };

        class ACE_Actions: ACE_Actions {

            class MG3_load
            {
                displayName = "$STR_ACE_mg3_laden";
                distance = 2;
                condition = "('rnt_mg3_kasten_fake' in backpackItems player)";
				statement = "[this, player, 'rnt_mg3_kasten_fake','Redd_Mg3_Belt_100'] call redd_fnc_mg3_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
                selection = "machinegun_eject_pos";
            };

            class ACE_MainActions: ACE_MainActions {
                selection = "fuss_vorne_up_axis";

                class MG_hoehe_justieren {
                    displayName="$STR_ACE_MG_hoehe_justieren";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
					distance=2;
					condition="";
					statement="";

                    class MG3_up_ext {
                        displayName = "$STR_ACE_mg3_hohe_justieren_hoeher";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
                        statement = "[this,'up'] spawn redd_fnc_mg3_adjust_height";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };

                    class MG3_down_ext {
                        displayName = "$STR_ACE_mg3_hohe_justieren_tiefer";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
                        statement = "[this,'down'] spawn redd_fnc_mg3_adjust_height";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };
                };

                class MG_hoehe_drehen {
                    displayName="$STR_ACE_MG_drehen";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
					distance=2;
					condition="";
					statement="";

                    class MG3_left_ext {
                        displayName = "$STR_ACE_mg3_drehen_links";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				        statement = "[this,'left'] spawn redd_fnc_mg3_turn";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };

                    class MG3_right_ext {
                        displayName = "$STR_ACE_mg3_drehen_rechts";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				        statement = "[this,'right'] spawn redd_fnc_mg3_turn";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };
                };
            };
        };

        class ACE_SelfActions: ACE_SelfActions {

            class MG3_load
            {
                displayName = "$STR_ACE_mg3_laden";
                distance = 2;
                condition = "('rnt_mg3_kasten_fake' in backpackItems player)";
				statement = "[this, player, 'rnt_mg3_kasten_fake','Redd_Mg3_Belt_100'] call redd_fnc_mg3_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
            };

            class MG_hoehe_justieren {
                displayName="$STR_ACE_MG_hoehe_justieren";
                icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                distance=2;
                condition="";
                statement="";

                class MG3_up_int {
                    displayName = "$STR_ACE_mg3_hohe_justieren_hoeher";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
				    statement = "[this,'up'] spawn redd_fnc_mg3_adjust_height";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };

                class MG3_down_int {
                    displayName = "$STR_ACE_mg3_hohe_justieren_tiefer";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
				    statement = "[this,'down'] spawn redd_fnc_mg3_adjust_height";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };
            };

            class MG_hoehe_drehen {
                displayName="$STR_ACE_MG_drehen";
                icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                distance=2;
                condition="";
                statement="";

                class MG3_left_int {
                    displayName = "$STR_ACE_mg3_drehen_links";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				    statement = "[this,'left'] spawn redd_fnc_mg3_turn";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };

                class MG3_right_int {
                    displayName = "$STR_ACE_mg3_drehen_rechts";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				    statement = "[this,'right'] spawn redd_fnc_mg3_turn";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };
            };
        };
    };

    class rnt_mg3_static_ai: rnt_mg3_static {

        class UserActions {
            delete MG3_load;
        };

        class ACE_Actions: ACE_Actions {

            class MG3_load
            {
                displayName = "$STR_ACE_mg3_laden";
                distance = 2;
                condition = "false";
                statement = "[this, player, 'Redd_Mg3_Belt_100_fake','Redd_Mg3_Belt_100'] call redd_fnc_mg3_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
                selection = "machinegun_eject_pos";
            };
        };

        class ACE_SelfActions: ACE_SelfActions {

            class MG3_load
            {
                displayName = "$STR_ACE_mg3_laden";
                distance = 2;
                condition = "false";
				statement = "[this, player, 'Redd_Mg3_Belt_100_fake','Redd_Mg3_Belt_100'] call redd_fnc_mg3_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
            };
        };
    };

    //GMW
	class StaticGrenadeLauncher: StaticWeapon{};
    class GMG_TriPod: StaticGrenadeLauncher{};
    class GMG_01_base_F: GMG_TriPod{};

	class rnt_gmw_static: GMG_01_base_F {

        class UserActions {
            delete gmw_up_ext;
            delete gmw_down_ext;
            delete gmw_left_ext;
            delete gmw_right_ext;
            delete gmw_up_int;
            delete gmw_down_int;
            delete gmw_left_int;
            delete gmw_right_int;
            delete gmw_load;
        };

        class ACE_Actions: ACE_Actions {

            class gmw_load
            {
                displayName = "$STR_ACE_gmw_laden";
                distance = 2;
                condition = "('rnt_gmw_kasten_fake' in backpackItems player)";
				statement = "[this, player, 'rnt_gmw_kasten_fake','Redd_gmw_Belt_32'] call redd_fnc_gmw_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
                selection = "gmw_eject_pos";
            };

            class ACE_MainActions: ACE_MainActions {
                selection = "fuss_vorne_up_axis";

                class MG_hoehe_justieren {
                    displayName="$STR_ACE_gmw_hoehe_justieren";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
					distance=2;
					condition="";
					statement="";

                    class gmw_up_ext {
                        displayName = "$STR_ACE_gmw_hohe_justieren_hoeher";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
                        statement = "[this,'up'] spawn redd_fnc_gmw_adjust_height";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };

                    class gmw_down_ext {
                        displayName = "$STR_ACE_gmw_hohe_justieren_tiefer";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
                        statement = "[this,'down'] spawn redd_fnc_gmw_adjust_height";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };
                };

                class MG_hoehe_drehen {
                    displayName="$STR_ACE_gmw_drehen";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
					distance=2;
					condition="";
					statement="";

                    class gmw_left_ext {
                        displayName = "$STR_ACE_gmw_drehen_links";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				        statement = "[this,'left'] spawn redd_fnc_gmw_turn";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };

                    class gmw_right_ext {
                        displayName = "$STR_ACE_gmw_drehen_rechts";
                        condition = "!(player in this) and (this turretUnit [0] isEqualTo objNull) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				        statement = "[this,'right'] spawn redd_fnc_gmw_turn";
                        icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                    };
                };
            };
        };

        class ACE_SelfActions: ACE_SelfActions {

            class gmw_load
            {
                displayName = "$STR_ACE_gmw_laden";
                distance = 2;
                condition = "('rnt_gmw_kasten_fake' in backpackItems player)";
				statement = "[this, player, 'rnt_gmw_kasten_fake','Redd_gmw_Belt_32'] call redd_fnc_gmw_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
            };

            class MG_hoehe_justieren {
                displayName="$STR_ACE_gmw_hoehe_justieren";
                icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                distance=2;
                condition="";
                statement="";

                class gmw_up_int {
                    displayName = "$STR_ACE_gmw_hohe_justieren_hoeher";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""up"",false]) and (alive this)";
				    statement = "[this,'up'] spawn redd_fnc_gmw_adjust_height";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };

                class gmw_down_int {
                    displayName = "$STR_ACE_gmw_hohe_justieren_tiefer";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and !(this getVariable [""down"",false]) and (alive this)";
				    statement = "[this,'down'] spawn redd_fnc_gmw_adjust_height";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };
            };

            class MG_hoehe_drehen {
                displayName="$STR_ACE_MG_drehen";
                icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                distance=2;
                condition="";
                statement="";

                class gmw_left_int {
                    displayName = "$STR_ACE_gmw_drehen_links";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				    statement = "[this,'left'] spawn redd_fnc_gmw_turn";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };

                class gmw_right_int {
                    displayName = "$STR_ACE_gmw_drehen_rechts";
                    condition = "(player in this) and !(this getVariable [""isInAnimation"",false]) and (alive this)";
				    statement = "[this,'right'] spawn redd_fnc_gmw_turn";
                    icon = "\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
                };
            };
        };
    };

    class rnt_gmw_static_ai: rnt_gmw_static {

        class UserActions {
            delete gmw_load;
        };

        class ACE_Actions: ACE_Actions {

            class gmw_load
            {
                displayName = "$STR_ACE_gmw_laden";
                distance = 2;
                condition = "false";
                statement = "[this, player, 'Redd_gmw_Belt_32_fake','Redd_gmw_Belt_32'] call redd_fnc_gmw_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
                selection = "gmw_eject_pos";
            };
        };

        class ACE_SelfActions: ACE_SelfActions {

            class gmw_load
            {
                displayName = "$STR_ACE_gmw_laden";
                distance = 2;
                condition = "false";
				statement = "[this, player, 'Redd_gmw_Belt_32_fake','Redd_gmw_Belt_32'] call redd_fnc_gmw_reload";
                icon = "\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa";
            };
        };
    };

    // M113pzm
    // class LandVehicle;
    // class Tank: LandVehicle {};

    // class Tank_F: Tank {

    //     class ACE_SelfActions;
    // };

    // class rnt_m113_pzm_base: Tank_F {

    //     class UserActions {
    //         delete Manual_reload_HE;
    //         delete Manual_reload_ANNZ;
    //         delete Manual_reload_Flare;
    //         delete Manual_reload_Smoke;
    //     };

    //     class ACE_SelfActions: ACE_SelfActions {
            
    //         class M113_pzm_reload {
    //             displayName="STR_Reload_m113";
    //             icon = "\z\ace\addons\rearm\ui\icon_rearm_interact.paa";
    //             distance=2;
    //             condition="[this,player,'Redd_1Rnd_120mm_Mo_shells'] call redd_fnc_m113CheckManReload || [this,player,'Redd_1Rnd_120mm_Mo_annz_shells'] call redd_fnc_m113CheckManReload || [this,player,'Redd_1Rnd_120mm_Mo_Flare_white'] call redd_fnc_m113CheckManReload || [this,player,'Redd_1Rnd_120mm_Mo_Smoke_white'] call redd_fnc_m113CheckManReload";
    //             statement="";
                
    //             class Manual_reload_HE {
    //                 displayName = "STR_ManReloadHE";
    //                 condition = "[this,player,'Redd_1Rnd_120mm_Mo_shells'] call redd_fnc_m113CheckManReload";
    //                 statement = "[this,player,'Redd_1Rnd_120mm_Mo_shells'] call redd_fnc_m113ManReload";
    //                 icon = "";
    //             };

    //             class Manual_reload_ANNZ {
    //                 displayName = "STR_ManReloadANNZ";
    //                 condition = "[this,player,'Redd_1Rnd_120mm_Mo_annz_shells'] call redd_fnc_m113CheckManReload";
    //                 statement = "[this,player,'Redd_1Rnd_120mm_Mo_annz_shells'] call redd_fnc_m113ManReload";
    //                 icon = "";
    //             };

    //             class Manual_reload_Flare {
    //                 displayName = "STR_ManReloadFLARE";
    //                 condition = "[this,player,'Redd_1Rnd_120mm_Mo_Flare_white'] call redd_fnc_m113CheckManReload";
    //                 statement = "[this,player,'Redd_1Rnd_120mm_Mo_Flare_white'] call redd_fnc_m113ManReload";
    //                 icon = "";
    //             };

    //             class Manual_reload_Smoke {
    //                 displayName = "STR_ManReloadSMOKE";
    //                 condition = "[this,player,'Redd_1Rnd_120mm_Mo_Smoke_white'] call redd_fnc_m113CheckManReload";
    //                 statement = "[this,player,'Redd_1Rnd_120mm_Mo_Smoke_white'] call redd_fnc_m113ManReload";
    //                 icon = "";
    //             };
    //         };
    //     };
    // };
};