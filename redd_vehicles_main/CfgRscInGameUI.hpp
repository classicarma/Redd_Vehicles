

    class RscControlsGroup 
    {

        class VScrollbar;
        class HScrollbar;

    };

    class RscControlsGroupNoScrollbars: RscControlsGroup
    {

	    class VScrollbar: VScrollbar
        {
            width=0;
        };
        class HScrollbar: HScrollbar
        {
            height=0;
        };

    };

    class RscPicture;
    class RscText;

    class RscInGameUI
    {

        class RscUnitInfo;

        class Redd_RCS_Driver: RscUnitInfo
        {
            idd=300;
            controls[]=
            {
                "CA_Zeroing",
                "CA_IGUI_elements_group"
            };
            class CA_IGUI_elements_group: RscControlsGroup
            {
                idc=170;
                class VScrollbar: VScrollbar
                {
                    width=0;
                };
                class HScrollbar: HScrollbar
                {
                    height=0;
                };
                x="0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
                y="0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
                w="53.5 * 		(0.01875 * SafezoneH)";
                h="40 * 		(0.025 * SafezoneH)";
                class controls
                {
                    class CA_TurretIndicator: RscPicture
                    {
                        IDC=206;
                        type=105;
                        textSize="0.02*SafezoneH";
                        style=0;
                        color[]={0.94999999,0.94999999,0.94999999,1};
                        x="39 * 		(0.01875 * SafezoneH)";
                        y="33 * 		(0.025 * SafezoneH)";
                        w="6 * 		(0.01875 * SafezoneH)";
                        h="6 * 		(0.025 * SafezoneH)";
                        imageHull="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\turretIndicatorHull.paa";
                        imageTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\turretIndicatorTurret.paa";
                        imageObsTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\turretIndicatorObsTurret2.paa";
                        imageGun="#(rgb,8,8,3)color(0,0,0,0)";
                    };
                    class CA_HorizontalCompass: RscPicture
                    {
                        IDC=207;
                        type=105;
                        font="EtelkaMonospacePro";
                        textSize="0.028*SafezoneH";
                        style=1;
                        color[]={0.94999999,0.94999999,0.94999999,1};
                        x="9.04 * 		(0.01875 * SafezoneH)";
                        y="0 * 		(0.025 * SafezoneH)";
                        w="35.18 * 		(0.01875 * SafezoneH)";
                        h="2 * 		(0.025 * SafezoneH)";
                        imageHull="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassHull.paa";
                        imageTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassTurret.paa";
                        imageObsTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassObsTurret.paa";
                        imageGun="#(rgb,8,8,3)color(0,0,0,0)";
                    };
                    class CA_Heading: RscText
                    {
                        idc=156;
                        style=2;
                        sizeEx="0.032*SafezoneH";
                        shadow=0;
                        font="EtelkaMonospacePro";
                        colorText[]={0.94999999,0.94999999,0.94999999,1};
                        text="015";
                        x="25.15 * 		(0.01875 * SafezoneH)";
                        y="2.25 * 		(0.025 * SafezoneH)";
                        w="3 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                };
            };
        };

        class Redd_RCS_Driver_APC: RscUnitInfo
        {
            idd=300;
            controls[]=
            {
                "CA_Zeroing",
                "CA_IGUI_elements_group"
            };
            class CA_IGUI_elements_group: RscControlsGroup
            {
                idc=170;
                class VScrollbar: VScrollbar
                {
                    width=0;
                };
                class HScrollbar: HScrollbar
                {
                    height=0;
                };
                x="0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
                y="0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
                w="53.5 * 		(0.01875 * SafezoneH)";
                h="40 * 		(0.025 * SafezoneH)";
                class controls
                {
                    
                    class CA_HorizontalCompass: RscPicture
                    {
                        IDC=207;
                        type=105;
                        font="EtelkaMonospacePro";
                        textSize="0.028*SafezoneH";
                        style=1;
                        color[]={0.94999999,0.94999999,0.94999999,1};
                        x="9.04 * 		(0.01875 * SafezoneH)";
                        y="0 * 		(0.025 * SafezoneH)";
                        w="35.18 * 		(0.01875 * SafezoneH)";
                        h="2 * 		(0.025 * SafezoneH)";
                        imageHull="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassHull.paa";
                        imageTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassTurret.paa";
                        imageObsTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassObsTurret.paa";
                        imageGun="#(rgb,8,8,3)color(0,0,0,0)";
                    };
                    class CA_Heading: RscText
                    {
                        idc=156;
                        style=2;
                        sizeEx="0.032*SafezoneH";
                        shadow=0;
                        font="EtelkaMonospacePro";
                        colorText[]={0.94999999,0.94999999,0.94999999,1};
                        text="015";
                        x="25.15 * 		(0.01875 * SafezoneH)";
                        y="2.25 * 		(0.025 * SafezoneH)";
                        w="3 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                };
            };
        };

        class Redd_RCS_Turret: RscUnitInfo
        {
            idd=300;
            controls[]=
            {
                "CA_Zeroing",
                "CA_IGUI_elements_group"
            };
            class CA_IGUI_elements_group: RscControlsGroup
            {
                idc=170;
                class VScrollbar: VScrollbar
                {
                    width=0;
                };
                class HScrollbar: HScrollbar
                {
                    height=0;
                };
                x="0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
                y="0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
                w="53.5 * 		(0.01875 * SafezoneH)";
                h="40 * 		(0.025 * SafezoneH)";
                class controls
                {
                    class CA_TurretIndicator: RscPicture
                    {
                        IDC=206;
                        type=105;
                        textSize="0.02*SafezoneH";
                        style=0;
                        color[]={0.94999999,0.94999999,0.94999999,1};
                        x="42.25 * 		(0.01875 * SafezoneH)";
                        y="3.5 * 		(0.025 * SafezoneH)";
                        w="6 * 		(0.01875 * SafezoneH)";
                        h="6 * 		(0.025 * SafezoneH)";
                        imageHull="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\turretIndicatorHull.paa";
					    imageTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\turretIndicatorTurret.paa";
					    imageObsTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\turretIndicatorObsTurret2.paa";
					    imageGun="#(rgb,8,8,3)color(0,0,0,0)";
                    };
                    class CA_HorizontalCompass: RscPicture
                    {
                        IDC=207;
                        type=105;
                        font="EtelkaMonospacePro";
                        textSize="0.02*SafezoneH";
                        style=1;
                        color[]={0.94999999,0.94999999,0.94999999,1};
                        x="13.04 * 		(0.01875 * SafezoneH)";
                        y="8.0 * 		(0.025 * SafezoneH)";
                        w="27.18 * 		(0.01875 * SafezoneH)";
                        h="1 * 		(0.025 * SafezoneH)";
                        imageHull="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassHull.paa";
                        imageTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassTurret.paa";
                        imageObsTurret="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\horizontalCompassObsTurret.paa";
                        imageGun="#(rgb,8,8,3)color(0,0,0,0)";
                    };
                    class AzimuthMark: RscPicture
                    {
                        IDC=1012;
                        text="A3\Ui_f\data\IGUI\RscIngameUI\RscOptics\AzimuthMark.paa";
                        x="26.35 * 		(0.01875 * SafezoneH)";
                        y="7.5 * 		(0.025 * SafezoneH)";
                        w="0.5 * 		(0.01875 * SafezoneH)";
                        h="0.5 * 		(0.025 * SafezoneH)";
                        colorText[]={0.94999999,0.94999999,0.94999999,1};
                    };
                    class CA_Heading: RscText
                    {
                        idc=156;
                        style=2;
                        sizeEx="0.032*SafezoneH";
                        shadow=0;
                        font="EtelkaMonospacePro";
                        colorText[]={0.94999999,0.94999999,0.94999999,1};
                        text="015";
                        x="25.15 * 		(0.01875 * SafezoneH)";
                        y="6.25 * 		(0.025 * SafezoneH)";
                        w="3 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                    class CA_LasedRange: RscText
                    {
                        idc=198;
                        style=2;
                        sizeEx="0.032*SafezoneH";
                        shadow=0;
                        font="EtelkaMonospacePro";
                        colorText[]={0.94999999,0.94999999,0.94999999,1};
                        text="2456";
                        x="21.15 * 		(0.01875 * SafezoneH)";
                        y="29.2 * 		(0.025 * SafezoneH)";
                        w="11 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                
                };
            };
        };

        class Redd_RSC_Milan: RscUnitInfo
        {
            idd=300;
            controls[]=
            {
                "CA_Zeroing",
                "CA_IGUI_elements_group"
            };
            class CA_IGUI_elements_group: RscControlsGroup
            {
                idc=170;
                class VScrollbar: VScrollbar
                {
                    width=0;
                };
                class HScrollbar: HScrollbar
                {
                    height=0;
                };
                x="0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
                y="0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
                w="53.5 * 		(0.01875 * SafezoneH)";
                h="40 * 		(0.025 * SafezoneH)";
                class controls
                {
                    
                    class CA_Heading: RscText
                    {
                        idc=156;
                        style=2;
                        sizeEx="0.038*SafezoneH";
                        shadow=0;
                        font="EtelkaMonospacePro";
                        color[]={0.70599997,0.074500002,0.0196,1};
                        colorText[]={0.70599997,0.074500002,0.0196,1};
                        text="015";
                        x="25.15 * 		(0.01875 * SafezoneH)";
                        y="6.25 * 		(0.025 * SafezoneH)";
                        w="3 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                    class CA_Distance: RscText
                    {
                        idc=198;
                        style=0;
                        sizeEx="0.03*SafezoneH";
                        colorText[]={0.70599997,0.074500002,0.0196,0.60000002};
                        shadow=0;
                        font="EtelkaMonospacePro";
                        text="2456";
                        x="25.15 * 		(0.01875 * SafezoneH)";
                        y="32.2 * 		(0.025 * SafezoneH)";
                        w="11 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                };
            };
        }; 

        class Redd_RSC_MG3: RscUnitInfo
        {
            idd=300;
            controls[]=
            {
                "CA_Zeroing",
                "CA_IGUI_elements_group"
            };
            class CA_IGUI_elements_group: RscControlsGroup
            {
                idc=170;
                class VScrollbar: VScrollbar
                {
                    width=0;
                };
                class HScrollbar: HScrollbar
                {
                    height=0;
                };
                x="0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
                y="0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
                w="53.5 * 		(0.01875 * SafezoneH)";
                h="40 * 		(0.025 * SafezoneH)";
                class controls
                {
                    
                    class CA_Heading: RscText
                    {
                        idc=156;
                        style=2;
                        sizeEx="0.038*SafezoneH";
                        shadow=0;
                        font="EtelkaMonospacePro";
                        color[]={0.70599997,0.074500002,0.0196,1};
                        colorText[]={0.70599997,0.074500002,0.0196,1};
                        text="015";
                        x="25.15 * 		(0.01875 * SafezoneH)";
                        y="6.25 * 		(0.025 * SafezoneH)";
                        w="3 * 		(0.01875 * SafezoneH)";
                        h="1.2 * 		(0.025 * SafezoneH)";
                    };
                };
            };
        };
    };