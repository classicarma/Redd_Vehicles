	
	
	class Mode_SemiAuto;
	class Mode_Burst;
	class Mode_FullAuto;
	
	class CfgWeapons 
	{
		
		class autocannon_35mm;
		class LMG_coax;
		class missiles_titan;
		class SmokeLauncher;
		class Default;
		class mortar_82mm;
		class GMG_40mm;
		class GMG_F;
		
		class Redd_MG3: LMG_coax
		{
			
			displayName = "MG 3";
			magazines[] = {"Redd_Mg3_Mag","Redd_Mg3_Mag_120"};
			magazineReloadTime = 8;
			autoReload = 1;
			ballisticsComputer = 2;
			modes[] = {"FullAuto","close","short","medium","far"};
			showToPlayer = 0;
			soundBurst = 0;
			
			class GunParticles 
			{

				class effect1 
				{
					
					positionName = "usti hlavne3";
					directionName = "konec hlavne3";
					effectName = "RifleAssaultCloud";
				};

				class effect2 
				{
					
					positionName = "machinegun_eject_pos";
					directionName = "machinegun_eject_dir";
					effectName = "Redd_MG3Cartridge";
					
				};
				
				class effect3
				{
					
					positionName = "machinegun_eject_pos";
					directionName = "machinegun_eject_dir";
					effectName = "Redd_MG3Eject";
					
				};
			
			};
			
			class FullAuto: Mode_FullAuto 
			{
				
				displayName = "MG 3";
				autoFire = 1;
				burst = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1200);
				dispersion = 0.0008;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_MG3_Shot_SoundSet", "Redd_MG3_Tail_SoundSet"};
					closure1[] = {"A3\sounds_f\weapons\closure\sfx7",0.56234133,1,40};
					closure2[] = {"A3\sounds_f\weapons\closure\sfx8",0.56234133,1,40};
					soundClosure[] = {"closure1",0.5,"closure2",0.5};
				};
				
			};
			
			class close: FullAuto
			{
				
				burst = 10;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 20;
				midRangeProbab = 0.7;
				maxRange = 50;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
				
			};

			class short: close
			{
				
				burst = 8;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 300;
				minRange = 50;
				minRangeProbab = 0.05;
				midRange = 150;
				midRangeProbab = 0.7;
				maxRange = 300;
				maxRangeProbab = 0.1;
				
			};
			
			class medium: close
			{
				
				burst = 6;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 600;
				minRange = 200;
				minRangeProbab = 0.05;
				midRange = 400;
				midRangeProbab = 0.7;
				maxRange = 600;
				maxRangeProbab = 0.1;
				
			};
			
			class far: close
			{
				
				burst = 4;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1000;
				minRange = 400;
				minRangeProbab = 0.05;
				midRange = 750;
				midRangeProbab = 0.4;
				maxRange = 1200;
				maxRangeProbab = 0.01;
				
			};
			
		};
		
		class Redd_MG3_Static: Redd_MG3 {
			magazines[] = {"Redd_Mg3_Belt_100"};
			magazineReloadTime = 6;

			class FullAuto: FullAuto {
				dispersion = 0.003;
			};
		};

		class Redd_MG3_Static_ai: Redd_MG3_Static {
			magazines[] = {"Redd_Mg3_Belt_100_ai"};
		};

		class Redd_MK20: autocannon_35mm
		{
			scope = 1;
			displayName = "MK 20mm";
			magazines[] = {"Redd_MK20_AP_Mag","Redd_MK20_HE_Mag","Redd_MK20_AP_Mag100","Redd_MK20_HE_Mag275"};
			muzzles[] = {"this"};
			multiplier = 1;
			magazineReloadTime = 0.1;
			ballisticsComputer = 2;
			autoReload = 1;
			autoFire = 0;
			soundContinuous = 0;			
			burst = 1;
			modes[] = {"Single","FullAuto","close","short","medium","far"};
			showToPlayer = 0;
			soundBurst = 0;
			
			class gunParticles 
			{
				
				class effect1 
				{
					
					positionName = "usti hlavne";
					directionName = "konec hlavne";
					effectName = "Redd_AutoCannonFired";
					
				};
				
				class effect2
				{
					
					positionName = "shell_eject_pos";
					directionName = "shell_eject_dir";
					effectName = "Redd_HeavyGunCartridge";
					
				};
				
				class effect3
				{
					
					positionName = "shell_eject_pos";
					directionName = "shell_eject_dir";
					effectName = "Redd_HeavyGunEject";
					
				};
				
			};
			
			class HE: autocannon_35mm {
			};

			class AP: autocannon_35mm {
			};
			
			class FullAuto: Mode_FullAuto 
			{
				
				displayName = "MK 20mm";
				autoFire = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/900);
				dispersion = 0.0008;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_Mk20_Shot_SoundSet", "Redd_Mk20_Tail_SoundSet"};
				
				};
				
			};
			
			class Single: Mode_SemiAuto 
			{
				
				displayName = "MK 20mm";
				autoFire = 0;
				sounds[] = {"StandardSound"};
				reloadTime = (60/900);
				dispersion = 0.0008;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_Mk20_Shot_SoundSet", "Redd_Mk20_Tail_SoundSet"};
					
				};
				
			};
			
			class close: FullAuto
			{
				
				burst = 10;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 500;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 250;
				midRangeProbab = 0.7;
				maxRange = 500;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
				
			};
			
			class short: close
			{
				
				burst = 7;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1000;
				minRange = 500;
				minRangeProbab = 0.05;
				midRange = 750;
				midRangeProbab = 0.7;
				maxRange = 1000;
				maxRangeProbab = 0.1;
				
			};
			
			class medium: close
			{
				
				burst = 4;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 1500;
				minRange = 100;
				minRangeProbab = 0.05;
				midRange = 1200;
				midRangeProbab = 0.7;
				maxRange = 1500;
				maxRangeProbab = 0.1;
				
			};
			
			class far: close
			{
				
				burst = 1;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 2000;
				minRange = 1500;
				minRangeProbab = 0.05;
				midRange = 1750;
				midRangeProbab = 0.4;
				maxRange = 2000;
				maxRangeProbab = 0.01;
				
			};
			
		};

		class Redd_MK20FL: Redd_MK20
		{
			
			magazines[] = {"Redd_MK20_AP_Mag120","Redd_MK20_HE_Mag200"};
			ballisticsComputer = 16;

		};

		class Redd_35mm: autocannon_35mm
		{

			scope = 1;
			displayName = "35mm KDA L/90";
			magazines[] = {"Redd_35mm_HE_Mag","Redd_35mm_AP_Mag"};
			muzzles[] = {"this"};
			multiplier = 1;
			magazineReloadTime = 0.1;
			ballisticsComputer = 1;
			autoReload = 1;
			autoFire = 0;
			soundContinuous = 0;
			modes[] = {"Single","Burst","FullAuto","close","short","medium","far"};
			showToPlayer = 0;
			soundBurst = 0;
			shotFromTurret=0;
			textureType="burst";

			class gunParticles 
			{
				
				class effect1 
				{
					
					positionName = "usti hlavne 1";
					directionName = "konec hlavne 1";
					effectName = "Redd_AutoCannonFired";
					
				};

				class effect2 
				{
					
					positionName = "usti hlavne 2";
					directionName = "konec hlavne 2";
					effectName = "Redd_AutoCannonFired";
					
				};

				class effect3 
				{
					
					positionName = "usti hlavne 4";
					directionName = "konec hlavne 4";
					effectName = "Redd_AutoCannonFired";
					
				};

				class effect4 
				{
					
					positionName = "usti hlavne 5";
					directionName = "konec hlavne 5";
					effectName = "Redd_AutoCannonFired";
					
				};
				
				class effect5
				{
					
					positionName = "shell_eject_pos 1";
					directionName = "shell_eject_dir 1";
					effectName = "Redd_HeavyGunCartridge_2";
					
				};

				class effect6
				{
					
					positionName = "shell_eject_pos 2";
					directionName = "shell_eject_dir 2";
					effectName = "Redd_HeavyGunCartridge_2";
					
				};
				
				class effect7
				{
					
					positionName = "gurt_eject_pos 1";
					directionName = "gurt_eject_dir 1";
					effectName = "Redd_HeavyGunEject_2";
					
				};

				class effect8
				{
					
					positionName = "gurt_eject_pos 2";
					directionName = "gurt_eject_dir 2";
					effectName = "Redd_HeavyGunEject_2";
					
				};
				
			};
			
			class HE: autocannon_35mm {
			};

			class AP: autocannon_35mm {
			};
			
			class FullAuto: Mode_FullAuto 
			{
				
				displayName = "35mm KDA L/90";
				autoFire = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
				
				};
				
			};
			
			class Burst: Mode_Burst 
			{
				
				displayName = "35mm KDA L/90";
				autoFire = 0;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				burst = 10;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
					
				};
				
			};
			
			class Single: Mode_SemiAuto 
			{
				
				displayName = "35mm KDA L/90";
				autoFire = 0;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				burst = 1;

				class StandardSound
				{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
					
				};
				
			};

			class close: FullAuto
			{
				
				burst = 18;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 750;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 500;
				midRangeProbab = 0.7;
				maxRange = 1000;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
				
			};
			
			class short: close
			{
				
				burst = 15;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1250;
				minRange = 750;
				minRangeProbab = 0.05;
				midRange = 1250;
				midRangeProbab = 0.7;
				maxRange = 1500;
				maxRangeProbab = 0.1;
				
			};
			
			class medium: close
			{
				
				burst = 12;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 1750;
				minRange = 1250;
				minRangeProbab = 0.05;
				midRange = 1750;
				midRangeProbab = 0.7;
				maxRange = 2000;
				maxRangeProbab = 0.1;
				
			};
			
			class far: close
			{
				
				burst = 9;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 2250;
				minRange = 1750;
				minRangeProbab = 0.05;
				midRange = 3000;
				midRangeProbab = 0.4;
				maxRange = 3500;
				maxRangeProbab = 0.01;
				
			};

		};

		class Redd_35mm_mantis: Redd_35mm {

			magazines[] = {"Redd_35mm_HE_2_Mag"};
			modes[] = {"FullAuto","close","short","medium","far"};
			displayName = "35mm GDF-020";

			class gunParticles {

				class effect1 {
					positionName = "usti hlavne 1";
					directionName = "konec hlavne 1";
					effectName = "Redd_AutoCannonFired";
				};

				class effect2 {
					positionName = "shell_eject_pos 1";
					directionName = "shell_eject_dir 1";
					effectName = "Redd_HeavyGunCartridge_2";
				};

				class effect3{
					positionName = "shell_eject_pos 2";
					directionName = "shell_eject_dir 2";
					effectName = "Redd_AutoCannonFired";
				};
			};

			class FullAuto: Mode_FullAuto {
				
				displayName = "35mm GDF-020";
				autoFire = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
				};
			};
			
			class close: FullAuto{
				
				burst = 18;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 750;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 500;
				midRangeProbab = 0.7;
				maxRange = 1000;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
			};
			
			class short: close{
				
				burst = 15;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1250;
				minRange = 750;
				minRangeProbab = 0.05;
				midRange = 1250;
				midRangeProbab = 0.7;
				maxRange = 1500;
				maxRangeProbab = 0.1;
			};
			
			class medium: close{
				
				burst = 12;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 1750;
				minRange = 1250;
				minRangeProbab = 0.05;
				midRange = 1750;
				midRangeProbab = 0.7;
				maxRange = 2000;
				maxRangeProbab = 0.1;
			};
			
			class far: close{
				
				burst = 9;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 2250;
				minRange = 1750;
				minRangeProbab = 0.05;
				midRange = 3000;
				midRangeProbab = 0.4;
				maxRange = 3500;
				maxRangeProbab = 0.01;
			};
		};
		
		class Redd_Gesichert: Default 
		{
			
			scope = 2;
			displayName = "$STR_Redd_Gesichert";
			magazines[] = {};
			textureType = "fullAuto";
			
		};
		
		class Redd_Milan: missiles_titan
		{
			
			displayName = "Milan";
			magazines[] = {"Redd_Milan_Mag"};
			ballisticsComputer = 0;
			autoReload = 1;
			magazineReloadTime = 15;
			reloadTime = 1;
			minRange = 150;
			minRangeProbab = 0.600000;
			midRange = 1500;
			midRangeProbab = 0.700000;
			maxRange = 4000;
			maxRangeProbab = 0.001000;
			canLock = 0;

			class StandardSound 
			{
			
				soundSetShot[] = {"Redd_Milan_Shot_SoundSet", "Redd_Milan_Tail_SoundSet"};

			};
			
		};

		class Redd_TOW: Redd_Milan
		{

			displayName = "TOW";
			magazines[] = {"Redd_TOW_Mag"};
			
		};
		
		class Redd_SmokeLauncher: SmokeLauncher 
		{
			
			magazines[] = {"Redd_SmokeLauncherMag"};
			reloadTime = 3;
			magazineReloadTime = 30;
			autoFire = 0;
			simulation = "cmlauncher";
			showToPlayer = 1;
			minRange = 0;
			maxRange = 10000;
			textureType = "semi";
			canLock = 0;
			
		};

		class Redd_mortar_120mm: mortar_82mm
		{

			displayname="M120 Tampella";
			sounds[] = {"StandardSound"};
			modes[] = {"Single1", "Single2", "Single3", "Single4", "Burst1", "Burst2", "Burst3", "Burst4"};
			magazineReloadTime = 0.5;

			class StandardSound
			{

				soundSetShot[] = {"Redd_Mortar_120mm_Shot_SoundSet", "Redd_Mortar_120mm_Tail_SoundSet"};

			};

			class GunParticles
			{
				class FirstEffect
				{
					effectName = "Redd_MortarFired";
					positionName = "Usti Hlavne";
					directionName = "Konec Hlavne";
				};
			};
			
			magazines[]=
			{
				
				"Redd_8Rnd_120mm_Mo_shells",
				"Redd_8Rnd_120mm_Mo_annz_shells",
				"Redd_8Rnd_120mm_Mo_Flare_white",
				"Redd_8Rnd_120mm_Mo_Smoke_white",

				"Redd_1Rnd_120mm_Mo_shells",
				"Redd_1Rnd_120mm_Mo_annz_shells",
				"Redd_1Rnd_120mm_Mo_Flare_white",
				"Redd_1Rnd_120mm_Mo_Smoke_white"
				
			};

			class Single1: Mode_SemiAuto
			{

				displayName = "$STR_Redd_close";
				sounds[] = {"StandardSound"};
				reloadSound[] = {"A3\Sounds_F\arsenal\weapons_static\Mortar\reload_mortar", 1, 1, 20};
				soundServo[] = {"", 0.0001, 1};
				reloadTime = 1.8;
				minRange = 0;
				midRange = 0;
				maxRange = 0;
				artilleryDispersion = 1.9;
				artilleryCharge = 0.35;

				class StandardSound
				{
					
					soundSetShot[] = {"Redd_Mortar_120mm_Shot_SoundSet", "Redd_Mortar_120mm_Tail_SoundSet"};

				};

			};

			class Single2: Single1 
			{

				displayName = "$STR_Redd_medium";
				artilleryCharge = 0.7;

			};

			class Single3: Single1 
			{

				displayName = "$STR_Redd_far";
				artilleryCharge = 1;

			};

			class Single4: Single1 
			{

				displayName = "$STR_Redd_farther";
				artilleryCharge = 1.2512; //250,24

			};
			
			class Burst1: Mode_Burst
			{
				
				showToPlayer = 0;
				displayName = "Burst (close)";
				burst = 4;
				sounds[] = {"StandardSound"};
				reloadSound[] = {"A3\Sounds_F\arsenal\weapons_static\Mortar\reload_mortar", 1, 1, 20};
				soundServo[] = {"", 0.0001, 1};
				soundBurst = 0;
				reloadTime = 1.8;
				minRange = 60;
				minRangeProbab = 0.5;
				midRange = 290;
				midRangeProbab = 0.7;
				maxRange = 665;
				maxRangeProbab = 0.5;
				artilleryDispersion = 2.2;
				artilleryCharge = 0.35;							

				class StandardSound
				{
					
					
					soundSetShot[] = {"Redd_Mortar_120mm_Shot_SoundSet", "Redd_Mortar_120mm_Tail_SoundSet"};

				};
				
			};

			class Burst2: Burst1
			{

				showToPlayer = 0;
				displayName = "Burst (medium)";
				minRange = 230;
				minRangeProbab = 0.4;
				midRange = 1175;
				midRangeProbab = 0.6;
				maxRange = 2660;
				maxRangeProbab = 0.4;
				artilleryCharge = 0.7;

			};

			class Burst3: Burst1
			{

				showToPlayer = 0;
				displayName = "Burst (far)";
				minRange = 540;
				minRangeProbab = 0.3;
				midRange = 2355;
				midRangeProbab = 0.4;
				maxRange = 5500;
				maxRangeProbab = 0.3;
				artilleryCharge = 1;


			};
			
			class Burst4: Burst1
			{

				showToPlayer = 0;
				displayName = "Burst (farther)";
				minRange = 1000;
				minRangeProbab = 0.3;
				midRange = 4000;
				midRangeProbab = 0.4;
				maxRange = 7000;
				maxRangeProbab = 0.3;
				artilleryCharge = 1.2512;

			};
			
		};

		class Redd_mortar_120mm_veh: mortar_82mm {
			
			magazines[]= {
				"Redd_1Rnd_120mm_Mo_shells",
				"Redd_1Rnd_120mm_Mo_annz_shells",
				"Redd_1Rnd_120mm_Mo_Flare_white",
				"Redd_1Rnd_120mm_Mo_Smoke_white"
			};
		};

		class Redd_gmw_Static: GMG_40mm {
			displayName = "GMW 40mm";
			magazines[] = {"Redd_gmw_Belt_32"};
			modes[] = {"manual", "close", "short", "medium", "far"};
			magazineReloadTime = 8;
			maxZeroing = 1500;
			ballisticsComputer = 2;
			
			class GunParticles {

				class effect1 {
					positionName = "usti hlavne3";
					directionName = "konec hlavne3";
					effectName = "GrenadeLauncherCloud";
				};

				class effect2 {
					positionName = "gmw_eject_pos";
					directionName = "gmw_eject_dir";
					effectName = "Redd_HeavyGunCartridge_3";
				};
			};

			class manual: GMG_F {
				displayName = "GMW 40mm";
				sounds[] = {"StandardSound"};
				class StandardSound{
					begin1[] = {"\redd_vehicles_main\sounds\gmw_fire.ogg", 1.12202, 1, 1200};
					begin2[] = {"\redd_vehicles_main\sounds\gmw_fire.ogg", 1.12202, 1, 1200};
					begin3[] = {"\redd_vehicles_main\sounds\gmw_fire.ogg", 1.12202, 1, 1200};
					soundBegin[] = {"begin1", 0.33, "begin2", 0.33, "begin3", 0.34};
					soundSetShot[] = {"rnt_gmw_Shot_SoundSet", "GMG40mm_Tail_SoundSet"};
				};
				soundContinuous = 0;
				soundBurst = 0;
				reloadTime = (60/300);
				dispersion = 0.0038;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 10;
				minRange = 0;
				minRangeProbab = 0.01;
				midRange = 1;
				midRangeProbab = 0.01;
				maxRange = 2;
				maxRangeProbab = 0.01;
			};

			class close: manual {
				aiBurstTerminable = 1;
				showToPlayer = 0;
				burst = 1;
				burstRangeMax = 6;
				aiRateOfFire = 1;
				aiRateOfFireDispersion = 2;
				aiRateOfFireDistance = 50;
				minRange = 16;
				minRangeProbab = 0.1;
				midRange = 200;
				midRangeProbab = 0.5;
				maxRange = 400;
				maxRangeProbab = 0.2;
			};

			class short: close {
				aiBurstTerminable = 1;
				showToPlayer = 0;
				burst = 1;
				burstRangeMax = 5;
				aiRateOfFire = 1;
				aiRateOfFireDispersion = 2;
				aiRateOfFireDistance = 200;
				minRange = 200;
				minRangeProbab = 0.5;
				midRange = 400;
				midRangeProbab = 0.7;
				maxRange = 800;
				maxRangeProbab = 0.7;
			};

			class medium: close {
				aiBurstTerminable = 1;
				showToPlayer = 0;
				burst = 1;
				burstRangeMax = 5;
				aiRateOfFire = 2;
				aiRateOfFireDispersion = 2;
				aiRateOfFireDistance = 700;
				minRange = 700;
				minRangeProbab = 0.7;
				midRange = 1000;
				midRangeProbab = 0.8;
				maxRange = 1500;
				maxRangeProbab = 0.75;
			};

			class far: close {
				aiBurstTerminable = 1;
				showToPlayer = 0;
				burst = 1;
				burstRangeMax = 3;
				aiRateOfFire = 4;
				aiRateOfFireDispersion = 4;
				aiRateOfFireDistance = 1000;
				minRange = 1000;
				minRangeProbab = 0.8;
				midRange = 1500;
				midRangeProbab = 0.75;
				maxRange = 2000;
				maxRangeProbab = 0.1;
			};

			drySound[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG20mm_static_dry", 1, 1, 10};
		};

		class Redd_gmw_Static_ai: Redd_gmw_Static {
			magazines[] = {"Redd_gmw_Belt_32_ai"};
		};
	};