﻿	
	
	class CfgSoundShaders 
	{

		//MG3
		class Redd_MG3_Closure_SoundShader
		{
			
			samples[]=
			{
				
				{"\Redd_Vehicles_Main\sounds\bwa3_mg5_firing_closure1", 1}

			};

			volume=0.3;
			range=5;

		};
		
		class Redd_MG3_closeShot_SoundShader 
		{
			
			samples[]= 
			{
				
				{"\Redd_Vehicles_Main\sounds\Redd_MG_3_1.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MG_3_2.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MG_3_3.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MG_3_4.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MG_3_5.ogg", 1}
				
			};
			
			volume=db6;
			range=50;
			rangeCurve="closeShotCurve";
			
		};
		
		class Redd_MG3_Bass_SoundShader: Redd_MG3_closeShot_SoundShader
		{

			samples[]=
			{
				
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_bass1",1}

			};

			volume=1;

		};
		
		class Redd_MG3_midShot_SoundShader 
		{
			
			samples[] = 
			{
				
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_mid1",1},
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_mid2",1},
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_mid3",1}

			};
			volume=3;
			range=2000;
			rangeCurve[]=
			{
				{0,0.5},
				{50,1},
				{300,0},
				{2000,0}
			};
			
		};
		
		class Redd_MG3_distShot_SoundShader 
		{
			
			samples[] = 
			{
				
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_dist1",1}
				
			};
			volume=1.8;
			range=2000;
			rangeCurve[]=
			{
				{0,0},
				{50,0.5},
				{300,1},
				{2000,1}
			};
			
		};
		
		class Redd_MG3_tailDistant_SoundShader 
		{
			
			samples[] = {{"Redd_Vehicles_Main\sounds\bwa3_mg3_coax_taildistant",1}};
			volume=3.5;
			range=2000;
			rangeCurve[]=
			{
				{0,0.2},
				{600,1},
				{2000,1}
			};
			limitation=1;
			
		};
		
		class Redd_MG3_tailForest_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailforest",
					1
				}
				
				
			};
			volume="(1-interior/1.4)*forest/3";
			range=2000;
			rangeCurve[]=
			{
				{0,1},
				{2000,0.30000001}
			};
			limitation=1;
			
		};
		
		class Redd_MG3_tailHouses_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailhouses",
					1
				}
			};
			volume="(1-interior/1.4)*houses/3";
			range=2000;
			rangeCurve[] = {{0,1},{200,0.5},{800,0.3},{2000,0}};
			limitation=1;
			
		};
		
		class Redd_MG3_tailMeadows_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailmeadows",
					1
				}
			};
			volume="(1-interior/1.4)*(meadows/2 max sea/2)/3";
			range=2000;
			rangeCurve[]=
			{
				{0,1},
				{2000,0.30000001}
			};
			limitation=1;
			
		};

		class Redd_MG3_tailTrees_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailtrees",
					1
				}
			};
			volume="(1-interior/1.4)*trees/3";
			range=2000;
			rangeCurve[]=
			{
				{0,1},
				{2000,0.30000001}
			};
			limitation=1;
			
		};
		
		//MK20
		class Redd_MK20_closeShot_SoundShader 
		{
			
			samples[] = 
			{
				
				{"\Redd_Vehicles_Main\sounds\Redd_MK_20_1.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MK_20_2.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MK_20_3.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MK_20_4.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_MK_20_5.ogg", 1}
				
			};
			volume = db10;
			range = 150;
			rangeCurve = "closeShotCurve";
			
		};

		class Redd_MK20_Bass_SoundShader: Redd_MK20_closeShot_SoundShader
		{

			samples[]=
			{
				
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_bass1",1}

			};

			volume=1;

		};
		
		class Redd_MK20_midShot_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_midShot_01",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_midShot_02",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_midShot_03",
					1
				}
			};
			volume=1.4;
			range=2300;
			rangeCurve[]=
			{
				{0,0.5},
				{200,1},
				{400,0},
				{2300,0}
			};
			
		};
		
		class Redd_MK20_distShot_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_distShot_01",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_distShot_02",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_distShot_03",
					1
				}
			};
			volume=1.8;
			range=2300;
			rangeCurve[]=
			{
				{0,0},
				{200,0},
				{400,1},
				{2300,1}
			};
			
		};
		
		class Redd_MK20_tailDistant_SoundShader 
		{
			
			samples[] = {{"\Redd_Vehicles_Main\sounds\bwa3_mg3_coax_taildistant",1}};
			volume=3.5;
			range=2300;
			rangeCurve[]=
			{
				{0,0},
				{600,1},
				{2300,1}
			};
			limitation=1;
			
		};
		
		class Redd_MK20_tailForest_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailforest",
					1
				}
				
				
			};
			volume="(1-interior/1.4)*forest/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{2300,0.3}
			};
			limitation=1;
	
		};
		
		class Redd_MK20_tailHouses_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailhouses",
					1
				}
			};
			volume="(1-interior/1.4)*houses/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{200,0.5},
				{800,0.3},
				{2300,0}
			};
			limitation=1;

		};
		
		class Redd_MK20_tailMeadows_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailmeadows",
					1
				}
			};
			volume="(1-interior/1.4)*(meadows/2 max sea/2)/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{2300,0.3}
			};
			limitation=1;
			
		};

		class Redd_MK20_tailTrees_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailtrees",
					1
				}
			};
			volume="(1-interior/1.4)*trees/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{2300,0.3}
			};
			limitation=1;
			
		};
		
		//Milan
		class Redd_Milan_closeShot_SoundShader 
		{

			samples[] = {{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_closeExp_03",1}};
			volume = 1.0;
			range = 90;
			rangeCurve = "CannonCloseShotCurve";

		};

		class Redd_Milan_midShot_SoundShader 
		{

			samples[] = {{"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Titan_launcher_midShot",1}};
			volume = 1.9952624;
			range = 350;
			rangeCurve = "CannonMidShotCurve";

		};

		class Redd_Milan_distShot_SoundShader 
		{

			samples[] = {{"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Titan_launcher_distShot",1}};
			volume = 1.9952624;
			range = 2500;
			rangeCurve[] = {{0,0},{100,0},{300,1},{2500,1}};

		};

		class Redd_Milan_tailForest_SoundShader 
		{

			samples[] = {{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailForest_01", 1}, {"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailForest_02", 1}, {"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailForest_03", 1}, {"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailForest_04", 1}};
			volume = "forest";
			range = 2500;
			limitation = 1;
			rangeCurve[] = {{0,1},{300,1},{2500,0.3}};

		};

		class Redd_Milan_tailMeadows_SoundShader 
		{

			samples[] = {{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailMeadows_01",1},{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailMeadows_02",1},{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailMeadows_03",1},{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailMeadows_04",1}};
			volume = "meadows max sea/2";
			range = 2500;
			limitation = 1;
			rangeCurve[] = {{0,1},{300,1},{2500,0.3}};

		};

		class Redd_Milan_tailHouses_SoundShader 
		{

			samples[] = {{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailHouses_01",1},{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailHouses_02",1},{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailHouses_03",1},{"A3\Sounds_F\arsenal\explosives\shells\ShellLightA_tailHouses_04",1}};
			volume = "houses";
			range = 2500;
			limitation = 1;
			rangeCurve[] = {{0,1},{300,1},{2500,0.3}};

		};

		//35mm
		class Redd_35mm_closeShot_SoundShader 
		{
			
			samples[] = 
			{
				
				{"\Redd_Vehicles_Main\sounds\Redd_35mm_1.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_35mm_2.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_35mm_3.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_35mm_4.ogg", 1},
				{"\Redd_Vehicles_Main\sounds\Redd_35mm_5.ogg", 1}
				
			};
			
			volume = db10;
			range = 150;
			rangeCurve = "closeShotCurve";
			
		};
		
		class Redd_35mm_Bass_SoundShader: Redd_35mm_closeShot_SoundShader
		{

			samples[]=
			{
				
				{"\Redd_Vehicles_Main\sounds\bwa3_mg3_firing_bass1",1}

			};

			volume=1;

		};

		class Redd_35mm_midShot_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_midShot_01",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_midShot_02",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_midShot_03",
					1
				}
			};
			volume=1.4;
			range=2300;
			rangeCurve[]=
			{
				{0,0.5},
				{200,1},
				{400,0},
				{2300,0}
			};
			
		};
		
		class Redd_35mm_distShot_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_distShot_01",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_distShot_02",
					1
				},
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\cannon_40mm\Autocannon40mmBody_distShot_03",
					1
				}
			};
			volume=1.8;
			range=2300;
			rangeCurve[]=
			{
				{0,0},
				{200,0},
				{400,1},
				{2300,1}
			};
			
		};
		
		class Redd_35mm_tailDistant_SoundShader 
		{
			
			samples[] = {{"\Redd_Vehicles_Main\sounds\bwa3_mg3_coax_taildistant",1}};
			volume=3.5;
			range=2300;
			rangeCurve[]=
			{
				{0,0},
				{600,1},
				{2300,1}
			};
			limitation=1;
			
		};
		
		class Redd_35mm_tailForest_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailforest",
					1
				}
				
			};
			volume="(1-interior/1.4)*forest/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{2300,0.3}
			};
			limitation=1;
	
		};
		
		class Redd_35mm_tailHouses_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailhouses",
					1
				}
			};
			volume="(1-interior/1.4)*houses/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{200,0.5},
				{800,0.3},
				{2300,0}
			};
			limitation=1;

		};
		
		class Redd_35mm_tailMeadows_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailmeadows",
					1
				}
			};
			volume="(1-interior/1.4)*(meadows/2 max sea/2)/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{2300,0.3}
			};
			limitation=1;
			
		};

		class Redd_35mm_tailTrees_SoundShader 
		{
			
			samples[]=
			{
				
				{
					"A3\Sounds_F\arsenal\weapons_vehicles\LMG_762mm\LMGCoax762mm_tailtrees",
					1
				}
			};
			volume="(1-interior/1.4)*trees/3";
			range=2300;
			rangeCurve[]=
			{
				{0,1},
				{2300,0.3}
			};
			limitation=1;
			
		};

		class Redd_Mortar_120mm_closeShot_SoundShader
		{

			samples[] = 
			{

				{"\Redd_Vehicles_Main\sounds\Redd_M120_1.ogg", 1}, 
				{"\Redd_Vehicles_Main\sounds\Redd_M120_2.ogg", 1}, 
				{"\Redd_Vehicles_Main\sounds\Redd_M120_3.ogg", 1} 

			};

			volume = 1;
			range = 70;
			rangeCurve[] = {{0, 1}, {50, 0.75}, {70, 0}};

		};

		class Redd_Mortar_120mm_midShot_SoundShader
		{

			samples[] = 
			{

				{"\Redd_Vehicles_Main\sounds\Redd_M120_mid_1.ogg", 1}, 
				{"\Redd_Vehicles_Main\sounds\Redd_M120_mid_2.ogg", 1}, 
				{"\Redd_Vehicles_Main\sounds\Redd_M120_mid_3.ogg", 1} 
				
			};

			volume = 1;
			range = 2200;
			rangeCurve[] = {{0, 1}, {100, 1}, {500, 0}, {2200, 0}};

		};
		class Redd_Mortar_120mm_distShot_SoundShader
		{

			samples[] = 
			{

				{"\Redd_Vehicles_Main\sounds\Redd_M120_dist_1.ogg", 1}, 
				{"\Redd_Vehicles_Main\sounds\Redd_M120_dist_2.ogg", 1}, 
				{"\Redd_Vehicles_Main\sounds\Redd_M120_dist_3.ogg", 1} 
				
			};

			volume = 1.12202;
			range = 2200;
			limitation = 1;
			rangeCurve[] = {{0, 1}, {250, 1}, {2200, 0}};

		};

		class Redd_Mortar_120mm_tailForest_SoundShader
		{

			samples[] = {{"A3\Sounds_F\arsenal\weapons_static\Mortar\Mortar82mm_tailForest", 1}};
			volume = "forest";
			range = 2200;
			limitation = 1;
			rangeCurve[] = {{0, 1}, {250, 1}, {2200, 0}};

		};

		class Redd_Mortar_120mm_tailMeadows_SoundShader
		{

			samples[] = {{"A3\Sounds_F\arsenal\weapons_static\Mortar\Mortar82mm_tailMeadows", 1}};
			volume = "meadows max sea/2";
			range = 2200;
			limitation = 1;
			rangeCurve[] = {{0, 1}, {250, 1}, {2200, 0}};

		};

		class Redd_Mortar_120mm_tailHouses_SoundShader
		{

			samples[] = {{"A3\Sounds_F\arsenal\weapons_static\Mortar\Mortar82mm_tailHouses", 1}};
			volume = "houses";
			range = 2200;
			limitation = 1;
			rangeCurve[] = {{0, 1}, {250, 1}, {2200, 0}};

		};

		class rnt_gmw_closeShot_SoundShader
		{
			samples[] = {
				{"\redd_vehicles_main\sounds\gmw_fire.ogg", 1},
				{"\redd_vehicles_main\sounds\gmw_fire.ogg", 1},
				{"\redd_vehicles_main\sounds\gmw_fire.ogg", 1}
			};
			volume = 3.5;
			range = 50;
			rangeCurve = "closeShotCurve";
		};
		class rnt_gmw_midShot_SoundShader
		{
			samples[] = {{"A3\Sounds_F\arsenal\weapons_vehicles\GMG_40mm\GMG40mm_midShot_01", 1}, {"A3\Sounds_F\arsenal\weapons_vehicles\GMG_40mm\GMG40mm_midShot_02", 1}, {"A3\Sounds_F\arsenal\weapons_vehicles\GMG_40mm\GMG40mm_midShot_03", 1}};
			volume = 0.707946;
			range = 1300;
			rangeCurve[] = {{0, 0.2}, {50, 1}, {200, 0}, {1300, 0}};
		};
		class rnt_gmw_distShot_SoundShader
		{
			samples[] = {{"A3\Sounds_F\arsenal\weapons_vehicles\GMG_40mm\GMG40mm_distShot_01", 1}, {"A3\Sounds_F\arsenal\weapons_vehicles\GMG_40mm\GMG40mm_distShot_02", 1}, {"A3\Sounds_F\arsenal\weapons_vehicles\GMG_40mm\GMG40mm_distShot_03", 1}};
			volume = 1;
			range = 1300;
			rangeCurve[] = {{0, 0}, {50, 0}, {200, 1}, {1300, 1}};
		};
		
	};