

	//Triggered by BI eventhandler "fired" when Marder fires a weapon

	params ["_veh","_weap"];
	
	if (_weap == "Redd_SmokeLauncher") then 
	{

		[_veh] spawn redd_fnc_SmokeLauncher; //Spawns smokelauncher function
		
	};

	if (_weap == "Redd_Milan") then 
	{

		[_veh] call redd_fnc_Milan_Fired; //Calls function to drop empty Milan tube
		
	};