

	//Triggerd by BI eventhandler "getout"
	
	params["_veh","_pos","_unit","_turret"];

	//checks if unit is in Milan
	if (_turret isEqualTo [1]) then
	{

		[_veh,[[0,0],false]] remoteExecCall ['lockTurret']; //unlocks the commander seat which was locked before to prevent units getting in commander seat while actual commander is in milan
		_veh setVariable ['Redd_Marder_Commander_Up', false,true];//resets the variable

	};

	//check if unit is in bino turret
	if (_turret isEqualTo [2]) then
	{	
		_veh setVariable ['Redd_Marder_Bino_In', false, true];
		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];
	};

	if ((_veh getVariable 'has_camonet_large') or (_veh getVariable 'has_camonet')) then
	{
		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");

		_unit setUnitTrait ["camouflageCoef", _camouflage];
	};