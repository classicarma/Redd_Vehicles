
	
	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;
			
		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/
		class Redd_Marder_1A5_base: Tank_F
		{
			
			#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
			displayname = "$STR_Marder_1A5";
			picture="\A3\armor_f_beta\APC_Tracked_01\Data\UI\APC_Tracked_01_base_ca.paa";
			icon="\A3\armor_f_beta\APC_Tracked_01\Data\UI\map_APC_Tracked_01_CA.paa";
			side = 1;
			crew = "B_crew_F";
			author = "Redd";
			model = "\Redd_Marder_1A5\Redd_Marder_1A5";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Spz";
			smokeLauncherGrenadeCount = 6;
			smokeLauncherVelocity = 14;
			smokeLauncherOnTurret = 1;
			smokeLauncherAngle = 142.5;
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="Redd_Marder_Driver_Out";
			driverInAction="Redd_Marder_Driver";
			armor = 300;
			armorStructural = 6;
			cost = 1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_3_bound";
			dustFrontRightPos = "wheel_2_3_bound";
			dustBackLeftPos = "wheel_1_7_bound";
			dustBackRightPos = "wheel_2_7_bound";
			viewDriverInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1100; //Pilot
			enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos cargo";
			forceHideDriver = 0;
			driverWeaponsInfoType="Redd_RCS_Driver";
			headGforceLeaningFactor[]={0.00075,0,0.0075};

			transportSoldier = 3;
			typicalCargo[] = {"B_crew_F"};
			cargoAction[]={"Redd_Marder_Passenger"};
			cargoProxyIndexes[] = {1,2,3};
			viewCargoInExternal = 1;
			cargoCompartments[] = {"Compartment2"};
			hideWeaponsCargo = 0;

			threat[]={0.9,0.6,0.7};
			audible = 6;
			camouflage = 7;	

			TFAR_AdditionalLR_Turret[] = {{1},{2}};
			
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver","commander","gunner",{"ffv",{0,3}},{"Turret",{1},{"Turret",{2}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver","commander","gunner",{"ffv",{0,3}},{"Turret",{1},{"Turret",{2}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1},{"Turret",{2}}};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 1;
			acre_infantryPhoneDisableRinging = 0;
			acre_infantryPhoneCustomRinging[] = {};
			acre_infantryPhoneIntercom[] = {"all"};
			acre_infantryPhoneControlActions[] = {"all"};
			acre_eventInfantryPhone = "QFUNC(noApiFunction)";
			//Ende ACRE2
			
			hiddenSelections[] = 
			{
				
				"Camo1",
				"Camo2",

				"zug1",
				"zug2",

				"plate_hinten_1",
				"plate_hinten_2",
				"plate_hinten_3",
				"plate_hinten_4",
				"plate_hinten_5",
				"plate_hinten_6",
				"plate_vorne_1",
				"plate_vorne_2",
				"plate_vorne_3",
				"plate_vorne_4",
				"plate_vorne_5",
				"plate_vorne_6",

				"Bataillon",
				"Kompanie",
				"Bataillon_hinten",
				"Kompanie_hinten"
				
			};
			
			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"Redd_Marder_1A5\mats\Redd_Marder_1A5_Camo1.rvmat",
					"Redd_Marder_1A5\mats\Redd_Marder_1A5_Camo1_damage.rvmat",
					"Redd_Marder_1A5\mats\Redd_Marder_1A5_Camo1_destruct.rvmat"

				};
				
        	};

			class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};
			
			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust";
					direction = "exhaust_dir";
					effect = "ExhaustEffectTankBack";
					
				};
				
			};
			
			class Reflectors
			{

				class Left 
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
					intensity = 1;
					useFlare = 0;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };

				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Orangelight
				{

					color[]={236, 99, 17};
					ambient[]={10,10,10};
					position="pos_orangelicht";
					direction="dir_orangelicht";
					hitpoint="";
					selection="pos_orangelicht";
					size=2;
					innerAngle=0;
					outerAngle=100;
					coneFadeCoef=1;
					intensity=250;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Orangelight_flare
				{

					color[]={236, 99, 17};
					ambient[]={0,0,0};
					position="pos_orangelicht_flare";
					direction="dir_orangelicht_flare";
					hitpoint="";
					selection="pos_orangelicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=250;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};

			};

			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull
				{	
				
					armor=2;
					material=-1;
					name="telo";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.3;
					armorComponent="";
					
				};

				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="Camo1";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.3;
					armorComponent="";
					
				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="motor";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.3;
					armorComponent="";
					
				};
				
				class HitLTrack: HitLTrack
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					armorComponent="";
					
				};
				
				class HitRTrack: HitRTrack
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					armorComponent="";
					
				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};
				
				class _xx_milan_Bag
				{
					
					backpack = "Redd_Milan_Static_Barrel";
					count = 1;
					
				};

				class _xx_milan_tripod
				{
					
					backpack = "Redd_Milan_Static_Tripod";
					count = 1;
					
				};
				
			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};
			
			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets: Turrets 
					{
						
						class CommanderOptics: CommanderOptics //[0,0] Commander
						{
						
							gunnerForceOptics=0;
							gunnerAction = "rnt_marder_com_out";
							gunnerInAction="rnt_marder_com";
							proxyIndex = 1;
							initElev = 0;
							minElev = -15;
							maxElev = 15;
							initTurn = 0;
							minTurn = -68;
							maxTurn = 68;
							viewGunnerInExternal = 1;
							hideProxyInCombat = 1;
							proxyType= "CPGunner";
							startEngine = 0;
							gunnerGetInAction="GetInHigh";
							gunnerGetOutAction="GetOutHigh";
							turretInfoType = "Redd_RCS_Turret";
							stabilizedInAxes = 3;
							lodTurnedIn = 1000; //Gunner
							lodTurnedOut = 1000; //Gunner
							soundAttenuationTurret = "TankAttenuation";
							gunnerCompartments= "Compartment3";
							weapons[] = {"Redd_SmokeLauncher"};
							magazines[] = {"Redd_SmokeLauncherMag"};
							ace_fcs_Enabled = 0;
							gunnerOutOpticsModel="";
							outGunnerMayFire=0;
							soundServo[]={};
							soundServoVertical[]={};
							animationSourceStickX="com_turret_control_x";
							animationSourceStickY="com_turret_control_y";
							gunnerLeftHandAnimName="com_turret_control";
							gunnerRightHandAnimName="com_turret_control";

							class ViewOptics: ViewOptics
							{
								
								visionMode[] = {"Normal", "NVG", "TI"};
								thermalMode[] = {2,3};
								initFov = 0.4;
								minFov = 0.08;
								maxFov = 0.03;
								
							};
							
							class OpticsIn
							{

								class Day1
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.23;
									maxFov = 0.23;
									minFov = 0.23;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T1";
									gunnerOpticsEffect[] = {};
												
								};
								
								class Day2
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.06;
									maxFov = 0.06;
									minFov = 0.06;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T2";
									gunnerOpticsEffect[] = {};
									
								};
								class WBG1
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.23;
									maxFov = 0.23;
									minFov = 0.23;
									visionMode[] = {"Ti"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W1";
									gunnerOpticsEffect[] = {};
									
								};
								class WBG2
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.023;
									maxFov = 0.023;
									minFov = 0.023;
									visionMode[] = {"Ti"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W2";
									gunnerOpticsEffect[] = {};
									
								};
								
							};

							class HitPoints 
							{
								
								class HitTurret	
								{
									
									armor = 1;
									material = -1;
									name = "vezVelitele";
									visual="Camo1";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.08;
									armorComponent="";
									isTurret=1;

								};
								
								class HitGun	
								{
									
									armor = 1;
									material = -1;
									name = "zbranVelitele";
									visual="Camo1";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.08;
									armorComponent="";
									isGun=1;

								};
								
							};
							
						};
						
						class CargoTurret_Links: NewTurret //[0,1] //Luke_links
						{
							
							body = "";
							gun = "";
							animationSourceBody = "";
							animationSourceGun = "";
							animationSourceHatch = "Hatch_left_Source";
							enabledByAnimationSource = "Hatch_left_rot";
							proxyType= "CPGunner";
							proxyIndex = 4;
							gunnerName = "$STR_Dachluke_links";
							primaryGunner = 0;
							gunnerGetInAction="GetInLow";
							gunnerGetOutAction="GetOutLow";
							gunnerForceOptics = 0;
							hideWeaponsGunner = 0;
							inGunnerMayFire=0;
							viewGunnerInExternal = 1;
							gunnerCompartments= "Compartment2";
							lodTurnedIn = 1200; //Cargo
							lodTurnedOut = 1100; //Pilot
							startEngine = 0;
							dontCreateAi = 1;
							soundAttenuationTurret = "TankAttenuation";
							gunnerInAction = "Redd_Marder_Passenger";
							gunnerAction  = "vehicle_turnout_1";
							memoryPointsGetInGunner= "pos cargo";
							memoryPointsGetInGunnerDir= "pos cargo dir";
							isPersonTurret = 1;

							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};
							
							class TurnOut 
							{

								limitsArrayTop[] = 
								{

									{45, -78},
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{
									{-5, -78},
									{-20, 0},
									{-20, 47},
									{7, 57},
									{7, 100},
									{0, 110},
									{-5, 115},
									{-5, 120}
									
								};
							
							};
						
						};
						
						class CargoTurret_Rechts: CargoTurret_Links//[0,2] //Luke_rechts
						{
							
							animationSourceHatch = "Hatch_right_Source";
							enabledByAnimationSource = "Hatch_right_rot";
							proxyIndex = 5;
							gunnerName = "$STR_Dachluke_rechts";
							memoryPointsGetInGunner= "pos cargo";
							memoryPointsGetInGunnerDir= "pos cargo dir";
							
							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};

							class TurnOut 
							{

								limitsArrayTop[] = 
								{

									{45, 78},
									{45, -120}
									
								};

								limitsArrayBottom[] = 
								{
									{-5, 88},
									{-18, 55},
									{-20, 0},
									{-20, -25},
									{-5, -30},
									{7, -35},
									{7, -70},
									{-5, -95},
									{-5, -120}
									
								};
							
							};
							
						};

						class CargoTurret_Hinten: CargoTurret_Links //[0,3] //Luke_hinten
						{
							
							animationSourceHatch = "Hatch_rear_Source";
							enabledByAnimationSource = "Hatch_rear_rot";
							proxyIndex = 6;
							gunnerName = "$STR_Dachluke_hinten";
							gunnerInAction = "Redd_Marder_Passenger_Back";
							memoryPointsGetInGunner= "pos cargo";
							memoryPointsGetInGunnerDir= "pos cargo dir";
							
							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							}; 

							class TurnOut 
							{

								limitsArrayTop[] = 
								{

									{45, 100},
									{45, -80}
									
								};

								limitsArrayBottom[] = 
								{
									{-7,100},
									{-7,70},
									{-20,60},
									{-20,0},
									{-20,-40},
									{-7,-50},
									{-7,-80}
									
								};
							
							};
						};

					};
					
					//Mainturret [0]
					weapons[] =
					{
						
						"Redd_Gesichert",
						"Redd_MK20",
						"Redd_MG3"

					};

					magazines[] =
					{
						
						"Redd_MK20_HE_Mag",
						"Redd_MK20_AP_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag"

					};
					
					startEngine = 0;
					stabilizedInAxes = 0;
					forceHideGunner = 1;
					initElev = 0;
					minElev = -13;
					maxElev = 65;
					initTurn = 0;
					minTurn = -360;
					maxTurn = 360;
					maxHorizontalRotSpeed = 1;
					maxVerticalRotSpeed = 1;
					gunnerForceOptics=0;
					gunnerInAction="Redd_Marder_Gunner";
					gunnerAction="Redd_Marder_Gunner";
					proxyIndex = 2;
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetInAction="GetInLow";
					gunnerGetOutAction="GetOutLow";
					turretInfoType = "Redd_RCS_Turret";
					discreteDistance[] = {200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000};
					discreteDistanceInitIndex = 3;
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					soundAttenuationTurret = "TankAttenuation";
					gunnerCompartments= "Compartment2";
					lockWhenDriverOut = 1;
					ace_fcs_Enabled = 0;
					animationSourceStickX="turret_control_x";
					animationSourceStickY="turret_control_y";
					gunnerLeftHandAnimName="turret_control";
					gunnerRightHandAnimName="turret_control";
					
					soundServo[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner",
						0.56234133,
						1,
						10
						
					};
					
					soundServoVertical[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner_vertical",
						0.56234133,
						1,
						30
						
					};

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {2,3};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
						
					class OpticsIn
					{

						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.23;
							maxFov = 0.23;
							minFov = 0.23;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.06;
							maxFov = 0.06;
							minFov = 0.06;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T2";
							gunnerOpticsEffect[] = {};
							
						};
						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.23;
							maxFov = 0.23;
							minFov = 0.23;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W1";
							gunnerOpticsEffect[] = {};
							
						};
						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.023;
							maxFov = 0.023;
							minFov = 0.023;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W2";
							gunnerOpticsEffect[] = {};
							
						};
						
					};
					
					class HitPoints 
					{
						
						class HitTurret	
						{
							
							armor = 1;
							material = -1;
							name = "vez";
							visual="Camo1";
							passThrough = 0;
							minimalHit = 0.02;
							explosionShielding = 0.3;
							radius = 0.08;
							armorComponent="";
							isTurret=1;
						};
						
						class HitGun	
						{
							
							armor = 1;
							material = -1;
							name = "zbran";
							visual="Camo1";
							passThrough = 0;
							minimalHit = 0;
							explosionShielding = 1;
							radius = 0.10;
							armorComponent="";
							isGun=1;
							
						};
					
					};
					
				};
				
				class Redd_Milan: NewTurret //[1]
				{
					
					body = "Milan_Rot_X";
					gun = "Milan_Rot_Y";
					animationSourceBody = "Milan_Rot_X";
					animationSourceGun = "Milan_Rot_Y";
					proxyindex = 3;
					gunnerName="Fake_Milan";
					primaryGunner = 0;
					primaryObserver = 1;
					weapons[] ={"Redd_Milan"};
					magazines[] = 
					{	
					
						"Redd_Milan_Mag",
						"Redd_Milan_Mag",
						"Redd_Milan_Mag",
						"Redd_Milan_Mag"
				
					};
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -15;
					maxTurn = 15;
					initTurn = 0;
					stabilizedInAxes = 3;
					gunnerGetOutAction = "GetOutHigh";
					turretInfoType = "Redd_RSC_Milan";
					gunnerForceOptics=0;
					canHideGunner = 0; //0 für GunnerOut 
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments= "Compartment4";
					//lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					startEngine = 0;
					gunnerRightHandAnimName = "Milan_Turret";
					gunnerLeftHandAnimName = "Milan_Gun";
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					//gunnerInAction="Redd_Marder_Commander_Milan";
					gunnerAction="Redd_Marder_Commander_Milan";
					//memoryPointGunnerOptics = "Milan_View";
					memoryPointGunnerOutOptics = "Milan_View";
					memoryPointsGetInGunner = "pos commander";
                    memoryPointsGetInGunnerDir = "pos commander dir";
					//lockWhenDriverOut = 0;
					ace_fcs_Enabled = 0;

					outGunnerMayFire = 1; //Rein für Gunner Out
					gunnerOutOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1"; //Rein für Gunner Out

					class OpticsOut //So addaptieren für ändern für Gunner Out
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 1.25;
							minFov = 0.25;
							visionMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};

					};

					class OpticsIn
					{

						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.15;
							maxFov = 0.15;
							minFov = 0.15;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.037;
							maxFov = 0.037;
							minFov = 0.037;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.15;
							maxFov = 0.15;
							minFov = 0.15;
							visionMode[] = {"Ti"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_W1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.037;
							maxFov = 0.037;
							minFov = 0.037;
							visionMode[] = {"Ti"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan_W1";
							gunnerOpticsEffect[] = {};
										
						};
						
					};
					
				};

				class Luchs_Bino_Turret_Com: NewTurret //[2]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 1;
					proxyType= "CPGunner";
					gunnerName="Bino_Com";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutHigh";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment5";
					LODTurnedIn = 1000; //Gunner
					LODTurnedOut= 1000; //Gunner
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="rnt_marder_com_out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="rnt_marder_com_out_ffv";
					memoryPointsGetInGunner = "pos commander";
                    memoryPointsGetInGunnerDir = "pos commander dir";

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};
				
			};
			
			class AnimationSources
			{

				class HatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class hatchCommander
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class Hatch_left_Source
				{
				
					source = "user";
					animPeriod = 2;
					initPhase = 0;
				
				};

				class Hatch_right_Source
				{
				
					source = "user";
					animPeriod = 2;
					initPhase = 0;
				
				};

				class heck_luke_rotation
				{
				
					source="door";
					initPhase=0;
					animPeriod=3;
					sound="Redd_Heckluke_sound";
				
				};
				class Hide_Knopf_Heck_luke
				{
				
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				class Hide_Milan_Source 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				class Milan_Hide_Rohr 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
				
				class Spiegel_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};
				
				class recoil_source_1 
				{
					
					source = "reload"; 
					weapon = "Redd_MK20";
					
				};
				
				class recoil_source_2
				{
					
					source = "reload"; 
					weapon = "Redd_MG3";
					
				};

				class Redd_Sandsacke_Links
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				class Redd_Sandsacke_Rechts
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Redd_sandsackrolle_links
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				class Redd_sandsackrolle_rechts
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class TarnLichtHinten_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class TarnLichtVorne_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class LichterHide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class ReloadMagazine
                {

                    source="reloadmagazine";
                    weapon = "Redd_Milan";
                    
                };

				//Rundumleuchte Hide
				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class Beacons
				{

					source = "user";
					animPeriod = 1;
					initPhase = 0;

				};
				class turn_left
            	{

					source = "user";
					animPeriod = 0.001;
					initPhase = 0;

           		};

				class orangelicht_an: turn_left{};

				class orangelicht_an_flare: turn_left{};

				class flash_mk_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MK20";
					initPhase = 0;
					
				};

				class flash_mg3_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MG3";
					initPhase = 0;
					
				};

				//Netze
				class marder_netz_boden_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class marder_netz_boden_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class marder_netz_boden_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class marder_netz_wanne_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class marder_netz_wanne_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class marder_netz_wanne_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

			};

			class UserActions
			{
				
				class heckluke_auf
				{
					
					displayName = "$STR_heckluke_auf";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this doorPhase 'heck_luke_rotation' == 0) and (alive this)"; 
					statement = "this animateDoor ['heck_luke_rotation', 1];this animate ['Hide_Knopf_Heck_luke', 0];";

				};
				
				class heckluke_zu
				{
					
					displayName = "$STR_heckluke_zu";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this doorPhase 'heck_luke_rotation' > 0) and (alive this)"; 
					statement = "this animateDoor ['heck_luke_rotation', 0];this animate ['Hide_Knopf_Heck_luke', 1];";
				
				};

				class heckluke_auf_2: heckluke_auf
				{
					
					condition = "(this turretUnit [0,3] == player) and (this doorPhase 'heck_luke_rotation' == 0) and (alive this)"; 
					
				};
				
				class heckluke_zu_2: heckluke_zu
				{
					
					condition = "(this turretUnit [0,3] == player) and (this doorPhase 'heck_luke_rotation' > 0) and (alive this)"; 
					
				};
				
				class milan_auf
				{
					
					displayName = "$STR_milan_auf";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationSourcePhase 'Hide_Milan_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['Hide_Milan_Source',0];this animate ['Milan_Hide_Rohr',0]";
					
				};

				class milan_ab
				{
					
					displayName = "$STR_milan_ab";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationSourcePhase 'Hide_Milan_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['Hide_Milan_Source',1];this animate ['Milan_Hide_Rohr',1];";
					
				};

				class milan_in
				{
					
					displayName = "$STR_milan_in";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationSourcePhase 'Hide_Milan_Source' == 0) and (alive this)"; 
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];"; 
					
				};

				class milan_aus
				{
					
					displayName = "$STR_milan_aus";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this);";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Marder_Commander_Up', false,true];";
					
				};

				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationSourcePhase 'hatchCommander' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [2]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Marder_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [2] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Marder_Bino_In', false, true];"; 
				
				};
				
				class WinkelsSpiegel_hoch
				{
					
					displayName = "$STR_Durch_Winkelspiegel_sehen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 0) and !(this getVariable 'Redd_Marder_Commander_Winkelspiegel') and (alive this)"; 
					statement = "[(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]),'Redd_Marder_Commander_Winkelspiegel'] remoteExecCall ['switchmove'];this setVariable ['Redd_Marder_Commander_Winkelspiegel', true,true];";
					
				};
				
				class WinkelsSpiegel_runter
				{
					
					displayName = "$STR_Wieder_hinsetzen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 0) and (this getVariable 'Redd_Marder_Commander_Winkelspiegel') and (alive this)"; 
					statement = "[(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]),'rnt_marder_com'] remoteExecCall ['switchmove'];this setVariable ['Redd_Marder_Commander_Winkelspiegel', false,true];";
					
				};

				class Spiegel_einklappen
				{
					
					displayName = "$STR_Spiegel_einklappen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 1];";
				
				};
				
				class Spiegel_ausklappen
				{
					
					displayName = "$STR_Spiegel_ausklappen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 0];";
				
				};
				
				class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};
				
				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};
				
				class Sandsacke_auf_Links
				{
					
					displayName = "$STR_Sandsaecke_aufbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Links' > 0) and (this turretUnit [0,1] == player) and (this animationSourcePhase 'Hatch_left_Source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Links',0]";
					
				};
				
				class Sandsacke_auf_Rechts
				{
					
					displayName = "$STR_Sandsaecke_aufbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Rechts' > 0) and (this turretUnit [0,2] == player) and (this animationSourcePhase 'Hatch_right_Source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Rechts',0]";
					
				};
				
				class Sandsacke_ab_Links
				{
					
					displayName = "$STR_Sandsaecke_abbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Links' == 0) and (this turretUnit [0,1] == player) and (this animationSourcePhase 'Hatch_left_Source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Links',1]";
					
				};
				
				class Sandsacke_ab_Rechts
				{
					
					displayName = "$STR_Sandsaecke_abbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Rechts' == 0) and (this turretUnit [0,2] == player) and (this animationSourcePhase 'Hatch_right_Source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Rechts',1]";
					
				};

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_montieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 1) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animateSource ['Hide_Rundumleuchte_fuss', 0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abmontieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animateSource ['Hide_Rundumleuchte_fuss', 1];";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_marder_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and !(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_marder_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and !(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_marder_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and !(this getVariable 'has_flag')";
					statement = "[this,3] call Redd_fnc_marder_flags";

				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_marder_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_marder_camonet";

				};

				class Tarnnetz_Boden_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_marder_camonet";

				};

				class Tarnnetz_Boden_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and (this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_marder_camonet";

				};
				
				class fixTurretBug
				{

					displayName = "Fix GetIn Bug"; 
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(alive this)";
					statement = "if (this turretUnit [1] isEqualTo objNull) then {[this,[[0,0],false]] remoteExecCall ['lockTurret']};";

				};

			};
			
			class EventHandlers: EventHandlers 
			{
				
				init = "_this call redd_fnc_Marder_Init";
				fired = "_this call redd_fnc_Marder_Fired";
				getOut = "_this call redd_fnc_Marder_GetOut";
				getIn = "_this call redd_fnc_Marder_GetIn";
				turnIn = "_this call redd_fnc_Marder_TurnIn";
				turnOut = "_this call redd_fnc_Marder_TurnOut";
				
			};
			
			class Attributes 
			{
				
				class Redd_Marder_1A5_Bataillon_Attribute 
				{
					
					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Marder_1A5_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_33 
						{
							
							name = "33";
							value = "1";
							
						};

						class Bataillon_92 
						{
							
							name = "92";
							value = "2";
							
						};

						class Bataillon_112  
						{
							
							name = "112";
							value = "3";
							
						};

						class Bataillon_122  
						{
							
							name = "122";
							value = "4";
							
						};
						
						class Bataillon_212  
						{
							
							name = "212";
							value = "5";
							
						};
						
						class Bataillon_371  
						{
							
							name = "371";
							value = "6";
							
						};
						
						class Bataillon_391  
						{
							
							name = "391";
							value = "7";
							
						};
						
						class Bataillon_401  
						{
							
							name = "401";
							value = "8";
							
						};
						
						class Bataillon_411  
						{
							
							name = "411";
							value = "9";
							
						};

						class Random
						{
							
							name = "$STR_Random";
							value = "10";
							
						};

					};
					
				};
				
				class Redd_Marder_1A5_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Marder_1A5_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

						class Random
						{
							
							name = "$STR_Random";
							value = "5";
							
						};
						
					};
					
				};
				
				class Redd_Marder_1A5_Zug_Buchstaben_Attribute 
				{
					
					displayName = "$STR_Zug_DisplayName";
					tooltip = "$STR_Zug_Buchstaben";
					property = "Redd_Marder_1A5_Zug_Buchstaben_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Zug_Buchstabe', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Alpha 
						{
							
							name = "Alpha";
							value = "1";
							
						};

						class Bravo 
						{
							
							name = "Bravo";
							value = "2";
							
						};

						class Charlie 
						{
							
							name = "Charlie";
							value = "3";
							
						};

						class Delta 
						{
							
							name = "Delta";
							value = "4";
							
						};

					};
		
				};
		
				class Redd_Marder_1A5_Fzg_Nummer_Attribute
				{
					
					displayName = "$STR_Fzg_Nummer_DisplayName";
					tooltip = "$STR_Fzg_Nummer";
					property = "Redd_Marder_1A5_Fzg_Nummer_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Fzg_Nummer', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Eins 
						{
							
							name = "1";
							value = "1";
							
						};

						class Zwei 
						{
							
							name = "2";
							value = "2";
							
						};

						class Drei 
						{
							
							name = "3";
							value = "3";
							
						};

						class Vier 
						{
							
							name = "4";
							value = "4";
							
						};

					};
					
				};
				
			};
			
		};
		
		/*	Public class Flecktarn	*/
		class Redd_Marder_1A5_Flecktarn: Redd_Marder_1A5_base
		{
			
			editorPreview="\Redd_Marder_1A5\pictures\Marder_F_Pre_Picture.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Marder_1A5_Flecktarn";

			hiddenSelectionsTextures[] = 
			{

				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_co.paa",
				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"
				
			};
			
			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_Marder_1A5_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_co.paa",
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_Marder_1A5_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Trope_co.paa",
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_Marder_1A5_Winter";
					author = "ReddNTank";
					
					textures[]=
					{

						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Winter_co.paa",
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};
			
		};
		
		/*	Public class Tropentarn	*/
		class Redd_Marder_1A5_Tropentarn: Redd_Marder_1A5_Flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Marder_1A5\pictures\Marder_D_Pre_Picture.paa";
			displayName="$STR_Marder_1A5_Tropentarn";

			hiddenSelectionsTextures[] = 
			{

				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Trope_co.paa",
				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"

			};
			
		};
		
		/*	Public class Wintertarn	*/
		class Redd_Marder_1A5_Wintertarn: Redd_Marder_1A5_Flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="\Redd_Marder_1A5\pictures\Marder_W_Pre_Picture.paa";
			displayName="$STR_Marder_1A5_Winter";

			hiddenSelectionsTextures[] = 
			{

				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Winter_co.paa",
				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"

			};
			
		};
		
	};