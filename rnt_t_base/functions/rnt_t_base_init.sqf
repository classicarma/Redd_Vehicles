

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Init for all Trucks
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_veh"];

[_veh,8] spawn redd_fnc_t_base_plate;//spawns function to randomise license plates

[_veh,[[0],true]] remoteExecCall ['lockTurret']; //Locks MG3 turret

_veh setVariable ['rnt_t_base_MG3_In', false, true];
_veh setVariable ['has_flag', false,true];//initiates varibale for flags