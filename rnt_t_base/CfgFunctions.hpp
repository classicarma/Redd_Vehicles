

class CfgFunctions {

    class Redd {
        tag = "Redd";

        class Functions {
            
            class t_base_init {
                file = "\rnt_t_base\functions\rnt_t_base_init.sqf";
            };

            class t_base_plate {
                file = "\rnt_t_base\functions\rnt_t_base_plate.sqf";
            };

            class t_base_move_to_mg3 {
                file = "\rnt_t_base\functions\rnt_t_base_move_to_mg3.sqf";
            };

            class t_base_getOut {
                file = "\rnt_t_base\functions\rnt_t_base_getOut.sqf";
            };

            class t_base_flags{
                file = "\rnt_t_base\functions\rnt_t_base_flags.sqf";
            };
        };
    };
};