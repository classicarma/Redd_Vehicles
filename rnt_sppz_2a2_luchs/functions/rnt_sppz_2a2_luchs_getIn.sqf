

	//Triggerd by BI eventhandler "getin"
	
	params ["_veh","_pos","_unit","_turret"];
	
	//check if commander gets in and mg3 is not in initial aimation phase
	if ((_turret isEqualTo [0,0]) and (_veh animationSourcePhase "mg3_hide_source" != 1)) then
	{	
		
		_veh animateSource ["mg3_hide_source",1];

		waitUntil {_veh animationSourcePhase "mg3_hide_source" == 1};

		_veh animateSource ["fake_mg3_hide_source",0];
		_veh animateSource ["fake_mg3_rot_source",0];

	};

	if (_veh getVariable 'has_camonet_large') then
	{

		_unit setUnitTrait ["camouflageCoef", 0.1];

	};

	if (_veh getVariable 'has_camonet') then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
		_camouflage_new = _camouflage/100*80;
		_unit setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

	};