

	//Sets random licenseplate

	if (!isServer) exitWith {};

	params ["_veh"];
	
	_ih = 18;
	_count = 0;
	_init = true;
	_num = 0;
	
	while {_count <= 5} do
	{
	
		if (_init) then {_num = selectRandom [1,2,3,4,5,6,7,8,9];_init = false;} //Initialarray without "0", so licenseplate doesnt start with "0" 
		else {_num = selectRandom [0,1,2,3,4,5,6,7,8,9];};
		
		_veh setObjectTextureGlobal [_ih, format["\Redd_Vehicles_Main\data\Redd_Plate_%1_ca.paa",_num]];
		
		_ih = _ih + 1; //increments the index of the hiddenselection
		_count = _count + 1;
		
	};
	
	