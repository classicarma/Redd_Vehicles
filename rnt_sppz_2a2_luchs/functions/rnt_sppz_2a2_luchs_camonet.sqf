

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Sets camonet
	//			 
	//	Example: ;
	//				 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  1: STRING - Camonet, Camonet_large
	//				  2: OBJECT - Player
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_vehicle","_type","_unit"];
	
	_fuelEventhandler = 0; 

	if (_type isEqualTo "camonet") then
	{

		if !(_vehicle getVariable 'has_camonet') then
		{

			_vehicle setVariable ['has_camonet', true,true];

			_vehicle_class = typeOf _vehicle;

			switch (_vehicle_class) do
			{
				
				case "rnt_sppz_2a2_luchs_flecktarn":
				{

					_vehicle animateSource ["netzWanne_hide_Source",0];

				};

				case "rnt_sppz_2a2_luchs_tropentarn": 
				{

					_vehicle animateSource ["netzWanne_d_hide_Source",0];

				};

				case "rnt_sppz_2a2_luchs_wintertarn":
				{

					_vehicle animateSource ["netzWanne_w_hide_Source",0];

				};

			};

			{

				_unit_class = typeOf _x;
				_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
				_camouflage_new = _camouflage/100*80;
				_x setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

			}
			forEach crew _vehicle;

		}
		else
		{

			_vehicle setVariable ['has_camonet', false,true];

			_vehicle_class = typeOf _vehicle;

			switch (_vehicle_class) do
			{
				
				case "rnt_sppz_2a2_luchs_flecktarn":
				{

					_vehicle animateSource ["netzWanne_hide_Source",1];

				};

				case "rnt_sppz_2a2_luchs_tropentarn": 
				{

					_vehicle animateSource ["netzWanne_d_hide_Source",1];

				};

				case "rnt_sppz_2a2_luchs_wintertarn":
				{

					_vehicle animateSource ["netzWanne_w_hide_Source",1];

				};

			};
			
			{

				_unit_class = typeOf _x;
				_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
				_x setUnitTrait ["camouflageCoef", _camouflage];

			}
			forEach crew _vehicle;

		};

	};

	if (_type isEqualTo "camonet_large") then
	{

		if !(_vehicle getVariable 'has_camonet_large') then
		{

			_vehicle setVariable ['has_camonet_large', true,true];

			_vehicle_class = typeOf _vehicle;

			switch (_vehicle_class) do
			{
				
				case "rnt_sppz_2a2_luchs_flecktarn":
				{

					_vehicle animateSource ["netzBoden_hide_Source",0];

				};

				case "rnt_sppz_2a2_luchs_tropentarn": 
				{

					_vehicle animateSource ["netzBoden_d_hide_Source",0];

				};

				case "rnt_sppz_2a2_luchs_wintertarn":
				{

					_vehicle animateSource ["netzBoden_w_hide_Source",0];

				};

			};

			{

				_x setUnitTrait ["camouflageCoef", 0.1];

			}
			forEach crew _vehicle;

		}
		else
		{
			_vehicle setVariable ['has_camonet_large', false,true];

			_vehicle_class = typeOf _vehicle;

			switch (_vehicle_class) do
			{
				
				case "rnt_sppz_2a2_luchs_flecktarn":
				{

					_vehicle animateSource ["netzBoden_hide_Source",1];

				};

				case "rnt_sppz_2a2_luchs_tropentarn": 
				{

					_vehicle animateSource ["netzBoden_d_hide_Source",1];

				};

				case "rnt_sppz_2a2_luchs_wintertarn":
				{

					_vehicle animateSource ["netzBoden_w_hide_Source",1];

				};

			};
			
			{

				_unit_class = typeOf _x;
				_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
				_x setUnitTrait ["camouflageCoef", _camouflage];

			}
			forEach crew _vehicle;

		};

	};

	true