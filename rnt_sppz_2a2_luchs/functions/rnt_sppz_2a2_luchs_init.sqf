
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Init for Luchs
	//			 
	//	Example:
	//						 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_veh"];

	[_veh] spawn redd_fnc_Luchs_Plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_Luchs_Bat_Komp;//spawns function to set battalion and company numbers

	[_veh,[[2],true]] remoteExecCall ['lockTurret']; //Locks MG3 turret
	[_veh,[[3],true]] remoteExecCall ['lockTurret']; //Locks Bino turret

	_veh setVariable ['Redd_Luchs_MG3_In', false, true];
	_veh setVariable ['Redd_Luchs_Bino_In', false, true];
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet
	_veh setVariable ['has_camonet_large', false,true];//initiates varibale for large camonet
	_veh setVariable ['has_flag', false,true];//initiates variable for flags
