

    class CfgPatches
	{
		
		class rnt_sppz_2a2_luchs
		{
			
			units[] = 
			{
				
				"rnt_sppz_2a2_luchs_flecktarn",
				"rnt_sppz_2a2_luchs_tropentarn",
				"rnt_sppz_2a2_luchs_wintertarn"

			};
			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"A3_Soft_F","Redd_Vehicles_Main"};

		};
		
	};