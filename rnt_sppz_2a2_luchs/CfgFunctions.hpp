
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class luchs_Init
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_init.sqf";

                };

                class luchs_w_Init
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_w_init.sqf";

                };

                class luchs_flags
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_flags.sqf";

                };

                class luchs_move_to_mg3
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_move_to_mg3.sqf";

                };

                class luchs_GetOut
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_getOut.sqf";

                };

                class luchs_GetIn
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_getIn.sqf";

                };

                class luchs_Plate
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_plate.sqf";

                };

                class luchs_Bat_Komp
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_bat_komp.sqf";

                };

                class luchs_fired
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_fired.sqf";

                };

                class luchs_camonet
                {

                    file = "\rnt_sppz_2a2_luchs\functions\rnt_sppz_2a2_luchs_camonet.sqf";

                };
                
            };

        };

    };