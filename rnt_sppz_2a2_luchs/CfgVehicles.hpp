    

    class CfgVehicles 
    {

        class Car;

        class Car_F: Car
	    {

            class NewTurret;
            class Sounds;

            class HitPoints
            {

                class HitBody;
                class HitEngine;
                class HitFuel;
                class HitHull;

                class HitLFWheel;
				class HitLF2Wheel;
				class HitLMWheel;
                class HitLBWheel;

                class HitRFWheel;
				class HitRF2Wheel;
                class HitRMWheel;
				class HitRBWheel;
				
            };

	    };

        class Wheeled_APC_F: Car_F
	    {
            class ViewOptics;
            class ViewCargo;

            class Sounds: Sounds
            {

                class Engine;
                class Movement;

            };

            class NewTurret;

            class Turrets
            {

                class MainTurret: NewTurret
                {

                    class ViewOptics;
                    class Turrets
                    {

                        class CommanderOptics;

                    };

                };

            };

            class AnimationSources;
            class EventHandlers;

        };

        class rnt_sppz_2a2_luchs_Base: Wheeled_APC_F
	    {
            
            #include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
            displayName = "$STR_sppz_2a2_luchs";
			icon="\A3\armor_f_beta\APC_Wheeled_01\Data\UI\map_amw_CA.paa";
			picture="\A3\armor_f_beta\APC_Wheeled_01\Data\UI\APC_Wheeled_01_ACRV_ca.paa";
			side = 1;
			crew = "B_crew_F";
            author = "ReddnTank";
            model = "\rnt_sppz_2a2_luchs\rnt_sppz_2a2_luchs";
            editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_SpPz";
            smokeLauncherGrenadeCount = 8;
			smokeLauncherVelocity = 14;
			smokeLauncherOnTurret = 1;
			smokeLauncherAngle = 142.5;
            getInAction = "GetInHigh";
		    getOutAction = "GetOutHigh"; 
            driverAction = "rnt_luchs_driver_out";
            driverInAction="rnt_luchs_driver";
            armor = 200;
            armorStructural = 5;
           	cost = 1000000;
			dustFrontLeftPos = "TrackFLL";
			dustFrontRightPos = "TrackFRR";
			dustBackLeftPos = "TrackBLL";
			dustBackRightPos = "TrackBRR";
            viewDriverInExternal = 1;
			LODDriverTurnedOut=1201;//Cargo 1
			LODDriverTurnedin=1100;//Pilot
            enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos driver";
			driverWeaponsInfoType="Redd_RCS_Driver_APC";
			headGforceLeaningFactor[]={0.00075,0,0.0075};

			forceHideDriver = 0;
            attenuationEffectType = "TankAttenuation";
		    crewCrashProtection = 0.25;
            weapons[] = {"TruckHorn"};
			destrType = "DestructWreck";
			wheelDamageThreshold = 1;
        	wheelDestroyThreshold = 0.99;
        	wheelDamageRadiusCoef = 1;
        	wheelDestroyRadiusCoef = 0.5;
			unloadInCombat = 0;
			typicalCargo[] = {"B_crew_F"};
			transportSoldier = 0;
			viewCargoInExternal = 1;
			memoryPointTrackFLL = "TrackFLL";
		    memoryPointTrackFLR = "TrackFLR";
		    memoryPointTrackBLL = "TrackBLL";
		    memoryPointTrackBLR = "TrackBLR";

		    memoryPointTrackFRL = "TrackFRL";
		    memoryPointTrackFRR = "TrackFRR";
		    memoryPointTrackBRL = "TrackBRL";
		    memoryPointTrackBRR = "TrackBRR";

			engineEffectSpeed=5;
			memoryPointsLeftEngineEffect="EngineEffectL";
			memoryPointsRightEngineEffect="EngineEffectR";

			threat[]={0.9,0.6,0.6};
            audible = 1;
			camouflage = 2;

			TFAR_AdditionalLR_Turret[] = {{1},{2},{3}};

			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"Turret",{1}},{"Turret",{2}},{"Turret",{3}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"Turret",{1}},{"Turret",{2}},{"Turret",{3}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "commander", "gunner",{"Turret",{1}},{"Turret",{2}},{"Turret",{3}}};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 0;
			//Ende ACRE2

            hiddenSelections[] = 
            {
                
				"schild_kleint",//0
				"dach_kleint",//1
				"dach_teile_schrauben",//2
				"rest_teile",//3
				"rest_fahrwerk",//4
				"wanne",//5
				"lafette_bf_kabel",//6
				"fahrer_funker",//7
				"instr",//8
				"turm_instr",//9
				"turm_innen",//10
				"mk_wspiegel",//11
				"innen_wanne",//12
				"turm_luken",//13
				"reifen_fahrwerk",//14
				"lafette_bf_kabel_2",//15

                "kompanie",//16
				"bataillon",//17

				"plate_1",//18
				"plate_2",
				"plate_3",
				"plate_4",
				"plate_5",
				"plate_6"
				
            };
			
            class PlayerSteeringCoefficients
		    {

			    turnIncreaseConst = 0.1;
			    turnIncreaseLinear = 1;
			    turnIncreaseTime = 1;

			    turnDecreaseConst = 1;
			    turnDecreaseLinear = 1; 
			    turnDecreaseTime = 0;

			    maxTurnHundred = 0.7;
				
            };

			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"rnt_sppz_2a2_luchs\mats\rnt_luchs_rest_fahrwerk.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_rest_fahrwerk_damage.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_rest_fahrwerk_destruct.rvmat",

					"rnt_sppz_2a2_luchs\mats\rnt_luchs_turm_luken.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_turm_luken_damage.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_turm_luken_destruct.rvmat",

					"rnt_sppz_2a2_luchs\mats\rnt_luchs_wanne.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_wanne_damage.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_wanne_destruct.rvmat",

					"rnt_sppz_2a2_luchs\mats\rnt_luchs_reifen_fahrwerk.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_reifen_fahrwerk.rvmat",
					"rnt_sppz_2a2_luchs\mats\rnt_luchs_reifen_fahrwerk.rvmat"
					
				};
				
        	};

            class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};

            class Exhausts
		    {

			    class Exhaust1
			    {

				    position = "exhaust1_pos"; 
				    direction = "exhaust1_dir";
				    effect = "ExhaustsEffect";

			    };

				class Exhaust2
			    {

				    position = "exhaust2_pos"; 
				    direction = "exhaust2_dir";
				    effect = "ExhaustsEffect";

			    };

		    };

            class Reflectors 
			{

				class Left 
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
					intensity = 1;
					useFlare = 0;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Left_2
                {

                    color[] = {2500, 1800, 1700};
					ambient[] = {0.25, 0.25, 0.25};
                    position = "Light_L_2";
					direction = "Light_L_end_2";
					hitpoint = "Light_L";
					selection = "Light_L_2";
                    size = 0.5;
                    innerAngle = 0;
					outerAngle = 95;
					coneFadeCoef = 1;
                    intensity = 0.05;
                    useFlare = 0;
                    dayLight = 1;
                    flareSize = 0;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 5;
                        hardLimitEnd = 10;
                   
                    };

                };

                class Right_2: Left_2
			    {

                    position = "Light_R_2";
					direction = "Light_R_end_2";
					hitpoint = "Light_R";
					selection = "Light_R_2";

			    };
				
				//Flare für Abblendlicht
				class Left_3
				{
					
					color[] = {2500, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L_3";
					direction = "Light_L_end_3";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 1;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right_3: Left_3 
				{
					
					position = "Light_R_3";
					direction = "Light_R_end_3";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

				class Orangelight
				{

					color[]={236, 99, 17};
					ambient[]={10,10,10};
					position="pos_orangelicht";
					direction="dir_orangelicht";
					hitpoint="";
					selection="pos_orangelicht";
					size=2;
					innerAngle=0;
					outerAngle=100;
					coneFadeCoef=1;
					intensity=250;
					useFlare=0;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=200;
						hardLimitEnd=300;

					};

				};

				class Orangelight_flare
				{

					color[]={236, 99, 17};
					ambient[]={0,0,0};
					position="pos_orangelicht_flare";
					direction="dir_orangelicht_flare";
					hitpoint="";
					selection="pos_orangelicht_flare";
					size=2;
					innerAngle=0;
					outerAngle=150;
					coneFadeCoef=1;
					intensity=250;
					useFlare=1;
					dayLight=1;
					flareSize=5;

					class Attenuation
					{

						start=1;
						constant=0;
						linear=0;
						quadratic=0.25;
						hardLimitStart=0.1;
						hardLimitEnd=0.1;

					};

				};

			};

            class HitPoints: HitPoints
            {
				
                class HitEngine: HitEngine
                {

                    armor = 0.5;
                    material = -1;
                    name = "engine";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 0.2;
                    radius = 0.3;
					armorComponent="";

                };

                class HitFuel: HitFuel
                {

                    armor = 0.5;
                    material = -1;
                    name = "fuel";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 0.5;
                    radius = 0.3;
					armorComponent="";

                };

                class HitHull: HitHull
                {

                    armor = 2.0;
                    material = -1;
                    name = "karoserie";
                    visual = "damage_visual";
                    passThrough = 0;
                    minimalHit = 0.1;
                    explosionShielding = 1.5;
                    radius = 0.3;
					armorComponent="";

                };

                class HitLFWheel: HitLFWheel
                {

                    armor = 1;
                    name = "wheel_1_1_steering";
					visual = "damage_LF_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

				class HitLF2Wheel: HitLF2Wheel
                {

                    armor = 1;
                    name = "wheel_1_2_steering";
					visual = "damage_LF2_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

                class HitLMWheel: HitLMWheel
                {

                    armor = 1;
                    name = "wheel_1_3_steering";
					visual = "damage_LM_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;
                };

				class HitLBWheel: HitLBWheel
                {

                    armor = 1;
                    name = "wheel_1_4_steering";
					visual = "damage_LB_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;
                };

				class HitRFWheel: HitRFWheel
                {

                    armor = 1;
                    name = "wheel_2_1_steering";
					visual = "damage_RF_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

				class HitRF2Wheel: HitRF2Wheel
                {

                    armor = 1;
                    name = "wheel_2_2_steering";
					visual = "damage_RF2_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };
                
                class HitRMWheel: HitRMWheel
                {

                    armor = 1;
                    name = "wheel_2_3_steering";
					visual = "damage_RM_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

				class HitRBWheel: HitRMWheel
                {

                    armor = 1;
                    name = "wheel_2_4_steering";
					visual = "damage_RB_visual";
                    minimalHit = 0.02;
                    explosionShielding = 2;
                    radius = 0.15;

                };

            };

			class TransportMagazines {};

			class TransportWeapons {};

            class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

			};

            class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};

            class MFD 
		    {

			    class ClockHUD
			    {

			        #include "cfgHUD.hpp"

			    };
		    };
            
            class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret //[0] 
				{

                    class Turrets: Turrets 
					{

                        class CommanderOptics: CommanderOptics //[0,0] Commander
						{
						
							gunnerForceOptics=0;
							gunnerAction = "rnt_luchs_com_out";
							gunnerInAction="rnt_luchs_com";
							proxyIndex = 1;
							initElev = 0;
							minElev = -15;
							maxElev = 15;
							initTurn = 0;
							minTurn = -80;
							maxTurn = 80;
							viewGunnerInExternal = 1;
							hideProxyInCombat = 1;
							proxyType= "CPCommander";
							startEngine = 0;
							gunnerGetInAction="GetInHigh";
							gunnerGetOutAction="GetOutHigh";
							turretInfoType = "Redd_RCS_Turret";
							stabilizedInAxes = 3;
							lodTurnedIn = 1000; //Gunner
							lodTurnedOut = 1202; //Cargo 2
							soundAttenuationTurret = "TankAttenuation";
							gunnerCompartments= "Compartment1";
							weapons[] = {"Redd_SmokeLauncher"};
							magazines[] = {"Redd_SmokeLauncherMag"};
							ace_fcs_Enabled = 0;
							gunnerOutOpticsModel="";
							outGunnerMayFire=0;
							soundServo[]={};
							soundServoVertical[]={};
							animationSourceStickX="com_turret_control_x";
							animationSourceStickY="com_turret_control_y";
							gunnerLeftHandAnimName="com_turret_control";
							gunnerRightHandAnimName="com_turret_control";
							memoryPointsGetInGunner = "pos gunner";
                            memoryPointsGetInGunnerDir = "pos gunner dir";

							class ViewOptics: ViewOptics
							{
								
								visionMode[] = {"Normal", "NVG", "TI"};
								thermalMode[] = {2,3};
								initFov = 0.4;
								minFov = 0.08;
								maxFov = 0.03;
								
							};

							class TurnIn
							{

								limitsArrayTop[]=
								{

									{11, -43},
									{11, 43}
									
								};

								limitsArrayBottom[]=
								{
									
									{-11, -43},
									{-11, 43}

								};
							
							};

							class TurnOut
							{

								limitsArrayTop[]=
								{

									{15, -90},
									{15, 90}
									
								};

								limitsArrayBottom[]=
								{
									
									{-15, -90},
									{-15, 90}

								};
							
							};
							
							class OpticsIn
							{

								class Day1
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.23;
									maxFov = 0.23;
									minFov = 0.23;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T1";
									gunnerOpticsEffect[] = {};
												
								};
								
								class Day2
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.06;
									maxFov = 0.06;
									minFov = 0.06;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T2";
									gunnerOpticsEffect[] = {};
									
								};
								class WBG1
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.23;
									maxFov = 0.23;
									minFov = 0.23;
									visionMode[] = {"Ti"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W1";
									gunnerOpticsEffect[] = {};
									
								};
								class WBG2
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.023;
									maxFov = 0.023;
									minFov = 0.023;
									visionMode[] = {"Ti"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W2";
									gunnerOpticsEffect[] = {};
									
								};
								
							};

							class HitPoints 
							{
								
								class HitTurret	
								{
									
									armor = 1;
									material = -1;
									name = "";
									visual="Camo1";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.08;
									armorComponent="";
									isTurret=1;

								};
								
								class HitGun	
								{
									
									armor = 1;
									material = -1;
									name = "";
									visual="Camo1";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.08;
									armorComponent="";
									isGun=1;

								};
								
							};
							
						};
                        
                    };

                    //Mainturret [0]
					weapons[] =
					{
						
						"Redd_Gesichert",
						"Redd_MK20"

					};

					magazines[] =
					{
						
						"Redd_MK20_HE_Mag275",
						"Redd_MK20_AP_Mag100"

					};
					
					startEngine = 0;
					stabilizedInAxes = 0;
					forceHideGunner = 0;
					initElev = 0;
					minElev = -13;
					maxElev = 28;
					initTurn = 0;
					minTurn = -180;
					maxTurn = 180;
					maxHorizontalRotSpeed = 1;
					maxVerticalRotSpeed = 1;
					gunnerForceOptics=0;
					gunnerInAction="rnt_luchs_gun";
					gunnerAction="rnt_luchs_gun_out";
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetInAction="GetInLow";
					gunnerGetOutAction="GetOutLow";
					turretInfoType = "Redd_RCS_Turret";
					discreteDistance[] = {200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000};
					discreteDistanceInitIndex = 3;
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1202; //Cargo 2
					soundAttenuationTurret = "TankAttenuation";
					gunnerCompartments= "Compartment1";
					lockWhenDriverOut = 1;
					ace_fcs_Enabled = 0;
					animationSourceStickX="turret_control_x";
					animationSourceStickY="turret_control_y";
					gunnerLeftHandAnimName="turret_control";
					gunnerRightHandAnimName="turret_control";
					
					soundServo[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner",
						0.56234133,
						1,
						10
						
					};
					
					soundServoVertical[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner_vertical",
						0.56234133,
						1,
						30
						
					};

					class TurnIn
					{

						limitsArrayTop[]=
						{

							{28, -180},
							{28, 180}
							
						};

						limitsArrayBottom[]=
						{
							
							{-2, -180},
							{-2, -140},
							{-13, -100},
							{-13,0},
							{-13, 100},
							{-2, 140},
							{-2, 180}

						};
					
					};
					
					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -90},
							{15, 90}
							
						};

						limitsArrayBottom[]=
						{
							
							{-15, -90},
							{-15, 90}

						};
					
					};

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {2,3};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
						
					class OpticsIn
					{

						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.23;
							maxFov = 0.23;
							minFov = 0.23;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T1";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.06;
							maxFov = 0.06;
							minFov = 0.06;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_T2";
							gunnerOpticsEffect[] = {};
							
						};
						class WBG1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.23;
							maxFov = 0.23;
							minFov = 0.23;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W1";
							gunnerOpticsEffect[] = {};
							
						};
						class WBG2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.023;
							maxFov = 0.023;
							minFov = 0.023;
							visionMode[] = {"Ti"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20_W2";
							gunnerOpticsEffect[] = {};
							
						};
						
					};
					
					class HitPoints 
					{
						
						class HitTurret	
						{
							
							armor = 1;
							material = -1;
							name = "MainTurret";
							visual="damage_visual";
							passThrough = 0;
							minimalHit = 0.02;
							explosionShielding = 0.3;
							radius = 0.2;
							armorComponent="";
							isTurret=1;
						};
						
						class HitGun	
						{
							
							armor = 1;
							material = -1;
							name = "mainGun";
							visual="damage_visual";
							passThrough = 0;
							minimalHit = 0;
							explosionShielding = 1;
							radius = 0.03;
							armorComponent="";
							isGun=1;
							
						};
					
					};
					
				};
				class Luchs_Reardriver_Turret: NewTurret //[1]
				{

					body = "driver_2_turret";
					gun = "driver_2_gun";
					animationSourceBody = "driver_2_turret_source";
					animationSourceGun = "driver_2_gun_source";
					animationSourceHatch = "hatchDriver_2";
                    enabledByAnimationSource = "driver_hatch_2";
					proxyindex = 2;
					gunnerName="$STR_R_Driver";
					primaryGunner = 0;
					primaryObserver = 0;
					weapons[] ={};
					magazines[] = {};
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -180;
					maxTurn = 180;
					initTurn = 180;
					stabilizedInAxes = 3;
					gunnerGetOutAction = "GetOutMedium";
					turretInfoType = "Redd_RCS_Driver_APC";
					gunnerForceOptics=0;
					forceHideGunner = 0;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment1";
					LODTurnedIn = 1100; //Pilot 1100
					LODTurnedOut= 1203; //Cargo 3
					startEngine = 0;
					gunnerInAction="rnt_luchs_driver_2";
					gunnerAction="rnt_luchs_driver_2_out";
					memoryPointGunnerOptics= "driverview_2";
					memoryPointsGetInGunner = "pos driver_2";
                    memoryPointsGetInGunnerDir = "pos driver dir_2";

					class OpticsIn
					{
						
						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 0.75;
							minFov = 0.75;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {4,5};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
							gunnerOpticsEffect[] = {};
										
						};
						
					};

					class TurnIn
					{

						limitsArrayTop[]=
						{

							{0, -180}

						};

						limitsArrayBottom[]=
						{

							{0, -180}

						};
					
					};

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};
					
				};
				class Luchs_MG3_Turret: NewTurret //[2]
				{

					body = "MainTurret_mg";
					gun = "mainGun_mg";
					animationSourceBody = "MainTurret_mg";
					animationSourceGun = "mainGun_mg";
					proxyindex = 3;
					gunnerName="MG3";
					primaryGunner = 0;
					primaryObserver = 1;
					weapons[] ={"Redd_MG3"};
					magazines[] = 
					{	
					
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120",
						"Redd_Mg3_Mag_120"
				
					};
					initElev = 0;
					minElev = -20;
					maxElev = 50;
					initTurn = 0;
					minTurn = -360;
					maxTurn = 360;
					stabilizedInAxes = 3;
					gunnerGetOutAction = "GetOutMedium";
					turretInfoType="Redd_RSC_MG3";
                    discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
					discreteDistanceInitIndex = 4;
					gunnerForceOptics=0;
					canHideGunner = 0; //0 für GunnerOut 
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment2";
					//lodTurnedIn = 1202; //Cargo 2 //Raus für Gunner Out
					lodTurnedOut = 1202; //Cargo 2
					startEngine = 0;	
					gunnerRightHandAnimName = "RecoilHlaven_mg";
					gunnerLeftHandAnimName = "mainGun_mg";
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					//gunnerInAction="rnt_luchs_com_out_mg"; //Raus für Gunner Out
					gunnerAction="rnt_luchs_com_out_mg";
					//memoryPointGunnerOptics= "gunnerview2"; //Raus für Gunner Out
					memoryPointGunnerOutOptics= "gunnerview2"; //Rein für Gunner Out
					memoryPointsGetInGunner = "pos gunner";
                    memoryPointsGetInGunnerDir = "pos gunner dir";
					ace_fcs_Enabled = 0;

					//Neu
					outGunnerMayFire = 1; //Rein für Gunner Out
					gunnerOutOpticsModel = "\A3\weapons_f\reticle\optics_empty"; //Rein für Gunner Out
					
					class OpticsOut //So addaptieren für ändern für Gunner Out
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 1.25;
							minFov = 0.25;
							visionMode[] = {};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};

					};

					class OpticsIn //So addaptieren bei den änderen für Gunner Out
					{

						class Wide
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.4;
							maxFov = 0.4;
							minFov = 0.4;
							visionMode[] = {"Normal", "NVG"};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Narrow
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.2;
							maxFov = 0.2;
							minFov = 0.2;
							visionMode[] = {"Normal", "NVG"};
							gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
							gunnerOpticsEffect[] = {};

						};

					};

					class TurnIn //Bestimmt wie vorher den blickumkreis
					{

						limitsArrayTop[]=
						{

							{50, -180},
							{50, 180}
							
						};

						limitsArrayBottom[]=
						{

							{-15, -180},
							{-8, -30},
							{-8, 0},
							{-8, 30},
							{-15, 180}

						};
					
					};
					
				};

				class Luchs_Bino_Turret_Com: NewTurret //[3]
				{
					body = "";
					gun = "";
					animationSourceBody = "";
					animationSourceGun = "";
					animationSourceHatch = "";
					animationSourceCamElev = "";
					proxyindex = 1;
					proxyType= "CPCommander";
					gunnerName="Bino_Com";
					primaryGunner = 0;
					primaryObserver = 1;
					minElev = -15;
					maxElev = 15;
					initElev = 0;
					minTurn = -80;
					maxTurn = 80;
					initTurn = 0;
					gunnerGetOutAction = "GetOutMedium";
					gunnerForceOptics=0;
					canHideGunner = 0;
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerCompartments = "Compartment3";
					LODTurnedIn = 1202; //Cargo 2
					LODTurnedOut= 1202; //Cargo 2
					startEngine = 0;
					dontCreateAi = 1;
					disableSoundAttenuation = 1;
					gunnerInAction="rnt_luchs_com_out_high";
					memoryPointGunnerOptics= "";
					isPersonTurret=1;
					personTurretAction="rnt_luchs_com_out_ffv";

					class TurnOut
					{

						limitsArrayTop[]=
						{

							{15, -80},
							{15, 80}

						};

						limitsArrayBottom[]=
						{

							{-15, -80},
							{-15, 80}

						};
					
					};

				};

            };

            class AnimationSources
		    {

				class hatchCommander
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class hatchGunner
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class hatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class hatchDriver_2
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class side_door_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

				class recoil_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MK20";
					
				};

				class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};

				class LichterHide_2_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Spiegel_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};

				//Rundumleuchte Hide
				class Hide_Rundumleuchte_fuss
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_fuss_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_glass_2
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class Hide_Rundumleuchte_rot
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				class Beacons
				{

					source = "user";
					animPeriod = 1;
					initPhase = 0;

				};
				class turn_left
            	{

					source = "user";
					animPeriod = 0.001;
					initPhase = 0;

           		};

				class orangelicht_an: turn_left{};

				class orangelicht_an_flare: turn_left{};

				class mk20_muzzle_source 
				{
					
					source = "reload"; 
					weapon = "Redd_mk20";
					
				};

				class Gurt_Deckel_source
				{
					 
					source = "user";
					initPhase = 0;
					animPeriod = 0.1;
					
				};

				class water_shield_Source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

				class fake_mg3_rot_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

				class fake_mg3_hide_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 0;

                };

				class mg3_hide_source
                {

                    source = user;
                    initPhase = 1;
                    animPeriod = 0;

                };

				class ReloadAnim
                {

                    source="reload";
                    weapon = "Redd_MG3";

                };

			    class ReloadMagazine
                {

                    source="reloadmagazine";
                    weapon = "Redd_MG3";
                    
                };

				class flash_mg3_source 
				{
					
					source = "reload"; 
					weapon = "Redd_MG3";
					initPhase = 0;
					
				};

				class rot_door_source
                {

                    source = user;
                    initPhase = 0;
                    animPeriod = 2;

                };

				class HitLFWheel
				{

					source = "Hit";
					hitpoint = "HitLFWheel";
					raw = 1;
				};
				class HitLF2Wheel: HitLFWheel{hitpoint = "HitLF2Wheel";};
				class HitLMWheel: HitLFWheel{hitpoint = "HitLMWheel";};
				class HitLBWheel: HitLFWheel{hitpoint = "HitLBWheel";};

				class HitRFWheel: HitLFWheel{hitpoint = "HitRFWheel";};
				class HitRF2Wheel: HitLFWheel{hitpoint = "HitRF2Wheel";};
				class HitRMWheel: HitLFWheel{hitpoint = "HitRMWheel";};
				class HitRBWheel: HitLFWheel{hitpoint = "HitRBWheel";};

				
				class netzBoden_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class netzBoden_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class netzBoden_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class netzWanne_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class netzWanne_d_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class netzWanne_w_hide_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

			};
			
            class UserActions
		    {
				
				class water_shield_Open
                {

                    displayName = "$STR_Schwallbrett_ausklappen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'water_shield_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['water_shield_Source', 1];";

                };

                class water_shield_Close
                {

                    displayName = "$STR_Schwallbrett_einklappen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationSourcePhase 'water_shield_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['water_shield_Source', 0];";

                };

				class Redd_removeflag
				{

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_luchs_flags";

				};

				class Redd_redFlag
				{

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_luchs_flags";

				};

				class Redd_greenFlag
				{

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_luchs_flags";

				};

				class Redd_blueFlag
				{

					displayName = "$STR_Redd_blue_flagge_anbringen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_flag')";
					statement = "[this,3] call Redd_fnc_luchs_flags";

				};

				class MG3_in
				{
					
					displayName = "$STR_MG3_benutzen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'commander_hatch' > 0) and (alive this) and (this turretUnit [0,0] == player)";
					statement = "[this,(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player])] spawn redd_fnc_luchs_move_to_mg3";
				};

				class MG3_out
				{
					
					displayName = "$STR_MG3_verlassen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [2] == player) and (alive this)";
					statement = "[this,(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player])] spawn redd_fnc_luchs_move_to_mg3";

				};

				class Bino_in
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,0] == player) and (this animationPhase 'commander_hatch' > 0) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [3]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Luchs_Bino_In', true, true];"; 
				
				};

				class Bino_out
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [3] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Luchs_Bino_In', false, true];"; 
				
				};

                class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";

				};

				class TarnLichtRundum_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";

				};
				
				class TarnLichtRundum_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_rundum_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class orangelicht_auf
				{
					
					displayName = "$STR_Rundumleuchte_montieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 1) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',0];this animate ['Hide_Rundumleuchte_glass_2',0];this animateSource ['Hide_Rundumleuchte_fuss', 0];this animateSource ['Hide_Rundumleuchte_fuss_2', 0];";

				};

				class orangelicht_ab
				{

					displayName = "$STR_Rundumleuchte_abmontieren";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander' == 1) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'Hide_Rundumleuchte_fuss_2' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "this animate ['Hide_Rundumleuchte_rot',1];this animate ['Hide_Rundumleuchte_glass_2',1];this animateSource ['Hide_Rundumleuchte_fuss', 1];this animateSource ['Hide_Rundumleuchte_fuss_2', 1];";

				};

				class Orangelicht_an
				{

					displayName = "$STR_Rundumleuchte_an";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'Hide_Rundumleuchte_fuss' == 0) and (this animationPhase 'orangelicht_an' < 0.5) and (alive this)";
					statement = "player action ['lightOn', this];this animate ['BeaconsStart_2',1];this animate ['orangelicht_an',1];this animate ['orangelicht_an_flare',1]";
					
				};
				class Orangelicht_aus
				{

					displayName = "$STR_Rundumleuchte_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "player == (driver this) and (this animationPhase 'orangelicht_an' > 0.5) and (alive this)";
					statement = "this animate ['orangelicht_an',0];this animate ['orangelicht_an_flare',0];this animate ['BeaconsStart_2',0]";

				};

				class door_open
                {

                    displayName = "$STR_Tuere_oeffnen";
					position = "pos gunner";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationSourcePhase 'rot_door_source' == 0) and (alive this)"; 
					statement = "this animateSource ['rot_door_source', 1];";

                };

				class door_close
                {

                    displayName = "$STR_Tuere_schliessen";
					position = "pos gunner";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationSourcePhase 'rot_door_source' == 1) and (alive this)"; 
					statement = "this animateSource ['rot_door_source', 0];";

                };
				
				class Spiegel_einklappen
				{
					
					displayName = "$STR_Spiegel_einklappen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 1];";
				
				};
				
				class Spiegel_ausklappen
				{
					
					displayName = "$STR_Spiegel_ausklappen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 0];";
				
				};

				class Tarnnetz_Fzg_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_luchs_camonet";

				};

				class Tarnnetz_Fzg_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_Fzg_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and (this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet',player] call Redd_fnc_luchs_camonet";

				};

				class Tarnnetz_Boden_aufbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_aufbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and !(this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_luchs_camonet";

				};

				class Tarnnetz_Boden_abbauen
				{

					displayName = "$STR_Redd_Tarnnetz_boden_abbauen";
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(player in this) and !(this getVariable 'has_camonet') and (this getVariable 'has_camonet_large')";
					statement = "[this,'camonet_large',player] call Redd_fnc_luchs_camonet";

				};

				class fixTurretBug
				{
					displayName = "Fix GetIn Bug"; 
					position = "actionPoint";
					radius = 5;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(alive this)";
					statement = "if ((this turretUnit [2] isEqualTo objNull) and (this turretUnit [3] isEqualTo objNull)) then {this setVariable ['Redd_Luchs_MG3_In', false, true];this setVariable ['Redd_Luchs_Bino_In', false, true];[this,[[0,0],false]] remoteExecCall ['lockTurret'];};";
				};

			};
			
            class EventHandlers: EventHandlers 
            {

               	init = "_this call redd_fnc_Luchs_Init";
				getOut = "_this call redd_fnc_Luchs_GetOut";
				getIn = "_this spawn redd_fnc_Luchs_GetIn";
				fired = "_this call redd_fnc_Luchs_Fired";

            };

			class Attributes 
			{

				class rnt_sppz_2a2_luchs_Bataillon_Attribute 
				{
					
					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "rnt_sppz_2a2_luchs_Bataillon_Attribute ";
					control = "Combo";
					expression = "_this setVariable ['rnt_sppz_2a2_luchs_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};

						class Btl_5 
						{
							
							name = "5";
							value = "5";
							
						};

						class Btl_6 
						{
							
							name = "6";
							value = "6";
							
						};

						class Btl_7 
						{
							
							name = "7";
							value = "7";
							
						};

						class Btl_12
						{
							
							name = "12";
							value = "12";
							
						};

						class Btl_13
						{
							
							name = "13";
							value = "13";
							
						};

					};
					
				};
				
				class rnt_sppz_2a2_luchs_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "rnt_sppz_2a2_luchs_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['rnt_sppz_2a2_luchs_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

                        class Kompanie_5
						{
							
							name = "5";
							value = "5";
							
						};

					};
					
				};

            };

        };

        class rnt_sppz_2a2_luchs_flecktarn: rnt_sppz_2a2_luchs_Base
		{
			
			editorPreview="rnt_sppz_2a2_luchs\pictures\luchs_fleck.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_rnt_sppz_2a2_luchs_flecktarn";

            hiddenSelectionsTextures[] = 
            {

                "rnt_sppz_2a2_luchs\data\rnt_luchs_schild_kleint_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_kleint_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_teile_schrauben_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_rest_teile_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_rest_fahrwerk_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_wanne_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_fahrer_funker_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_instr_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_instr_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_innen_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_mk_wspiegel_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_innen_wanne_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_luken_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_reifen_fahrwerk_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_2_blend_co"

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_rnt_sppz_2a2_luchs_flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						"rnt_sppz_2a2_luchs\data\rnt_luchs_schild_kleint_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_kleint_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_teile_schrauben_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_rest_teile_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_rest_fahrwerk_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_wanne_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_fahrer_funker_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_instr_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_instr_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_innen_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_mk_wspiegel_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_innen_wanne_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_luken_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_reifen_fahrwerk_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_2_blend_co"
						
					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_rnt_sppz_2a2_luchs_tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_schild_kleint_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_dach_kleint_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_dach_teile_schrauben_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_rest_teile_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_rest_fahrwerk_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_wanne_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_lafette_bf_kabel_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_fahrer_funker_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_instr_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_instr_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_turm_innen_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_mk_wspiegel_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_innen_wanne_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_turm_luken_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_reifen_fahrwerk_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_d_lafette_bf_kabel_2_blend_co"

					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_rnt_sppz_2a2_luchs_wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{

						"rnt_sppz_2a2_luchs\data\rnt_luchs_w_schild_kleint_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_kleint_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_teile_schrauben_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_w_rest_teile_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_rest_fahrwerk_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_w_wanne_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_fahrer_funker_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_instr_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_instr_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_innen_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_mk_wspiegel_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_innen_wanne_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_w_turm_luken_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_w_reifen_fahrwerk_blend_co",
						"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_2_blend_co"

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

		};

		class rnt_sppz_2a2_luchs_tropentarn: rnt_sppz_2a2_luchs_flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="rnt_sppz_2a2_luchs\pictures\luchs_trope.paa";
			displayName="$STR_rnt_sppz_2a2_luchs_tropentarn";

			hiddenSelectionsTextures[] = 
            {

				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_schild_kleint_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_dach_kleint_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_dach_teile_schrauben_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_rest_teile_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_rest_fahrwerk_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_wanne_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_lafette_bf_kabel_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_fahrer_funker_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_instr_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_instr_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_turm_innen_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_mk_wspiegel_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_innen_wanne_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_turm_luken_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_reifen_fahrwerk_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_d_lafette_bf_kabel_2_blend_co"

            };

		};

		class rnt_sppz_2a2_luchs_wintertarn: rnt_sppz_2a2_luchs_flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="rnt_sppz_2a2_luchs\pictures\luchs_winter.paa";
			displayName="$STR_rnt_sppz_2a2_luchs_wintertarn";

			/*
			class EventHandlers: EventHandlers
			{
				init = "_this call redd_fnc_Luchs_w_Init";
			};
			*/
			hiddenSelectionsTextures[] = 
            {

				"rnt_sppz_2a2_luchs\data\rnt_luchs_w_schild_kleint_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_kleint_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_dach_teile_schrauben_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_w_rest_teile_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_rest_fahrwerk_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_w_wanne_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_fahrer_funker_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_instr_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_instr_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_turm_innen_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_mk_wspiegel_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_innen_wanne_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_w_turm_luken_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_w_reifen_fahrwerk_blend_co",
				"rnt_sppz_2a2_luchs\data\rnt_luchs_lafette_bf_kabel_2_blend_co"

            };

		};
        
    };